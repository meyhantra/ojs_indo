<?php return array (
  'blockContent' => 
  array (
    'en_US' => '<ul class="sidemenu">
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/submissions#authorGuidelines">Author Guidelines</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/editorialTeam#focusandscope">Editorial Team</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/editorialPolicies#focusandscope">Focus and Scope</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/editorialPolicies#custom-0">Publication Ethics</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/editorialPolicies#openAccessPolicy">Open Access Policy</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/editorialPolicies#peerReviewProcess">Peer Review Process</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/submissions">Online Submissions</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/authorFees">Author Fees</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/contact">Contact Us</a></strong></li>
</ul>',
  ),
  'enabled' => true,
);