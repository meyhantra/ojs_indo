<?php return array (
  'enabled' => true,
  'manualInstructions' => 'This journal charges the following author fees.

Article Submission: 0.00 (IDR)

Fast-Track Review: 0.00 (IDR)

Article Publication: 0.00 (IDR)

If this paper is accepted for publication, you will be asked to pay an Article Publication Fee to cover publications costs.',
);