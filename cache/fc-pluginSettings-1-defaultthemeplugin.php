<?php return array (
  'baseColour' => '#AEC2D0',
  'enabled' => true,
  'showDescriptionInJournalIndex' => 'false',
  'typography' => 'notoSerif_notoSans',
  'useHomepageImageAsHeader' => 'false',
);