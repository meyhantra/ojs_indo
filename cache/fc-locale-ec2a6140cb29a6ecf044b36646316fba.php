<?php return array (
  'plugins.generic.plagiarism.displayName' => 'iThenticate Plagiarism Detector Plugin',
  'plugins.generic.plagiarism.description' => 'Send all submissions to iThenticate to be checked for possible plagiarism.',
  'plugins.generic.plagiarism.description.seeReadme' => 'Send all submissions to iThenticate to be checked for possible plagiarism. <strong>See the README document for this plugin for installation instructions.</strong>',
);