<?php return array (
  'doiIssueSuffixPattern' => NULL,
  'doiPrefix' => '10.54373',
  'doiPublicationSuffixPattern' => NULL,
  'doiRepresentationSuffixPattern' => NULL,
  'doiSuffix' => 'default',
  'enabled' => true,
  'enableIssueDoi' => true,
  'enablePublicationDoi' => true,
  'enableRepresentationDoi' => false,
);