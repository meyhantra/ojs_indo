<?php
/* Smarty version 3.1.34-dev-7, created on 2022-08-06 03:44:05
  from 'plugins-0-plugins-generic-customBlockManager-generic-customBlockManager:block.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_62ed8115a5d254_04703935',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '711df617e45e1f0103f08424acb0608cd990639a' => 
    array (
      0 => 'plugins-0-plugins-generic-customBlockManager-generic-customBlockManager:block.tpl',
      1 => 1659732072,
      2 => 'plugins-0-plugins-generic-customBlockManager-generic-customBlockManager',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_62ed8115a5d254_04703935 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="pkp_block block_custom" id="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['customBlockId']->value ));?>
">
	<div class="content">
		<?php echo $_smarty_tpl->tpl_vars['customBlockContent']->value;?>

	</div>
</div>
<?php }
}
