<?php
/* Smarty version 3.1.34-dev-7, created on 2022-08-06 02:17:24
  from 'plugins-2-plugins-generic-customBlockManager-generic-customBlockManager:block.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_62ed6cc483b656_72859356',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e2c7763ed9cbd24d28766d981f416888c019bde5' => 
    array (
      0 => 'plugins-2-plugins-generic-customBlockManager-generic-customBlockManager:block.tpl',
      1 => 1611341800,
      2 => 'plugins-2-plugins-generic-customBlockManager-generic-customBlockManager',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_62ed6cc483b656_72859356 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="pkp_block block_custom" id="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['customBlockId']->value ));?>
">
	<div class="content">
		<?php echo $_smarty_tpl->tpl_vars['customBlockContent']->value;?>

	</div>
</div>
<?php }
}
