<?php
/* Smarty version 3.1.34-dev-7, created on 2022-08-06 02:36:32
  from 'plugins-2-plugins-generic-orcidProfile-generic-orcidProfile:statusForm.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_62ed71405313f8_55772606',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '86f5187720a242af582d879561c4046850766ffb' => 
    array (
      0 => 'plugins-2-plugins-generic-orcidProfile-generic-orcidProfile:statusForm.tpl',
      1 => 1629665612,
      2 => 'plugins-2-plugins-generic-orcidProfile-generic-orcidProfile',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_62ed71405313f8_55772606 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
>
	$(function() {
		// Attach the form handler.
		$('#orcidProfileStatusForm').pkpHandler('$.pkp.controllers.form.AjaxFormHandler');
	});
<?php echo '</script'; ?>
>


<div id="orcidProfileStatus">
	<h3><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"plugins.generic.orcidProfile.manager.status.description"),$_smarty_tpl ) );?>
</h3>

	<div class="description">
	<?php if ($_smarty_tpl->tpl_vars['globallyConfigured']->value) {?>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"plugins.generic.orcidProfile.manager.status.configuration.global"),$_smarty_tpl ) );?>

	<?php } else { ?>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"plugins.generic.orcidProfile.manager.status.configuration.journal"),$_smarty_tpl ) );?>

	<?php }?>
	</div>
	<div class="description">
	<?php if ($_smarty_tpl->tpl_vars['pluginEnabled']->value) {?>
		<p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"plugins.generic.orcidProfile.manager.status.configuration.enabled"),$_smarty_tpl ) );?>
</p>
	<?php } else { ?>
		<p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"plugins.generic.orcidProfile.manager.status.configuration.disabled"),$_smarty_tpl ) );?>
</p>
	</div>

	<?php }?>
   </div>


<div class="description">
	<?php if ($_smarty_tpl->tpl_vars['clientIdValid']->value) {?>
	<p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"plugins.generic.orcidProfile.manager.status.configuration.clientIdValid"),$_smarty_tpl ) );?>
</p>
	<?php } else { ?>
	<p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"plugins.generic.orcidProfile.manager.status.configuration.clientIdInvalid"),$_smarty_tpl ) );?>
</p>
	<?php }?>
</div>

<div class="description">
	<?php if ($_smarty_tpl->tpl_vars['clientSecretValid']->value) {?>
		<p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"plugins.generic.orcidProfile.manager.status.configuration.clientSecretValid"),$_smarty_tpl ) );?>
</p>
	<?php } else { ?>
		<p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['translate'][0], array( array('key'=>"plugins.generic.orcidProfile.manager.status.configuration.clientSecretInvalid"),$_smarty_tpl ) );?>
</p>
	<?php }?>
</div>



</div>


<?php }
}
