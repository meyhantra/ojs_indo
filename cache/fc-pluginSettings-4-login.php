<?php return array (
  'blockContent' => 
  array (
    'en_US' => '<form id="login" class="pkp_form login" action="https://ejournal.indo-intellectual.id/index.php/ifi/login/signIn" method="post">
<div class="form-group"><label for="login-username"> Username </label> <input id="login-username" class="form-control" name="username" required="" type="text" value="" placeholder="Username"></div>
<div class="form-group"><label for="login-password"> Password </label> <input id="login-password" class="form-control" maxlength="32" name="password" required="$passwordRequired" type="password" placeholder="Password"></div>
<div class="form-group"><a href="https://ejournal.indo-intellectual.id/index.php/ifi/login/lostPassword"> Forgot your password? </a></div>
<div class="checkbox"><label> <input id="remember" checked="checked" name="remember" type="checkbox" value="1"> Keep me logged in </label></div>
<div class="buttons"><button class="btn btn-primary" type="submit"> Login </button> <a class="btn btn-default register-button" role="button" href="https://ejournal.indo-intellectual.id/index.php/ifi/user/register?source="> Register </a></div>
</form>',
  ),
  'enabled' => true,
);