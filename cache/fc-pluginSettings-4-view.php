<?php return array (
  'blockContent' => 
  array (
    'en_US' => '<ul class="sidemenu">
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/idijes/about/submissions#authorGuidelines">Author Guidelines</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/idijes/about/editorialTeam#focusandscope">Editorial Team</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/idijes/about/editorialPolicies#focusandscope">Focus and Scope</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/idijes/about/editorialPolicies#custom-0">Publication Ethics</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/idijes/about/editorialPolicies#openAccessPolicy">Open Access Policy</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/idijes/about/editorialPolicies#peerReviewProcess">Peer Review Process</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/idijes/about/submissions">Online Submissions</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/idijes/authorFees">Author Fees</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/idijes/about/contact">Contact Us</a></strong></li>
</ul>',
  ),
  'enabled' => true,
);