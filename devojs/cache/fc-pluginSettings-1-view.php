<?php return array (
  'blockContent' => 
  array (
    'en_US' => '<ul class="sidemenu">
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/submissions#authorGuidelines">Author Guidelines</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/editorialTeam">Editorial Team</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/focusandscope">Focus and Scope</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/submissions#authorGuidelines">Publication Ethics</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/submissions#authorGuidelines">Open Access Policy</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/submissions#authorGuidelines">Peer Review Process</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/submissions#authorGuidelines">Online Submissions</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/about/submissions#authorGuidelines">Author Fees</a></strong></li>
<li><strong><a href="https://ejournal.indo-intellectual.id/index.php/ifi/contactus">Contact Us</a></strong></li>
</ul>',
  ),
  'enabled' => true,
);