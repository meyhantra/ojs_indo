msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2020-01-17T18:14:00+00:00\n"
"PO-Revision-Date: 2020-01-17T18:14:00+00:00\n"
"Language: \n"

msgid "plugins.importexport.quickSubmit.displayName"
msgstr "Hızlı Gönderi Eklentisi"

msgid "plugins.importexport.quickSubmit.description"
msgstr "Tek Adımda Gönderi Eklentisi"

msgid "plugins.importexport.quickSubmit.success"
msgstr "Makale Kaydedildi"

msgid "plugins.importexport.quickSubmit.successDescription"
msgstr "Makale başarılı bir şekilde oluşturuldu."

msgid "plugins.importexport.quickSubmit.successReturn"
msgstr "Hızlı Gönderi Eklentisine geri dön."
