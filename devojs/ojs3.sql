-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 20 Jan 2021 pada 21.28
-- Versi server: 5.7.31
-- Versi PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ojs3`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `access_keys`
--

DROP TABLE IF EXISTS `access_keys`;
CREATE TABLE IF NOT EXISTS `access_keys` (
  `access_key_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `context` varchar(40) NOT NULL,
  `key_hash` varchar(40) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  `expiry_date` datetime NOT NULL,
  PRIMARY KEY (`access_key_id`),
  KEY `access_keys_hash` (`key_hash`,`user_id`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `announcements`
--

DROP TABLE IF EXISTS `announcements`;
CREATE TABLE IF NOT EXISTS `announcements` (
  `announcement_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assoc_type` smallint(6) DEFAULT NULL,
  `assoc_id` bigint(20) NOT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `date_expire` datetime DEFAULT NULL,
  `date_posted` datetime NOT NULL,
  PRIMARY KEY (`announcement_id`),
  KEY `announcements_assoc` (`assoc_type`,`assoc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `announcement_settings`
--

DROP TABLE IF EXISTS `announcement_settings`;
CREATE TABLE IF NOT EXISTS `announcement_settings` (
  `announcement_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `announcement_settings_pkey` (`announcement_id`,`locale`,`setting_name`),
  KEY `announcement_settings_announcement_id` (`announcement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `announcement_types`
--

DROP TABLE IF EXISTS `announcement_types`;
CREATE TABLE IF NOT EXISTS `announcement_types` (
  `type_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assoc_type` smallint(6) DEFAULT NULL,
  `assoc_id` bigint(20) NOT NULL,
  PRIMARY KEY (`type_id`),
  KEY `announcement_types_assoc` (`assoc_type`,`assoc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `announcement_type_settings`
--

DROP TABLE IF EXISTS `announcement_type_settings`;
CREATE TABLE IF NOT EXISTS `announcement_type_settings` (
  `type_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `announcement_type_settings_pkey` (`type_id`,`locale`,`setting_name`),
  KEY `announcement_type_settings_type_id` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `authors`
--

DROP TABLE IF EXISTS `authors`;
CREATE TABLE IF NOT EXISTS `authors` (
  `author_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(90) NOT NULL,
  `include_in_browse` tinyint(4) NOT NULL DEFAULT '1',
  `publication_id` bigint(20) DEFAULT NULL,
  `submission_id` bigint(20) DEFAULT NULL,
  `seq` double NOT NULL DEFAULT '0',
  `user_group_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`author_id`),
  KEY `authors_publication_id` (`publication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `author_settings`
--

DROP TABLE IF EXISTS `author_settings`;
CREATE TABLE IF NOT EXISTS `author_settings` (
  `author_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) DEFAULT NULL,
  UNIQUE KEY `author_settings_pkey` (`author_id`,`locale`,`setting_name`),
  KEY `author_settings_author_id` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_sources`
--

DROP TABLE IF EXISTS `auth_sources`;
CREATE TABLE IF NOT EXISTS `auth_sources` (
  `auth_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) NOT NULL,
  `plugin` varchar(32) NOT NULL,
  `auth_default` tinyint(4) NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `context_id` bigint(20) NOT NULL,
  `parent_id` bigint(20) NOT NULL,
  `seq` bigint(20) DEFAULT NULL,
  `path` varchar(255) NOT NULL,
  `image` text,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_path` (`context_id`,`path`),
  KEY `category_context_id` (`context_id`,`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `category_settings`
--

DROP TABLE IF EXISTS `category_settings`;
CREATE TABLE IF NOT EXISTS `category_settings` (
  `category_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `category_settings_pkey` (`category_id`,`locale`,`setting_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `citations`
--

DROP TABLE IF EXISTS `citations`;
CREATE TABLE IF NOT EXISTS `citations` (
  `citation_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `publication_id` bigint(20) NOT NULL DEFAULT '0',
  `raw_citation` text,
  `seq` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`citation_id`),
  UNIQUE KEY `citations_publication_seq` (`publication_id`,`seq`),
  KEY `citations_publication` (`publication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `citation_settings`
--

DROP TABLE IF EXISTS `citation_settings`;
CREATE TABLE IF NOT EXISTS `citation_settings` (
  `citation_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `citation_settings_pkey` (`citation_id`,`locale`,`setting_name`),
  KEY `citation_settings_citation_id` (`citation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `completed_payments`
--

DROP TABLE IF EXISTS `completed_payments`;
CREATE TABLE IF NOT EXISTS `completed_payments` (
  `completed_payment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `payment_type` bigint(20) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  `amount` double NOT NULL,
  `currency_code_alpha` varchar(3) DEFAULT NULL,
  `payment_method_plugin_name` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`completed_payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `controlled_vocabs`
--

DROP TABLE IF EXISTS `controlled_vocabs`;
CREATE TABLE IF NOT EXISTS `controlled_vocabs` (
  `controlled_vocab_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `symbolic` varchar(64) NOT NULL,
  `assoc_type` bigint(20) NOT NULL DEFAULT '0',
  `assoc_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`controlled_vocab_id`),
  UNIQUE KEY `controlled_vocab_symbolic` (`symbolic`,`assoc_type`,`assoc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `controlled_vocabs`
--

INSERT INTO `controlled_vocabs` (`controlled_vocab_id`, `symbolic`, `assoc_type`, `assoc_id`) VALUES
(8, 'interest', 0, 0),
(4, 'mods34-genre-marcgt', 0, 0),
(2, 'mods34-name-role-roleTerms-marcrelator', 0, 0),
(1, 'mods34-name-types', 0, 0),
(5, 'mods34-physicalDescription-form-marcform', 0, 0),
(3, 'mods34-typeOfResource', 0, 0),
(7, 'openurl10-book-genres', 0, 0),
(6, 'openurl10-journal-genres', 0, 0),
(15, 'submissionAgency', 1048588, 1),
(13, 'submissionDiscipline', 1048588, 1),
(11, 'submissionKeyword', 1048588, 1),
(14, 'submissionLanguage', 1048588, 1),
(12, 'submissionSubject', 1048588, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `controlled_vocab_entries`
--

DROP TABLE IF EXISTS `controlled_vocab_entries`;
CREATE TABLE IF NOT EXISTS `controlled_vocab_entries` (
  `controlled_vocab_entry_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `controlled_vocab_id` bigint(20) NOT NULL,
  `seq` double DEFAULT NULL,
  PRIMARY KEY (`controlled_vocab_entry_id`),
  KEY `controlled_vocab_entries_cv_id` (`controlled_vocab_id`,`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `controlled_vocab_entries`
--

INSERT INTO `controlled_vocab_entries` (`controlled_vocab_entry_id`, `controlled_vocab_id`, `seq`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 1),
(5, 2, 2),
(6, 2, 3),
(7, 2, 4),
(8, 2, 5),
(9, 2, 6),
(10, 2, 7),
(11, 3, 1),
(12, 3, 2),
(13, 3, 3),
(14, 4, 1),
(15, 4, 2),
(16, 4, 3),
(17, 4, 4),
(18, 4, 5),
(19, 4, 6),
(20, 4, 7),
(21, 4, 8),
(22, 4, 9),
(23, 4, 10),
(24, 4, 11),
(25, 4, 12),
(26, 5, 1),
(27, 5, 2),
(28, 6, 1),
(29, 6, 2),
(30, 6, 3),
(31, 6, 4),
(32, 6, 5),
(33, 6, 6),
(34, 6, 7),
(35, 7, 1),
(36, 7, 2),
(37, 7, 3),
(38, 7, 4),
(39, 7, 5),
(40, 7, 6),
(41, 7, 7),
(85, 11, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `controlled_vocab_entry_settings`
--

DROP TABLE IF EXISTS `controlled_vocab_entry_settings`;
CREATE TABLE IF NOT EXISTS `controlled_vocab_entry_settings` (
  `controlled_vocab_entry_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `c_v_e_s_pkey` (`controlled_vocab_entry_id`,`locale`,`setting_name`),
  KEY `c_v_e_s_entry_id` (`controlled_vocab_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `controlled_vocab_entry_settings`
--

INSERT INTO `controlled_vocab_entry_settings` (`controlled_vocab_entry_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, '', 'name', 'personal', 'string'),
(2, '', 'name', 'corporate', 'string'),
(3, '', 'name', 'conference', 'string'),
(4, '', 'description', 'Author', 'string'),
(4, '', 'name', 'aut', 'string'),
(5, '', 'description', 'Contributor', 'string'),
(5, '', 'name', 'ctb', 'string'),
(6, '', 'description', 'Editor', 'string'),
(6, '', 'name', 'edt', 'string'),
(7, '', 'description', 'Illustrator', 'string'),
(7, '', 'name', 'ill', 'string'),
(8, '', 'description', 'Photographer', 'string'),
(8, '', 'name', 'pht', 'string'),
(9, '', 'description', 'Sponsor', 'string'),
(9, '', 'name', 'spn', 'string'),
(10, '', 'description', 'Translator', 'string'),
(10, '', 'name', 'trl', 'string'),
(11, '', 'name', 'multimedia', 'string'),
(12, '', 'name', 'still image', 'string'),
(13, '', 'name', 'text', 'string'),
(14, '', 'name', 'article', 'string'),
(15, '', 'name', 'book', 'string'),
(16, '', 'name', 'conference publication', 'string'),
(17, '', 'name', 'issue', 'string'),
(18, '', 'name', 'journal', 'string'),
(19, '', 'name', 'newspaper', 'string'),
(20, '', 'name', 'picture', 'string'),
(21, '', 'name', 'review', 'string'),
(22, '', 'name', 'periodical', 'string'),
(23, '', 'name', 'series', 'string'),
(24, '', 'name', 'thesis', 'string'),
(25, '', 'name', 'web site', 'string'),
(26, '', 'name', 'electronic', 'string'),
(27, '', 'name', 'print', 'string'),
(28, '', 'name', 'journal', 'string'),
(29, '', 'name', 'issue', 'string'),
(30, '', 'name', 'article', 'string'),
(31, '', 'name', 'proceeding', 'string'),
(32, '', 'name', 'conference', 'string'),
(33, '', 'name', 'preprint', 'string'),
(34, '', 'name', 'unknown', 'string'),
(35, '', 'name', 'book', 'string'),
(36, '', 'name', 'bookitem', 'string'),
(37, '', 'name', 'proceeding', 'string'),
(38, '', 'name', 'conference', 'string'),
(39, '', 'name', 'report', 'string'),
(40, '', 'name', 'document', 'string'),
(41, '', 'name', 'unknown', 'string'),
(85, 'en_US', 'submissionKeyword', 'Semabrang', 'string');

-- --------------------------------------------------------

--
-- Struktur dari tabel `custom_issue_orders`
--

DROP TABLE IF EXISTS `custom_issue_orders`;
CREATE TABLE IF NOT EXISTS `custom_issue_orders` (
  `issue_id` bigint(20) NOT NULL,
  `journal_id` bigint(20) NOT NULL,
  `seq` double NOT NULL DEFAULT '0',
  UNIQUE KEY `custom_issue_orders_pkey` (`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `custom_section_orders`
--

DROP TABLE IF EXISTS `custom_section_orders`;
CREATE TABLE IF NOT EXISTS `custom_section_orders` (
  `issue_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `seq` double NOT NULL DEFAULT '0',
  UNIQUE KEY `custom_section_orders_pkey` (`issue_id`,`section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_object_tombstones`
--

DROP TABLE IF EXISTS `data_object_tombstones`;
CREATE TABLE IF NOT EXISTS `data_object_tombstones` (
  `tombstone_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `data_object_id` bigint(20) NOT NULL,
  `date_deleted` datetime NOT NULL,
  `set_spec` varchar(255) NOT NULL,
  `set_name` varchar(255) NOT NULL,
  `oai_identifier` varchar(255) NOT NULL,
  PRIMARY KEY (`tombstone_id`),
  KEY `data_object_tombstones_data_object_id` (`data_object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_object_tombstone_oai_set_objects`
--

DROP TABLE IF EXISTS `data_object_tombstone_oai_set_objects`;
CREATE TABLE IF NOT EXISTS `data_object_tombstone_oai_set_objects` (
  `object_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tombstone_id` bigint(20) NOT NULL,
  `assoc_type` bigint(20) NOT NULL,
  `assoc_id` bigint(20) NOT NULL,
  PRIMARY KEY (`object_id`),
  KEY `data_object_tombstone_oai_set_objects_tombstone_id` (`tombstone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_object_tombstone_settings`
--

DROP TABLE IF EXISTS `data_object_tombstone_settings`;
CREATE TABLE IF NOT EXISTS `data_object_tombstone_settings` (
  `tombstone_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `data_object_tombstone_settings_pkey` (`tombstone_id`,`locale`,`setting_name`),
  KEY `data_object_tombstone_settings_tombstone_id` (`tombstone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `edit_decisions`
--

DROP TABLE IF EXISTS `edit_decisions`;
CREATE TABLE IF NOT EXISTS `edit_decisions` (
  `edit_decision_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `submission_id` bigint(20) NOT NULL,
  `review_round_id` bigint(20) DEFAULT NULL,
  `stage_id` bigint(20) DEFAULT NULL,
  `round` tinyint(4) NOT NULL,
  `editor_id` bigint(20) NOT NULL,
  `decision` tinyint(4) NOT NULL,
  `date_decided` datetime NOT NULL,
  PRIMARY KEY (`edit_decision_id`),
  KEY `edit_decisions_submission_id` (`submission_id`),
  KEY `edit_decisions_editor_id` (`editor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `email_log`
--

DROP TABLE IF EXISTS `email_log`;
CREATE TABLE IF NOT EXISTS `email_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assoc_type` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  `sender_id` bigint(20) NOT NULL,
  `date_sent` datetime NOT NULL,
  `event_type` bigint(20) DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `recipients` text,
  `cc_recipients` text,
  `bcc_recipients` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  PRIMARY KEY (`log_id`),
  KEY `email_log_assoc` (`assoc_type`,`assoc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `email_log_users`
--

DROP TABLE IF EXISTS `email_log_users`;
CREATE TABLE IF NOT EXISTS `email_log_users` (
  `email_log_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  UNIQUE KEY `email_log_user_id` (`email_log_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `email_templates`
--

DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE IF NOT EXISTS `email_templates` (
  `email_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email_key` varchar(64) NOT NULL,
  `context_id` bigint(20) DEFAULT '0',
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`),
  UNIQUE KEY `email_templates_email_key` (`email_key`,`context_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `email_templates_default`
--

DROP TABLE IF EXISTS `email_templates_default`;
CREATE TABLE IF NOT EXISTS `email_templates_default` (
  `email_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email_key` varchar(64) NOT NULL,
  `can_disable` tinyint(4) NOT NULL DEFAULT '1',
  `can_edit` tinyint(4) NOT NULL DEFAULT '1',
  `from_role_id` bigint(20) DEFAULT NULL,
  `to_role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`email_id`),
  KEY `email_templates_default_email_key` (`email_key`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `email_templates_default`
--

INSERT INTO `email_templates_default` (`email_id`, `email_key`, `can_disable`, `can_edit`, `from_role_id`, `to_role_id`) VALUES
(1, 'NOTIFICATION', 0, 1, NULL, NULL),
(2, 'NOTIFICATION_CENTER_DEFAULT', 0, 1, NULL, NULL),
(3, 'PASSWORD_RESET_CONFIRM', 0, 1, NULL, NULL),
(4, 'PASSWORD_RESET', 0, 1, NULL, NULL),
(5, 'USER_REGISTER', 0, 1, NULL, NULL),
(6, 'USER_VALIDATE', 0, 1, NULL, NULL),
(7, 'REVIEWER_REGISTER', 0, 1, NULL, NULL),
(8, 'PUBLISH_NOTIFY', 0, 1, NULL, NULL),
(9, 'LOCKSS_EXISTING_ARCHIVE', 0, 1, NULL, NULL),
(10, 'LOCKSS_NEW_ARCHIVE', 0, 1, NULL, NULL),
(11, 'SUBMISSION_ACK', 1, 1, NULL, 65536),
(12, 'SUBMISSION_ACK_NOT_USER', 1, 1, NULL, 65536),
(13, 'EDITOR_ASSIGN', 1, 1, 16, 16),
(14, 'REVIEW_CANCEL', 1, 1, 16, 4096),
(15, 'REVIEW_REINSTATE', 1, 1, 16, 4096),
(16, 'REVIEW_REQUEST', 1, 1, 16, 4096),
(17, 'REVIEW_REQUEST_SUBSEQUENT', 1, 1, 16, 4096),
(18, 'REVIEW_REQUEST_ONECLICK', 1, 1, 16, 4096),
(19, 'REVIEW_REQUEST_ONECLICK_SUBSEQUENT', 1, 1, 16, 4096),
(20, 'REVIEW_REQUEST_ATTACHED', 0, 1, 16, 4096),
(21, 'REVIEW_REQUEST_ATTACHED_SUBSEQUENT', 0, 1, 16, 4096),
(22, 'REVIEW_REQUEST_REMIND_AUTO', 0, 1, NULL, 4096),
(23, 'REVIEW_REQUEST_REMIND_AUTO_ONECLICK', 0, 1, NULL, 4096),
(24, 'REVIEW_CONFIRM', 1, 1, 4096, 16),
(25, 'REVIEW_DECLINE', 1, 1, 4096, 16),
(26, 'REVIEW_ACK', 1, 1, 16, 4096),
(27, 'REVIEW_REMIND', 0, 1, 16, 4096),
(28, 'REVIEW_REMIND_AUTO', 0, 1, NULL, 4096),
(29, 'REVIEW_REMIND_ONECLICK', 0, 1, 16, 4096),
(30, 'REVIEW_REMIND_AUTO_ONECLICK', 0, 1, NULL, 4096),
(31, 'EDITOR_DECISION_ACCEPT', 0, 1, 16, 65536),
(32, 'EDITOR_DECISION_SEND_TO_EXTERNAL', 0, 1, 16, 65536),
(33, 'EDITOR_DECISION_SEND_TO_PRODUCTION', 0, 1, 16, 65536),
(34, 'EDITOR_DECISION_REVISIONS', 0, 1, 16, 65536),
(35, 'EDITOR_DECISION_RESUBMIT', 0, 1, 16, 65536),
(36, 'EDITOR_DECISION_DECLINE', 0, 1, 16, 65536),
(37, 'EDITOR_DECISION_INITIAL_DECLINE', 0, 1, 16, 65536),
(38, 'EDITOR_RECOMMENDATION', 0, 1, 16, 16),
(39, 'COPYEDIT_REQUEST', 1, 1, 16, 4097),
(40, 'LAYOUT_REQUEST', 1, 1, 16, 4097),
(41, 'LAYOUT_COMPLETE', 1, 1, 4097, 16),
(42, 'EMAIL_LINK', 0, 1, 1048576, NULL),
(43, 'SUBSCRIPTION_NOTIFY', 0, 1, NULL, 1048576),
(44, 'OPEN_ACCESS_NOTIFY', 0, 1, NULL, 1048576),
(45, 'SUBSCRIPTION_BEFORE_EXPIRY', 0, 1, NULL, 1048576),
(46, 'SUBSCRIPTION_AFTER_EXPIRY', 0, 1, NULL, 1048576),
(47, 'SUBSCRIPTION_AFTER_EXPIRY_LAST', 0, 1, NULL, 1048576),
(48, 'SUBSCRIPTION_PURCHASE_INDL', 0, 1, NULL, 2097152),
(49, 'SUBSCRIPTION_PURCHASE_INSTL', 0, 1, NULL, 2097152),
(50, 'SUBSCRIPTION_RENEW_INDL', 0, 1, NULL, 2097152),
(51, 'SUBSCRIPTION_RENEW_INSTL', 0, 1, NULL, 2097152),
(52, 'CITATION_EDITOR_AUTHOR_QUERY', 0, 1, NULL, NULL),
(53, 'REVISED_VERSION_NOTIFY', 0, 1, NULL, 16),
(54, 'STATISTICS_REPORT_NOTIFICATION', 0, 1, 16, 17),
(55, 'ORCID_COLLECT_AUTHOR_ID', 0, 1, NULL, NULL),
(56, 'ORCID_REQUEST_AUTHOR_AUTHORIZATION', 0, 1, NULL, NULL),
(57, 'MANUAL_PAYMENT_NOTIFICATION', 0, 1, NULL, NULL),
(58, 'PAYPAL_INVESTIGATE_PAYMENT', 0, 1, NULL, NULL),
(59, 'ANNOUNCEMENT', 0, 1, 16, 1048576);

-- --------------------------------------------------------

--
-- Struktur dari tabel `email_templates_default_data`
--

DROP TABLE IF EXISTS `email_templates_default_data`;
CREATE TABLE IF NOT EXISTS `email_templates_default_data` (
  `email_key` varchar(64) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT 'en_US',
  `subject` varchar(120) NOT NULL,
  `body` text,
  `description` text,
  UNIQUE KEY `email_templates_default_data_pkey` (`email_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `email_templates_default_data`
--

INSERT INTO `email_templates_default_data` (`email_key`, `locale`, `subject`, `body`, `description`) VALUES
('ANNOUNCEMENT', 'ar_IQ', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nÙ‚Ù… Ø¨Ø²ÙŠØ§Ø±Ø© Ù…ÙˆÙ‚Ø¹Ù†Ø§ Ù„Ù„Ø§Ø·Ù„Ø§Ø¹ Ø¹Ù„Ù‰ <a href=\"{$url}\">Ø§Ù„Ø¥Ø¹Ù„Ø§Ù† Ø¨Ø´ÙƒÙ„Ù‡ Ø§Ù„ÙƒØ§Ù…Ù„</a>.', 'Ù‡Ø°Ù‡ Ø§Ù„Ø±Ø³Ø§Ù„Ø© ØªÙØ±Ø³Ù„ Ø¥Ù„Ù‰ Ø§Ù„Ù…Ø´ØªØ±ÙƒÙŠÙ† ÙÙŠ Ø§Ù„Ù…ÙˆÙ‚Ø¹ Ø¹Ù†Ø¯ Ø¥Ù†Ø´Ø§Ø¡ Ø¥Ø¹Ù„Ø§Ù† Ø¬Ø¯ÙŠØ¯.'),
('ANNOUNCEMENT', 'ca_ES', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nVisiteu el nostre lloc web per llegir <a href=\"{$url}\">l\'avÃ­s complet</a>.', 'Aquest correu electrÃ²nic s\'envia quan es crea un avÃ­s nou.'),
('ANNOUNCEMENT', 'cs_CZ', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nNavÅ¡tivte naÅ¡i webovou strÃ¡nku, pokud si chcete pÅ™eÄÃ­st <a href=\"{$url}\">plnÃ½ text</a>.', 'Tento email je posÃ­lÃ¡n, pokud je vytvoÅ™eno novÃ© oznÃ¡menÃ­.'),
('ANNOUNCEMENT', 'da_DK', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nBesÃ¸g vores websted  <a href=\"{$url}\">for at lÃ¦se hele meddelelsen</a>.', 'Denne e-mail sendes, nÃ¥r der oprettes en ny meddelelse.'),
('ANNOUNCEMENT', 'en_US', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nVisit our website to read the <a href=\"{$url}\">full announcement</a>.', 'This email is sent when a new announcement is created.'),
('ANNOUNCEMENT', 'es_ES', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nVisite nuestro site para leer el <a href=\"{$url}\">anuncio completo</a>.', 'Se mandarÃ¡ este email tras la creaciÃ³n de un nuevo anuncio.'),
('ANNOUNCEMENT', 'fr_CA', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nVisiter notre site Web pour consulter <a href=\"{$url}\">l\'annonce complÃ¨te</a>.', 'Ce courriel est envoyÃ© lorsqu\'une nouvelle annonce est crÃ©Ã©e.'),
('ANNOUNCEMENT', 'id_ID', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nKunjungi website kami untuk melihat <a href=\"{$url}\">pengumuman selengkapnya</a>.', 'Surel ini dikirim ketika terdapat pengumuman baru.'),
('ANNOUNCEMENT', 'it_IT', '{$titolo}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nVisita il nostro sito per leggere la  <a href=\"{$url}\">news completa</a>.', 'Questo messaggio Ã¨ inviato quando viene creata una nuova news.'),
('ANNOUNCEMENT', 'pt_BR', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nVisite nosso site para ler o <a href=\"{$url}\"> comunicado completo </a>.', 'Este email Ã© enviado quando um novo comunicado Ã© criado.'),
('ANNOUNCEMENT', 'pt_PT', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nVisite o nosso site para ler a <a href=\"{$url}\">notÃ­cia completa</a>.', 'Este email Ã© enviado quando uma nova notÃ­cia Ã© criada.'),
('ANNOUNCEMENT', 'ru_RU', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nÐŸÐ¾ÑÐµÑ‚Ð¸Ñ‚Ðµ Ð½Ð°Ñˆ Ð²ÐµÐ±-ÑÐ°Ð¹Ñ‚, Ñ‡Ñ‚Ð¾Ð±Ñ‹ Ð¿Ñ€Ð¾Ñ‡Ð¸Ñ‚Ð°Ñ‚ÑŒ <a href=\"{$url}\">Ð¾Ð±ÑŠÑÐ²Ð»ÐµÐ½Ð¸Ðµ Ð¿Ð¾Ð»Ð½Ð¾ÑÑ‚ÑŒÑŽ</a>.', 'Ð­Ñ‚Ð¾ Ð¿Ð¸ÑÑŒÐ¼Ð¾ Ð¾Ñ‚Ð¿Ñ€Ð°Ð²Ð»ÑÐµÑ‚ÑÑ Ð¿Ñ€Ð¸ ÑÐ¾Ð·Ð´Ð°Ð½Ð¸Ð¸ Ð½Ð¾Ð²Ð¾Ð³Ð¾ Ð¾Ð±ÑŠÑÐ²Ð»ÐµÐ½Ð¸Ñ.'),
('ANNOUNCEMENT', 'sl_SI', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nObiÅ¡Äite naÅ¡o spletno stran, da vidite <a href=\"{$url}\">celotno obvestilo</a>.', 'Email je poslan, ko je objavljeno novo obvestilo.'),
('ANNOUNCEMENT', 'sv_SE', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nBesÃ¶k vÃ¥r webbsida fÃ¶r att lÃ¤sa <a href=\"{$url}\">hela meddelandet</a>.', 'Detta e-post-meddelande skickas nÃ¤r ett nytt publicerat meddelande skapas.'),
('ANNOUNCEMENT', 'tr_TR', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nWeb sitesini ziyaret et ve <a href=\"{$url}\">tÃ¼m duyuruyu</a> oku.', 'Bu e-posta, yeni bir duyuru oluÅŸturulduÄŸunda gÃ¶nderilir.'),
('ANNOUNCEMENT', 'vi_VN', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nGhÃ© thÄƒm trang web cá»§a chÃºng tÃ´i Ä‘á»ƒ Ä‘á»c <a href=\"{$url}\">thÃ´ng bÃ¡o Ä‘áº§y Ä‘á»§</a>.', 'Email nÃ y Ä‘Æ°á»£c gá»­i khi má»™t thÃ´ng bÃ¡o má»›i Ä‘Æ°á»£c táº¡o.'),
('ANNOUNCEMENT', 'zh_CN', '{$title}', '<b>{$title}</b><br />\n<br />\n{$summary}<br />\n<br />\nè®¿é—®æˆ‘ä»¬çš„ç½‘ç«™æŸ¥çœ‹<a href=\"{$url}\">å…¬å‘Šå…¨æ–‡</a>ã€‚.', 'æ­¤é‚®ä»¶å½“å…¬å‘Šå‘å¸ƒæ—¶å°†ä¼šè‡ªåŠ¨å‘é€ã€‚'),
('CITATION_EDITOR_AUTHOR_QUERY', 'en_US', 'Citation Editing', '{$authorFirstName},<br />\n<br />\nCould you please verify or provide us with the proper citation for the following reference from your article, {$submissionTitle}:<br />\n<br />\n{$rawCitation}<br />\n<br />\nThanks!<br />\n<br />\n{$userFirstName}<br />\nCopy-Editor, {$contextName}<br />\n', 'This email allows copyeditors to request additional information about references from authors.'),
('CITATION_EDITOR_AUTHOR_QUERY', 'id_ID', 'Penyuntingan Sitiran', '{$authorFirstName},<br />\n<br />\nMohon verifikasi atau berikan sitasi yang benar untuk rujukan berikut dari artikel Anda, {$submissionTitle}:<br />\n<br />\n{$rawCitation}<br />\n<br />\nTerimakasih!<br />\n<br />\n{$userFirstName}<br />\nCopy-Editor, {$contextName}<br />\n', 'Email ini memungkinkan copyeditor untuk meminta informasi tambahan tentang rujukan dari penulis.'),
('COPYEDIT_REQUEST', 'en_US', 'Copyediting Request', '{$participantName}:<br />\n<br />\nI would ask that you undertake the copyediting of &quot;{$submissionTitle}&quot; for {$contextName} by following these steps.<br />\n1. Click on the Submission URL below.<br />\n2. Open any files available under Draft Files and do your copyediting, while adding any Copyediting Discussions as needed.<br />\n3. Save copyedited file(s), and upload to Copyedited panel.<br />\n4. Notify the Editor that all files have been prepared, and that the Production process may begin.<br />\n<br />\n{$contextName} URL: {$contextUrl}<br />\nSubmission URL: {$submissionUrl}<br />\nUsername: {$participantUsername}', 'This email is sent by a Section Editor to a submission\'s Copyeditor to request that they begin the copyediting process. It provides information about the submission and how to access it.'),
('COPYEDIT_REQUEST', 'id_ID', 'Permohonan Copyediting', '{$participantName}:<br />\n<br />\nKami meminta Anda melakukan copyediting terhadap &quot;{$submissionTitle}&quot; untuk {$contextName} mengikuti langkah-langkah berikut.<br />\n1. Klik URL Naskah di bawah.<br />\n2. Buka semua file yang ada di file Draft dan lakukan copyediting, tambahkan Diskusi Copyediting sesuai kebutuhan.<br />\n3. Simpan file yang telah di-copyedit, dan unggah ke panel Sudah Copyedit.<br />\n4. Beritahu Editor bahwa semua file telah siap, dan bahwa proses Produksi dapat dimulai.<br />\n<br />\n{$contextName} URL: {$contextUrl}<br />\nURL Naskah: {$submissionUrl}<br />\nNama pengguna: {$participantUsername}', 'Email ini dari Editor Bagian ke Copyeditor naskah agar memulai proses copyediting.  Email ini memuat informasi terkait penyerahan naskah dan bagaimana mengaksesnya.'),
('EDITOR_ASSIGN', 'en_US', 'Editorial Assignment', '{$editorialContactName}:<br />\n<br />\nThe submission, &quot;{$submissionTitle},&quot; to {$contextName} has been assigned to you to see through the editorial process in your role as Section Editor.<br />\n<br />\nSubmission URL: {$submissionUrl}<br />\nUsername: {$editorUsername}<br />\n<br />\nThank you.', 'This email notifies a Section Editor that the Editor has assigned them the task of overseeing a submission through the editing process. It provides information about the submission and how to access the journal site.'),
('EDITOR_ASSIGN', 'id_ID', 'Penugasan Editorial', '{$editorialContactName}:<br />\n<br />\nNaskah, &quot;{$submissionTitle},&quot; di {$contextName} telah ditugaskan kepada Anda sebagai Editor Bagian untuk memastikan selesainya seluruh proses editorial.<br />\n<br />\nURL Naskah: {$submissionUrl}<br />\nNama pengguna: {$editorUsername}<br />\n<br />\nTerimakasih.', 'Email ini memberitahu Editor Bagian bahwa Editor telah memberikan tugas untuk mengawasi suatu naskah dalam menyelesaikan proses editorial. Email ini memberikan informasi mengenai naskah dan cara mengaksesnya di website jurnal.'),
('EDITOR_DECISION_ACCEPT', 'en_US', 'Editor Decision', '{$authorName}:<br />\n<br />\nWe have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nOur decision is to: Accept Submission', 'This email from the Editor or Section Editor to an Author notifies them of a final \"accept submission\" decision regarding their submission.'),
('EDITOR_DECISION_ACCEPT', 'id_ID', 'Keputusan Editor', '{$authorName}:<br />\n<br />\nKami telah membuat keputusan terkait naskah yang Anda kirimkan ke {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nKeputusan kami adalah: Naskah Diterima', 'Email ini dari Editor atau Editor Bagian kepada Penulis memberitahukan keputusan final \"Naskah Diterima\".'),
('EDITOR_DECISION_DECLINE', 'en_US', 'Editor Decision', '{$authorName}:<br />\n<br />\nWe have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nOur decision is to: Decline Submission', 'This email from the Editor or Section Editor to an Author notifies them of a final \"decline\" decision regarding their submission.'),
('EDITOR_DECISION_DECLINE', 'id_ID', 'Keputusan Editor', '{$authorName}:<br />\n<br />\nKami telah membuat keputusan terkait naskah yang Anda kirimkan ke {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nKeputusan kami adalah: Naskah Ditolak', 'Email ini dari Editor atau Editor Bagian kepada Penulis memberitahukan keputusan final \"Naskah Ditolak\".'),
('EDITOR_DECISION_INITIAL_DECLINE', 'en_US', 'Editor Decision', '\n			{$authorName}:<br />\n<br />\nWe have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nOur decision is to: Decline Submission', 'This email is sent to the author if the editor declines their submission initially, before the review stage'),
('EDITOR_DECISION_INITIAL_DECLINE', 'id_ID', 'Keputusan Penyunting', '\n			{$authorName}:<br />\n<br />\nKami telah sampai pada keputusan mengenai naskah Anda {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nKeputusannya adalah: Menolak Naskah', 'Email ini dikirim kepada penulis jika naskah mereka ditolak oleh editor lebih awal, sebelum masuk ke tahap review'),
('EDITOR_DECISION_RESUBMIT', 'en_US', 'Editor Decision', '{$authorName}:<br />\n<br />\nWe have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nOur decision is to: Resubmit for Review', 'This email from the Editor or Section Editor to an Author notifies them of a final \"resubmit\" decision regarding their submission.'),
('EDITOR_DECISION_RESUBMIT', 'id_ID', 'Keputusan Editor', '{$authorName}:<br />\n<br />\nKami telah membuat keputusan terkait naskah yang Anda kirimkan ke {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nKeputusan kami adalah: Kirim Ulang untuk Review', 'Email ini dari Editor atau Editor Bagian kepada Penulis memberitahukan bahwa Penulis perlu mengirim ulang naskahnya.'),
('EDITOR_DECISION_REVISIONS', 'en_US', 'Editor Decision', '{$authorName}:<br />\n<br />\nWe have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nOur decision is: Revisions Required', 'This email from the Editor or Section Editor to an Author notifies them of a final \"revisions required\" decision regarding their submission.'),
('EDITOR_DECISION_REVISIONS', 'id_ID', 'Keputusan Editor', '{$authorName}:<br />\n<br />\nKami telah membuat keputusan terkait naskah yang Anda kirimkan ke {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nKeputusan kami adalah: Perlu Revisi', 'Email ini dari Editor atau Editor Bagian kepada Penulis memberitahukan keputusan final \"Perlu Revisi\".'),
('EDITOR_DECISION_SEND_TO_EXTERNAL', 'en_US', 'Editor Decision', '{$authorName}:<br />\n<br />\nWe have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nOur decision is to: Send to Review<br />\n<br />\nSubmission URL: {$submissionUrl}', 'This email from the Editor or Section Editor to an Author notifies them that their submission is being sent to an external review.'),
('EDITOR_DECISION_SEND_TO_EXTERNAL', 'id_ID', 'Keputusan Editor', '{$authorName}:<br />\n<br />\nKami telah membuat keputusan terkait naskah yang Anda kirimkan ke {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nKeputusan kami adalah: Dikirimkan ke Reviewer Eksternal<br />\n<br />\nURL Naskah: {$submissionUrl}', 'Email ini dari Editor atau Editor Bagian kepada Penulis memberitahukan bahwa naskah dikirimkan ke reviewer eksternal.'),
('EDITOR_DECISION_SEND_TO_PRODUCTION', 'en_US', 'Editor Decision', '{$authorName}:<br />\n<br />\nThe editing of your submission, &quot;{$submissionTitle},&quot; is complete.  We are now sending it to production.<br />\n<br />\nSubmission URL: {$submissionUrl}', 'This email from the Editor or Section Editor to an Author notifies them that their submission is being sent to production.'),
('EDITOR_DECISION_SEND_TO_PRODUCTION', 'id_ID', 'Keputusan Editor', '{$authorName}:<br />\n<br />\nProses editing naskah Anda, &quot;{$submissionTitle},&quot; telah selesai.  Kami sekarang mengirimkannya ke produksi.<br />\n<br />\nURL Naskah: {$submissionUrl}', 'Email ini dari Editor atau Editor Bagian kepada Penulis memberitahukan bahwa naskah dikirim ke produksi.'),
('EDITOR_RECOMMENDATION', 'en_US', 'Editor Recommendation', '{$editors}:<br />\n<br />\nThe recommendation regarding the submission to {$contextName}, &quot;{$submissionTitle}&quot; is: {$recommendation}', 'This email from the recommending Editor or Section Editor to the decision making Editors or Section Editors notifies them of a final recommendation regarding the submission.'),
('EDITOR_RECOMMENDATION', 'id_ID', 'Rekomendasi Penyunting', '{$editors}:<br />\n<br />\nRekomendasi berkaitan dengan  naskah {$contextName}, &quot;{$submissionTitle}&quot; adalah: {$recommendation}', 'Email ini berasal dari Editor atau Editor Bagian kepada Editor pengambilan keputusan atau Editor Bagian memberi tahu mereka tentang rekomendasi akhir mengenai naskah tersebut.'),
('EMAIL_LINK', 'en_US', 'Article of Possible Interest', 'Thought you might be interested in seeing &quot;{$submissionTitle}&quot; by {$authorName} published in Vol {$volume}, No {$number} ({$year}) of {$contextName} at &quot;{$articleUrl}&quot;.', 'This email template provides a registered reader with the opportunity to send information about an article to somebody who may be interested. It is available via the Reading Tools and must be enabled by the Journal Manager in the Reading Tools Administration page.'),
('EMAIL_LINK', 'id_ID', 'Artikel Menarik', 'Anda mungkin tertarik dengan &quot;{$submissionTitle}&quot; oleh {$authorName} diterbitkan di Vol {$volume}, No {$number} ({$year}) {$contextName} yang tersedia di &quot;{$articleUrl}&quot;.', 'Template email ini memungkinkan pembaca terdaftar mengirimkan informasi tentang suatu artikel kepada seseorang yang mungkin akan tertarik. Email ini tersedia di Alat Baca dan harus diaktifkan oleh Manajer Jurnal di halaman Administrasi Alat Baca.'),
('LAYOUT_COMPLETE', 'en_US', 'Galleys Complete', '{$editorialContactName}:<br />\n<br />\nGalleys have now been prepared for the manuscript, &quot;{$submissionTitle},&quot; for {$contextName} and are ready for proofreading.<br />\n<br />\nIf you have any questions, please contact me.<br />\n<br />\n{$participantName}', 'This email from the Layout Editor to the Section Editor notifies them that the layout process has been completed.'),
('LAYOUT_COMPLETE', 'id_ID', 'Galley Selesai', '{$editorialContactName}:<br />\n<br />\nGalley telah dipersiapkan bagi naskah, &quot;{$submissionTitle},&quot; di {$contextName} dan siap untuk proofreading.<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami.<br />\n<br />\n{$participantName}', 'Email from the Layout Editor kepada Editor Bagian memberitahukan bahwa proses layout telah selesai.'),
('LAYOUT_REQUEST', 'en_US', 'Request Galleys', '{$participantName}:<br />\n<br />\nThe submission &quot;{$submissionTitle}&quot; to {$contextName} now needs galleys laid out by following these steps.<br />\n1. Click on the Submission URL below.<br />\n2. Log into the journal and use the Production Ready files to create the galleys according to the journal\'s standards.<br />\n3. Upload the galleys to the Galley Files section.<br />\n4. Notify the Editor using Production Discussions that the galleys are uploaded and ready.<br />\n<br />\n{$contextName} URL: {$contextUrl}<br />\nSubmission URL: {$submissionUrl}<br />\nUsername: {$participantUsername}<br />\n<br />\nIf you are unable to undertake this work at this time or have any questions, please contact me. Thank you for your contribution to this journal.', 'This email from the Section Editor to the Layout Editor notifies them that they have been assigned the task of performing layout editing on a submission. It provides information about the submission and how to access it.'),
('LAYOUT_REQUEST', 'id_ID', 'Permohonan Galley', '{$participantName}:<br />\n<br />\nNaskah &quot;{$submissionTitle}&quot; di {$contextName} memerlukan pembuatan galley dengan mengikuti langkah-langkah berikut.<br />\n1. Klik URL Naskah di bawah.<br />\n2. Login ke jurnal dan gunakan file Siap Produksi untuk membuat galley sesuai standar jurnal.<br />\n3. Unggah galley ke bagian File Galley.<br />\n4. Beritahu Editor menggunakan Diskusi Produksi bahwa galley telah diunggah dan siap digunakan.<br />\n<br />\n{$contextName} URL: {$contextUrl}<br />\nURL Naskah: {$submissionUrl}<br />\nNama pengguna: {$participantUsername}<br />\n<br />\nJika Anda tidak dapat melakukan tugas ini saat ini atau mempunyai pertanyaan, silakan hubungi kami.  Terimakasih atas kontribusi Anda ke jurnal ini.', 'Email dari Editor Bagian kepada Layout Editor memberitahukan bahwa mereka telah ditugaskan untuk melakukan layout editing terhadap suatu naskah.  Email ini memuat informasi terkait penyerahan naskah dan bagaimana mengaksesnya.'),
('LOCKSS_EXISTING_ARCHIVE', 'en_US', 'Archiving Request for {$contextName}', 'Dear [University Librarian]<br />\n<br />\n{$contextName} &amp;lt;{$contextUrl}&amp;gt;, is a journal for which a member of your faculty, [name of member], serves as a [title of position]. The journal is seeking to establish a LOCKSS (Lots of Copies Keep Stuff Safe) compliant archive with this and other university libraries.<br />\n<br />\n[Brief description of journal]<br />\n<br />\nThe URL to the LOCKSS Publisher Manifest for our journal is: {$contextUrl}/gateway/lockss<br />\n<br />\nWe understand that you are already participating in LOCKSS. If we can provide any additional metadata for purposes of registering our journal with your version of LOCKSS, we would be happy to provide it.<br />\n<br />\nThank you,<br />\n{$principalContactSignature}', 'This email requests the keeper of a LOCKSS archive to consider including this journal in their archive. It provides the URL to the journal\'s LOCKSS Publisher Manifest.'),
('LOCKSS_EXISTING_ARCHIVE', 'id_ID', 'Permohonan Pengarsipan untuk {$contextName}', 'Yang terhormat [Pustakawan Universitas]<br />\n<br />\n{$contextName} &amp;lt;{$contextUrl}&amp;gt;, merupakan jurnal yang salah satu anggota fakultas Anda, [name of member], menjadi [title of position]. Jurnal ini hendak membuat arsip sesuai LOCKSS (Lots of Copies Keep Stuff Safe) di perpustakaan Anda dan perpustakaan universitas lainnya.<br />\n<br />\n[Deskripsi singkat jurnal]<br />\n<br />\nURL Publisher Manifest LOCKSS jurnal kami adalah: {$contextUrl}/gateway/lockss<br />\n<br />\nKami memahami bahwa Anda telah bergabung dengan LOCKSS. Jika kami perlu memberikan metadata tambahan untuk mendaftarkan jurnal kami di versi LOCKSS Anda, dengan senang hati kami akan memberikannya.<br />\n<br />\nTerimakasih,<br />\n{$principalContactSignature}', 'Email ini meminta penjaga arsip LOCKSS untuk mempertimbangkan memasukkan jurnal ini ke dalam arsip mereka. Email ini memberikan URL Publisher Manifest LOCKSS jurnal.'),
('LOCKSS_NEW_ARCHIVE', 'en_US', 'Archiving Request for {$contextName}', 'Dear [University Librarian]<br />\n<br />\n{$contextName} &amp;lt;{$contextUrl}&amp;gt;, is a journal for which a member of your faculty, [name of member] serves as a [title of position]. The journal is seeking to establish a LOCKSS (Lots of Copies Keep Stuff Safe) compliant archive with this and other university libraries.<br />\n<br />\n[Brief description of journal]<br />\n<br />\nThe LOCKSS Program &amp;lt;http://lockss.org/&amp;gt;, an international library/publisher initiative, is a working example of a distributed preservation and archiving repository, additional details are below. The software, which runs on an ordinary personal computer is free; the system is easily brought on-line; very little ongoing maintenance is required.<br />\n<br />\nTo assist in the archiving of our journal, we invite you to become a member of the LOCKSS community, to help collect and preserve titles produced by your faculty and by other scholars worldwide. To do so, please have someone on your staff visit the LOCKSS site for information on how this system operates. I look forward to hearing from you on the feasibility of providing this archiving support for this journal.<br />\n<br />\nThank you,<br />\n{$principalContactSignature}', 'This email encourages the recipient to participate in the LOCKSS initiative and include this journal in the archive. It provides information about the LOCKSS initiative and ways to become involved.'),
('LOCKSS_NEW_ARCHIVE', 'id_ID', 'Permohonan Pengarsipan untuk {$contextName}', 'Yang terhormat [University Librarian]<br />\n<br />\n{$contextName} &amp;lt;{$contextUrl}&amp;gt;, merupakan jurnal yang salah satu anggota fakultas Anda, [name of member], menjadi [title of position]. Jurnal ini hendak membuat arsip sesuai LOCKSS (Lots of Copies Keep Stuff Safe) di perpustakaan Anda dan perpustakaan universitas lainnya.<br />\n<br />\n[Deskripsi singkat jurnal]<br />\n<br />\nProgram LOCKSS &amp;lt;http://lockss.org/&amp;gt;, suatu gerakan perpustakaan/penerbit internasional, merupakan contoh nyata penyimpanan dan pengarsipan terdistribusi, detail tambahan dapat dilihat di bawah. Piranti lunak ini, yang berjalan di komputer biasa, merupakan piranti lunak yang gratis; sistemnya mudah di-online-kan; tidak membutuhkan banyak perawatan.<br />\n<br />\nUntuk membantu mengarsipkan jurnal kami, kami mengundang Anda untuk menjadi anggota komunitas LOCKSS, guna membantu mengumpulkan dan menyimpan judul-judul karya anggota fakultas Anda dan ilmuwan lainnya di seluruh dunia. Jika berkenan, silakan mengunjungi website LOCKSS untuk informasi mengenai bagaimana cara kerja sistem ini. Kami berharap Anda berkenan mendukung pengarsipan jurnal ini.<br />\n<br />\nTerimakasih,<br />\n{$principalContactSignature}', 'Email ini mendorong penerima untuk berpartisipasi di gerakan LOCKSS dan memasukkan jurnal ini dalam arsip. Email ini memberikan informasi tentang gerakan LOCKSS dan cara untuk bergabung.'),
('MANUAL_PAYMENT_NOTIFICATION', 'en_US', 'Manual Payment Notification', 'A manual payment needs to be processed for the journal {$contextName} and the user {$userFullName} (username &quot;{$userName}&quot;).<br />\n<br />\nThe item being paid for is &quot;{$itemName}&quot;.<br />\nThe cost is {$itemCost} ({$itemCurrencyCode}).<br />\n<br />\nThis email was generated by Open Journal Systems\' Manual Payment plugin.', 'This email template is used to notify a journal manager contact that a manual payment was requested.'),
('MANUAL_PAYMENT_NOTIFICATION', 'id_ID', 'Pemberitahuan Pembayaran Manual', 'Pembayaran manual harus diproses untuk jurnal {$contextName} dan pengguna {$userFullName} (username &quot;{$userName}&quot;).<br />\n<br />\nItem yang akan dibayar adalah &quot;{$itemName}&quot;.<br />\nBiayanya {$itemCost} ({$itemCurrencyCode}).<br />\n<br />\nSurat elektronik ini dibuat oleh plugin Pembayaran Manual OJS.', 'Email ini digunakan untuk memberitahukan kontak manajer jurnal bahwa pembayaran manual dibutuhkan.'),
('NOTIFICATION', 'en_US', 'New notification from {$siteTitle}', 'You have a new notification from {$siteTitle}:<br />\n<br />\n{$notificationContents}<br />\n<br />\nLink: {$url}<br />\n<br />\n{$principalContactSignature}', 'The email is sent to registered users that have selected to have this type of notification emailed to them.'),
('NOTIFICATION', 'id_ID', 'Notifikasi baru dari {$siteTitle}', 'Anda memperoleh satu notifikasi baru dari {$siteTitle}:<br />\n<br />\n{$notificationContents}<br />\n<br />\nTautan: {$url}<br />\n<br />\n{$principalContactSignature}', 'Email ini dikirimkan ke pengguna terdaftar yang memilih untuk memperoleh jenis notifikasi ini dikirimkan melalui email.'),
('NOTIFICATION_CENTER_DEFAULT', 'en_US', 'A message regarding {$contextName}', 'Please enter your message.', 'The default (blank) message used in the Notification Center Message Listbuilder.'),
('NOTIFICATION_CENTER_DEFAULT', 'id_ID', 'Pesan terkait {$contextName}', 'Silakan tuliskan pesan Anda.', 'Pesan default (kosong) yang digunakan di Pengelola Pesan Pusat Notifikasi.'),
('OPEN_ACCESS_NOTIFY', 'en_US', 'Issue Now Open Access', 'Readers:<br />\n<br />\n{$contextName} has just made available in an open access format the following issue. We invite you to review the Table of Contents here and then visit our web site ({$contextUrl}) to review articles and items of interest.<br />\n<br />\nThanks for the continuing interest in our work,<br />\n{$editorialContactSignature}', 'This email is sent to registered readers who have requested to receive a notification email when an issue becomes open access.'),
('OPEN_ACCESS_NOTIFY', 'id_ID', 'Terbitan Sudah Open Access', 'Readers:<br />\n<br />\n{$contextName} telah menjadikan terbitan berikut Open Access. Kami mengundang Anda untuk menelaah Daftar Isi berikut dan mengunjungi website kami ({$contextUrl}) untuk membaca artikel dan item yang Anda minati.<br />\n<br />\nTerimakasih atas perhatiannya,<br />\n{$editorialContactSignature}', 'Email ini dikirimkan ke pembaca terdaftar yang meminta untuk dikirimkan email pemberitahuan saat suatu terbitan menjadi Open Access.'),
('ORCID_COLLECT_AUTHOR_ID', 'en_US', 'Submission ORCID', 'Dear {$authorName},<br/>\n<br/>\nYou have been listed as an author on a manuscript submission to {$contextName}.<br/>\nTo confirm your authorship, please add your ORCID id to this submission by visiting the link provided below.<br/>\n<br/>\n<a href=\"{$authorOrcidUrl}\"><img id=\"orcid-id-logo\" src=\"https://orcid.org/sites/default/files/images/orcid_16x16.png\" width=\'16\' height=\'16\' alt=\"ORCID iD icon\" style=\"display: block; margin: 0 .5em 0 0; padding: 0; float: left;\"/>Register or connect your ORCID iD</a><br/>\n<br/>\n<br>\n<a href=\"{$orcidAboutUrl}\">More information about ORCID at {$contextName}</a><br/>\n<br/>\nIf you have any questions, please contact me.<br/>\n<br/>\n{$principalContactSignature}<br/>\n', 'This email template is used to collect the ORCID id\'s from authors.'),
('ORCID_COLLECT_AUTHOR_ID', 'id_ID', 'ORCID Naskah', 'Yang Kami Hormati {$authorName},<br/>\n<br/>\nAnda telah terdaftar sebagai penulis naskah{$contextName}.<br/>\nUntuk mengonfirmasi kepenulisan tersebut, tambahkanlah id ORCID Anda pada naskah tersebut dengan membuka tautan berikut ini.<br/>\n<br/>\n<a href=\"{$authorOrcidUrl}\"><img id=\"orcid-id-logo\" src=\"https://orcid.org/sites/default/files/images/orcid_16x16.png\" width=\'16\' height=\'16\' alt=\"ORCID iD icon\" style=\"display: block; margin: 0 .5em 0 0; padding: 0; float: left;\"/>Register atau hubungkan iD ORCID Anda</a><br/>\n<br/>\n<br>\n<a href=\"{$orcidAboutUrl}\">Informasi selengkapnya tentang ORCID pada {$contextName}</a><br/>\n<br/>\nBila ada pertanyaan, silakan hubungi kami.<br/>\n<br/>\n{$principalContactSignature}<br/>\n', 'Template surel ini digunakan untuk mengumpulkan id ORCID penulis.'),
('ORCID_REQUEST_AUTHOR_AUTHORIZATION', 'en_US', 'Requesting ORCID record access', 'Dear {$authorName},<br>\n<br>\nYou have been listed as an author on the manuscript submission \"{$submissionTitle}\" to {$contextName}.\n<br>\n<br>\nPlease allow us to add your ORCID id to this submission and also to add the submission to your ORCID profile on publication.<br>\nVisit the link to the official ORCID website, login with your profile and authorize the access by following the instructions.<br>\n<a href=\"{$authorOrcidUrl}\"><img id=\"orcid-id-logo\" src=\"https://orcid.org/sites/default/files/images/orcid_16x16.png\" width=\'16\' height=\'16\' alt=\"ORCID iD icon\" style=\"display: block; margin: 0 .5em 0 0; padding: 0; float: left;\"/>Register or Connect your ORCID iD</a><br/>\n<br>\n<br>\n<a href=\"{$orcidAboutUrl}\">More about ORCID at {$contextName}</a><br/>\n<br>\nIf you have any questions, please contact me.<br>\n<br>\n{$principalContactSignature}<br>\n', 'This email template is used to request ORCID record access from authors.'),
('ORCID_REQUEST_AUTHOR_AUTHORIZATION', 'id_ID', 'Meminta akses rekaman ORCID', 'Yang Kami Hormati {$authorName},<br>\n<br>\nAnda telah terdaftar sebagai penulis naskah \"{$submissionTitle}\" pada {$contextName}.\n<br>\n<br>\nIjinkanlah kami menambahkan id ORCID Anda pada naskah ini serta menambahkan naskah pada profil ORCID Anda pada terbitan.<br>\nBuka tautan web ORCID, login dan beri akses sesuai petunjuk berikut.<br>\n<a href=\"{$authorOrcidUrl}\"><img id=\"orcid-id-logo\" src=\"https://orcid.org/sites/default/files/images/orcid_16x16.png\" width=\'16\' height=\'16\' alt=\"ORCID iD icon\" style=\"display: block; margin: 0 .5em 0 0; padding: 0; float: left;\"/>Register atau hubungkan iD ORCID Anda</a><br/>\n<br>\n<br>\n<a href=\"{$orcidAboutUrl}\">nformasi selengkapnya tentang ORCID pada {$contextName}</a><br/>\n<br>\nBila ada pertanyaan, silakan hubungi kami.<br>\n<br>\n{$principalContactSignature}<br/>\n', 'Template surel ini digunakan untuk meminta akses rekaman ORCID penulis.'),
('PASSWORD_RESET', 'en_US', 'Password Reset', 'Your password has been successfully reset for use with the {$siteTitle} web site. Please retain this username and password, as it is necessary for all work with the journal.<br />\n<br />\nYour username: {$username}<br />\nPassword: {$password}<br />\n<br />\n{$principalContactSignature}', 'This email is sent to a registered user when they have successfully reset their password following the process described in the PASSWORD_RESET_CONFIRM email.'),
('PASSWORD_RESET', 'id_ID', 'Reset Sandi', 'Sandi akun Anda di website {$siteTitle} telah berhasil di-reset. Silakan simpan nama pengguna dan sandi ini karena akan dibutuhkan untuk semua kegiatan terkait jurnal di website ini.<br />\n<br />\nNama pengguna Anda: {$username}<br />\nSandi: {$password}<br />\n<br />\n{$principalContactSignature}', 'Email ini dikirimkan ke pengguna terdaftar saat proses reset password berhasil sesuai prosedur di email PASSWORD_RESET_CONFIRM.'),
('PASSWORD_RESET_CONFIRM', 'en_US', 'Password Reset Confirmation', 'We have received a request to reset your password for the {$siteTitle} web site.<br />\n<br />\nIf you did not make this request, please ignore this email and your password will not be changed. If you wish to reset your password, click on the below URL.<br />\n<br />\nReset my password: {$url}<br />\n<br />\n{$principalContactSignature}', 'This email is sent to a registered user when they indicate that they have forgotten their password or are unable to login. It provides a URL they can follow to reset their password.'),
('PASSWORD_RESET_CONFIRM', 'id_ID', 'Konfirmasi Reset Sandi', 'Kami menerima permintaan reset sandi untuk akun Anda di website {$siteTitle}.<br />\n<br />\nJika Anda tidak merasa mengajukan permintaan ini, abaikan pesan ini dan sandi Anda tidak akan diubah.  Jika Anda memang ingin melakukan reset sandi, klik tautan berikut ini.<br />\n<br />\nReset sandi saya: {$url}<br />\n<br />\n{$principalContactSignature}', 'Email ini dikirimkan ke pengguna terdaftar ketika lupa sandi atau tidak dapat login. Email ini memberikan URL untuk melakukan reset sandi.'),
('PAYPAL_INVESTIGATE_PAYMENT', 'en_US', 'Unusual PayPal Activity', 'Open Journal Systems has encountered unusual activity relating to PayPal payment support for the journal {$contextName}. This activity may need further investigation or manual intervention.<br />\n                       <br />\nThis email was generated by Open Journal Systems\' PayPal plugin.<br />\n<br />\nFull post information for the request:<br />\n{$postInfo}<br />\n<br />\nAdditional information (if supplied):<br />\n{$additionalInfo}<br />\n<br />\nServer vars:<br />\n{$serverVars}<br />\n', 'This email template is used to notify a journal\'s primary contact that suspicious activity or activity requiring manual intervention was encountered by the PayPal plugin.'),
('PAYPAL_INVESTIGATE_PAYMENT', 'id_ID', 'Kegiatan pembayaran paypal yang tidak biasa', 'Open Journal System telah menemukan kegiatan yang tidak biasa yang terkait dengan bantuan pembayaran paypal untuk jurnal {$contextName}. Kegiatan ini mungkin memerlukan investigasi lebih lanjut atau intervensi manual.<br />\n                       <br />\nSurel ini dibuat oleh plugin PayPal pada OJS.<br />\n<br />\nInformasi posting penuh untuk permohonan:<br />\n{$postInfo}<br />\n<br />\nInformasi tambahan (jika tersedia):<br />\n{$additionalInfo}<br />\n<br />\nServer vars:<br />\n{$serverVars}<br />\n', 'Email ini digunakan untuk memberitahukan narahubung utama jurnal bahwa plugin PayPal menemukan kegiatan mencurigakan atau kegiatan yang membutuhkan intervensi manual.'),
('PUBLISH_NOTIFY', 'en_US', 'New Issue Published', 'Readers:<br />\n<br />\n{$contextName} has just published its latest issue at {$contextUrl}. We invite you to review the Table of Contents here and then visit our web site to review articles and items of interest.<br />\n<br />\nThanks for the continuing interest in our work,<br />\n{$editorialContactSignature}', 'This email is sent to registered readers via the \"Notify Users\" link in the Editor\'s User Home. It notifies readers of a new issue and invites them to visit the journal at a supplied URL.'),
('PUBLISH_NOTIFY', 'id_ID', 'Terbitan Baru', 'Readers:<br />\n<br />\n{$contextName} baru saja menerbitkan terbitan terbarunya di {$contextUrl}. Kami mengundang Anda untuk membaca Daftar Isi dan selanjutnya mengunjungi website kami untuk membaca artikel yang Anda minati.<br />\n<br />\nTerimakasih atas perhatian Anda,<br />\n{$editorialContactSignature}', 'Email ini dikirimkan ke pembaca terdaftar melalui tautan \"Beritahu Pengguna\" di Beranda Pengguna Editor. Email ini memberitahu pembaca tentang terbitan baru dan mengundang pembaca untuk mengunjungi jurnal melalui URL yang diberikan.'),
('REVIEWER_REGISTER', 'en_US', 'Registration as Reviewer with {$contextName}', 'In light of your expertise, we have taken the liberty of registering your name in the reviewer database for {$contextName}. This does not entail any form of commitment on your part, but simply enables us to approach you with a submission to possibly review. On being invited to review, you will have an opportunity to see the title and abstract of the paper in question, and you\'ll always be in a position to accept or decline the invitation. You can also ask at any point to have your name removed from this reviewer list.<br />\n<br />\nWe are providing you with a username and password, which is used in all interactions with the journal through its website. You may wish, for example, to update your profile, including your reviewing interests.<br />\n<br />\nUsername: {$username}<br />\nPassword: {$password}<br />\n<br />\nThank you,<br />\n{$principalContactSignature}', 'This email is sent to a newly registered reviewer to welcome them to the system and provide them with a record of their username and password.'),
('REVIEWER_REGISTER', 'id_ID', 'Registrasi sebagai Reviewer di {$contextName}', 'Dengan mempertimbangkan keahlian Anda, kami memasukkan nama Anda dalam database reviewer di {$contextName}. Hal ini tidak bersifat mengikat, hanya sekedar memudahkan kami untuk mengundang Anda untuk melakukan review terhadap suatu naskah. Ketika memperoleh undangan untuk melakukan review suatu naskah, Anda dapat melihat judul dan abstrak naskah tersebut, dan Anda berhak menentukan apakah akan menerima atau menolak undangan tersebut. Anda juga dapat meminta dihapus dari daftar reviewer kapan saja Anda menghendaki.<br />\n<br />\nKami menyertakan nama pengguna dan sandi Anda, yang digunakan dalam semua interaksi dengan jurnal melalui website. Anda dapat melakukan update profil, termasuk minat review Anda.<br />\n<br />\nNama pengguna: {$username}<br />\nSandi: {$password}<br />\n<br />\nTerimakasih,<br />\n{$principalContactSignature}', 'Email ini dikirimkan ke reviewer baru untuk menyambut dan memberikan informasi nama pengguna dan sandi.'),
('REVIEW_ACK', 'en_US', 'Article Review Acknowledgement', '{$reviewerName}:<br />\n<br />\nThank you for completing the review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We appreciate your contribution to the quality of the work that we publish.', 'This email is sent by a Section Editor to confirm receipt of a completed review and thank the reviewer for their contributions.'),
('REVIEW_ACK', 'id_ID', 'Ucapan Terimakasih atas Review Artikel', '{$reviewerName}:<br />\n<br />\nTerimakasih telah menyelesaikan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Kami sangat menghargai kontribusi Anda terhadap kualitas karya yang kami publikasikan.', 'Email ini dikirimkan Editor Bagian untuk mengkonfirmasi penerimaan review dan mengucapkan terimakasih atas kontribusinya.'),
('REVIEW_CANCEL', 'en_US', 'Request for Review Cancelled', '{$reviewerName}:<br />\n<br />\nWe have decided at this point to cancel our request for you to review the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We apologize for any inconvenience this may cause you and hope that we will be able to call on you to assist with this journal\'s review process in the future.<br />\n<br />\nIf you have any questions, please contact me.', 'This email is sent by the Section Editor to a Reviewer who has a submission review in progress to notify them that the review has been cancelled.'),
('REVIEW_CANCEL', 'id_ID', 'Permohonan Review Dibatalkan', '{$reviewerName}:<br />\n<br />\nKami memutuskan untuk membatalkan permohonan review kami kepada Anda untuk naskah, &quot;{$submissionTitle},&quot; di {$contextName}. Kami mohon maaf sebesar-besarnya untuk ketidaknyamanan ini dan kami harap di masa mendatang Anda dapat membantu proses review di jurnal ini.<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami.', 'Email ini dikirimkan Editor Bagian kepada Reviewer yang sedang melakukan review untuk memberitahukan pembatalan review.'),
('REVIEW_CONFIRM', 'en_US', 'Able to Review', 'Editors:<br />\n<br />\nI am able and willing to review the submission, &quot;{$submissionTitle},&quot; for {$contextName}. Thank you for thinking of me, and I plan to have the review completed by its due date, {$reviewDueDate}, if not before.<br />\n<br />\n{$reviewerName}', 'This email is sent by a Reviewer to the Section Editor in response to a review request to notify the Section Editor that the review request has been accepted and will be completed by the specified date.'),
('REVIEW_CONFIRM', 'id_ID', 'Dapat Melakukan Review', 'Editor:<br />\n<br />\nSaya dapat dan bersedia melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Terimakasih telah mempercayakan kepada saya, dan saya berencana untuk menyelesaikan review ini sesuai tenggat, {$reviewDueDate}, atau sebelumnya.<br />\n<br />\n{$reviewerName}', 'Email ini dikirimkan Reviewer kepada Editor Bagian sebagai respon terhadap permohonan review untuk memberitahu Editor Bagian bahwa permohonan review diterima dan akan diselesaikan pada tanggal yang ditetapkan.'),
('REVIEW_DECLINE', 'en_US', 'Unable to Review', 'Editors:<br />\n<br />\nI am afraid that at this time I am unable to review the submission, &quot;{$submissionTitle},&quot; for {$contextName}. Thank you for thinking of me, and another time feel free to call on me.<br />\n<br />\n{$reviewerName}', 'This email is sent by a Reviewer to the Section Editor in response to a review request to notify the Section Editor that the review request has been declined.'),
('REVIEW_DECLINE', 'id_ID', 'Tidak Dapat Melakukan Review', 'Editor:<br />\n<br />\nMohon maaf saat ini saya tidak dapat melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Terimakasih telah mempercayakann kepada saya, dan lain waktu silakan menghubungi saya lagi.<br />\n<br />\n{$reviewerName}', 'Email ini dikirimkan Reviewer kepada Editor Bagian sebagai respon terhadap permohonan review untuk memberitahu Editor Bagian bahwa permohonan review ditolak.'),
('REVIEW_REINSTATE', 'en_US', 'Request for Review Reinstated', '{$reviewerName}:<br />\n<br />\nWe would like to reinstate our request for you to review the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We hope that you will be able to assist with this journal\'s review process.<br />\n<br />\nIf you have any questions, please contact me.', 'This email is sent by the Section Editor to a Reviewer who has a submission review in progress to notify them that a cancelled review has been reinstated.'),
('REVIEW_REINSTATE', 'id_ID', 'Meminta Pemulihan Review', '{$reviewerName}:<br />\n<br />\nKami ingin memulihkan permintaan kami kepada Anda untuk mereview naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Kami berharap Anda dapat membantu proses review jurnal kami.<br />\n<br />\nJika ada pertanyaan, silakan hubungi saya.', 'Email ini dikirim oleh Editor Bagian kepada Mitra Bestari yang sedang mereview naskah untuk memberi tahu bahwa ulasan mereka yang dibatalkan telah dipulihkan.'),
('REVIEW_REMIND', 'en_US', 'Submission Review Reminder', '{$reviewerName}:<br />\n<br />\nJust a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have this review by {$reviewDueDate}, and would be pleased to receive it as soon as you are able to prepare it.<br />\n<br />\nIf you do not have your username and password for the journal\'s web site, you can use this link to reset your password (which will then be emailed to you along with your username). {$passwordResetUrl}<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nPlease confirm your ability to complete this vital contribution to the work of the journal. I look forward to hearing from you.<br />\n<br />\n{$editorialContactSignature}', 'This email is sent by a Section Editor to remind a reviewer that their review is due.'),
('REVIEW_REMIND', 'id_ID', 'Pengingat Review Naskah', '{$reviewerName}:<br />\n<br />\nSekedar mengingatkan tentang permohonan kami untuk melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Kami mengharapkan telah menerima review ini pada {$reviewDueDate}, dan akan sangat berbahagia untuk dapat menerimanya segera setelah Anda menyelesaikannya.<br />\n<br />\nJika Anda tidak memiliki nama pengguna dan sandi untuk website jurnal ini, Anda dapat menggunakan tautan berikut untuk mereset sandi Anda (yang selanjutnya akan diemailkan kepada Anda bersama nama pengguna Anda). {$passwordResetUrl}<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nMohon konfirmasi kesanggupan Anda untuk menyelesaikan kontribusi penting ini.  Kami menunggu balasan Anda.<br />\n<br />\n{$editorialContactSignature}', 'Email ini dikirimkan Editor Bagian untuk mengingatkan reviewer bahwa review telah mencapai tenggat.'),
('REVIEW_REMIND_AUTO', 'en_US', 'Automated Submission Review Reminder', '{$reviewerName}:<br />\n<br />\nJust a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have this review by {$reviewDueDate}, and this email has been automatically generated and sent with the passing of that date. We would still be pleased to receive it as soon as you are able to prepare it.<br />\n<br />\nIf you do not have your username and password for the journal\'s web site, you can use this link to reset your password (which will then be emailed to you along with your username). {$passwordResetUrl}<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nPlease confirm your ability to complete this vital contribution to the work of the journal. I look forward to hearing from you.<br />\n<br />\n{$editorialContactSignature}', 'This email is automatically sent when a reviewer\'s due date elapses (see Review Options under Settings > Workflow > Review) and one-click reviewer access is disabled. Scheduled tasks must be enabled and configured (see the site configuration file).'),
('REVIEW_REMIND_AUTO', 'id_ID', 'Pengingat Otomatis Review Naskah', '{$reviewerName}:<br />\n<br />\nSekedar mengingatkan tentang permohonan kami untuk melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Kami mengharapkan telah menerima review ini pada {$reviewDueDate}, dan email ini dikirim secara otomatis seiring terlewatinya tanggal tersebut. Kami masih tetap menanti review tersebut segera setelah Anda dapat menyelesaikannya.<br />\n<br />\nJika Anda tidak memiliki nama pengguna dan sandi untuk website jurnal ini, Anda dapat menggunakan tautan berikut untuk mereset sandi Anda (yang selanjutnya akan diemailkan kepada Anda bersama nama pengguna Anda). {$passwordResetUrl}<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nMohon konfirmasi kesanggupan Anda untuk menyelesaikan kontribusi penting ini.  Kami menunggu balasan Anda.<br />\n<br />\n{$editorialContactSignature}', 'Email ini dikirimkan otomatis saat tenggat waktu review terlewati (lihat Opsi Review di menu Pengaturan > Alur Kerja > Review) and one-click reviewer access is disabled. Scheduled tasks must be enabled and configured (see the site configuration file).'),
('REVIEW_REMIND_AUTO_ONECLICK', 'en_US', 'Automated Submission Review Reminder', '{$reviewerName}:<br />\n<br />\nJust a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have this review by {$reviewDueDate}, and this email has been automatically generated and sent with the passing of that date. We would still be pleased to receive it as soon as you are able to prepare it.<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nPlease confirm your ability to complete this vital contribution to the work of the journal. I look forward to hearing from you.<br />\n<br />\n{$editorialContactSignature}', 'This email is automatically sent when a reviewer\'s due date elapses (see Review Options under Settings > Workflow > Review) and one-click reviewer access is enabled. Scheduled tasks must be enabled and configured (see the site configuration file).'),
('REVIEW_REMIND_AUTO_ONECLICK', 'id_ID', 'Pengingat Review Naskah Otomatis', '{$reviewerName}:<br />\n<br />\nSekedar mengingatkan tentang permohonan kami untuk melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. We were hoping to have this review by {$reviewDueDate}, dan email ini dikirim secara otomatis seiring terlewatinya tanggal tersebut. We would still be pleased to receive it as soon as you are able to prepare it.<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nMohon konfirmasi kesanggupan Anda untuk menyelesaikan kontribusi penting ini.  Kami menunggu balasan Anda.<br />\n<br />\n{$editorialContactSignature}', 'Email ini dikirimkan otomatis saat tenggat waktu review terlewati (lihat Opsi Review di menu Pengaturan > Alur Kerja > Review) dan akses one-click reviewer diaktifkan. Tugas terjadwal harus diaktifkan dan diatur (lihat file konfigurasi website).'),
('REVIEW_REMIND_ONECLICK', 'en_US', 'Submission Review Reminder', '{$reviewerName}:<br />\n<br />\nJust a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have this review by {$reviewDueDate}, and would be pleased to receive it as soon as you are able to prepare it.<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nPlease confirm your ability to complete this vital contribution to the work of the journal. I look forward to hearing from you.<br />\n<br />\n{$editorialContactSignature}', 'This email is sent by a Section Editor to remind a reviewer that their review is due.'),
('REVIEW_REMIND_ONECLICK', 'id_ID', 'Pengingat Review Naskah', '{$reviewerName}:<br />\n<br />\nSekedar mengingatkan tentang permohonan kami untuk melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Kami mengharapkan telah menerima review ini pada {$reviewDueDate}, dan akan sangat berbahagia untuk dapat menerimanya segera setelah Anda menyelesaikannya.<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nMohon konfirmasi kesanggupan Anda untuk menyelesaikan kontribusi penting ini.  Kami menunggu balasan Anda.<br />\n<br />\n{$editorialContactSignature}', 'Email ini dikirimkan Editor Bagian untuk mengingatkan reviewer bahwa review telah mencapai tenggat.');
INSERT INTO `email_templates_default_data` (`email_key`, `locale`, `subject`, `body`, `description`) VALUES
('REVIEW_REQUEST', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\n<br />\nI believe that you would serve as an excellent reviewer of the manuscript, &quot;{$submissionTitle},&quot; which has been submitted to {$contextName}. The submission\'s abstract is inserted below, and I hope that you will consider undertaking this important task for us.<br />\n<br />\nPlease log into the journal web site by {$responseDueDate} to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation. The web site is {$contextUrl}<br />\n<br />\nThe review itself is due {$reviewDueDate}.<br />\n<br />\nIf you do not have your username and password for the journal\'s web site, you can use this link to reset your password (which will then be emailed to you along with your username). {$passwordResetUrl}<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'This email from the Section Editor to a Reviewer requests that the reviewer accept or decline the task of reviewing a submission. It provides information about the submission such as the title and abstract, a review due date, and how to access the submission itself. This message is used when the Standard Review Process is selected in Management > Settings > Workflow > Review. (Otherwise see REVIEW_REQUEST_ATTACHED.)'),
('REVIEW_REQUEST', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\n<br />\nKami memandang bahwa Anda merupakan reviewer yang tepat untuk naskah, &quot;{$submissionTitle},&quot; yang diserahkan ke {$contextName}. Abstrak naskah tersebut disertakan di bawah ini, dan kami berharap Anda berkenan melakukannya.<br />\n<br />\nSilakan login ke website jurnal sebelum {$responseDueDate} untuk memberitahukan apakah Anda bersedia melakukan review atau tidak, juga untuk mengakses naskah dan menyerahkan review beserta rekomendasi Anda. Website jurnal adalah {$contextUrl}<br />\n<br />\nTenggat review ini sendiri adalah {$reviewDueDate}.<br />\n<br />\nJika Anda tidak memiliki nama pengguna dan sandi untuk website jurnal ini, Anda dapat menggunakan tautan berikut untuk mereset sandi Anda (yang selanjutnya akan diemailkan kepada Anda bersama nama pengguna Anda). {$passwordResetUrl}<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'Email ini dari Editor Bagian kepada Reviewer untuk meminta reviewer memberitahukan apakah menerima atau menolak permohonan review. Email ini memberikan informasi tentang naskah meliputi judul dan abstrak, tenggat review, dan cara mengakses naskah. Pesan ini digunakan saat Proses Review Standar dipilih di Manajemen > Pengaturan > Alur Kerja > Review. (Selain itu lihat di REVIEW_REQUEST_ATTACHED.)'),
('REVIEW_REQUEST_ATTACHED', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\n<br />\nI believe that you would serve as an excellent reviewer of the manuscript, &quot;{$submissionTitle},&quot; and I am asking that you consider undertaking this important task for us. The Review Guidelines for this journal are appended below, and the submission is attached to this email. Your review of the submission, along with your recommendation, should be emailed to me by {$reviewDueDate}.<br />\n<br />\nPlease indicate in a return email by {$responseDueDate} whether you are able and willing to do the review.<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n<br />\nReview Guidelines<br />\n<br />\n{$reviewGuidelines}<br />\n', 'This email is sent by the Section Editor to a Reviewer to request that they accept or decline the task of reviewing a submission. It includes the submission as an attachment. This message is used when the Email-Attachment Review Process is selected in Management > Settings > Workflow > Review. (Otherwise see REVIEW_REQUEST.)'),
('REVIEW_REQUEST_ATTACHED', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\n<br />\nKami memandang bahwa Anda merupakan reviewer yang tepat untuk naskah, &quot;{$submissionTitle},&quot; dan kami mengharapkan Anda berkenan melakukan review. Panduan Review untuk jurnal ini disertakan di bawah, dan naskah telah dilampirkan bersama email ini. Review beserta rekomendasi Anda, kami harap telah diemailkan sebelum tenggat review {$reviewDueDate}.<br />\n<br />\nMohon sampaikan dalam email balasan sebelum {$responseDueDate} tentang kesediaan Anda untuk melakukan review.<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n<br />\nPanduan Review<br />\n<br />\n{$reviewGuidelines}<br />\n', 'Email ini dari Editor Bagian kepada Reviewer untuk meminta reviewer memberitahukan apakah menerima atau menolak permohonan review. Email disertai naskah terlampir bersama email. Pesan ini digunakan saat Proses Review Lampiran Email dipilih di Manajemen > Pengaturan > Alur Kerja > Review. (Selain itu lihat REVIEW_REQUEST.)'),
('REVIEW_REQUEST_ATTACHED_SUBSEQUENT', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\n<br />\nThis regards the manuscript &quot;{$submissionTitle},&quot; which is under consideration by {$contextName}.<br />\n<br />\nFollowing the review of the previous version of the manuscript, the authors have now submitted a revised version of their paper. We would appreciate it if you could help evaluate it.<br />\n<br />\nThe Review Guidelines for this journal are appended below, and the submission is attached to this email. Your review of the submission, along with your recommendation, should be emailed to me by {$reviewDueDate}.<br />\n<br />\nPlease indicate in a return email by {$responseDueDate} whether you are able and willing to do the review.<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n<br />\nReview Guidelines<br />\n<br />\n{$reviewGuidelines}<br />\n', 'This email is sent by the Section Editor to a Reviewer to request that they accept or decline the task of reviewing a submission for a second or greater round of review. It includes the submission as an attachment. This message is used when the Email-Attachment Review Process is selected in Management > Settings > Workflow > Review. (Otherwise see REVIEW_REQUEST_SUBSEQUENT.)'),
('REVIEW_REQUEST_ATTACHED_SUBSEQUENT', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\n<br />\nEmail ini terkait naskah &quot;{$submissionTitle},&quot; yang sedang dalam pertimbangan di {$contextName}.<br />\n<br />\nSetelah review untuk versi naskah sebelumnya, penulis saat ini telah menyerahkan versi revisi naskahnya.  Kami berharap Anda dapat membantu mengevaluasinya.<br />\n<br />\nPanduan Review untuk jurnal ini disertakan di bawah, dan naskah dilampirkan bersama email ini. Review dan rekomendasi Anda, mohon dapat diemailkan ke kami pada {$reviewDueDate}.<br />\n<br />\nMohon sampaikan di email balasan sebelum {$responseDueDate} tentang kesediaan Anda untuk melakukan review.<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n<br />\nPanduan Review<br />\n<br />\n{$reviewGuidelines}<br />\n', 'Email ini dari Editor Bagian kepada Reviewer untuk meminta reviewer memberitahukan apakah menerima atau menolak permohonan review untuk ronde kedua atau berikutnya.  Email disertai naskah terlampir bersama email. Pesan ini digunakan saat Proses Review Lampiran Email dipilih di Manajemen > Pengaturan > Alur Kerja > Review. (Selain itu lihat REVIEW_REQUEST_SUBSEQUENT.)'),
('REVIEW_REQUEST_ONECLICK', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\n<br />\nI believe that you would serve as an excellent reviewer of the manuscript, &quot;{$submissionTitle},&quot; which has been submitted to {$contextName}. The submission\'s abstract is inserted below, and I hope that you will consider undertaking this important task for us.<br />\n<br />\nPlease log into the journal web site by {$responseDueDate} to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation.<br />\n<br />\nThe review itself is due {$reviewDueDate}.<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'This email from the Section Editor to a Reviewer requests that the reviewer accept or decline the task of reviewing a submission. It provides information about the submission such as the title and abstract, a review due date, and how to access the submission itself. This message is used when the Standard Review Process is selected in Management > Settings > Workflow > Review, and one-click reviewer access is enabled.'),
('REVIEW_REQUEST_ONECLICK', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\n<br />\nKami memandang bahwa Anda merupakan reviewer yang tepat untuk naskah, &quot;{$submissionTitle},&quot; yang diserahkan ke {$contextName}. Abstrak naskah tersebut disertakan di bawah ini, dan kami berharap Anda berkenan melakukannya.<br />\n<br />\nSilakan login ke website jurnal sebelum {$responseDueDate} untuk memberitahukan apakah Anda bersedia melakukan review atau tidak, juga untuk mengakses naskah dan menyerahkan review beserta rekomendasi Anda. Website jurnal adalah {$contextUrl}<br />\n<br />\nTenggat review ini sendiri adalah {$reviewDueDate}.<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'Email ini dari Editor Bagian kepada Reviewer  untuk meminta reviewer memberitahukan apakah menerima atau menolak permohonan review. Email ini memberikan informasi tentang naskah meliputi judul dan abstrak, tenggat review, dan cara mengakses naskah. Pesan ini digunakan saat Proses Review Standar dipilih di Manajemen > Pengaturan > Alur Kerja > Review, dan akses one-click reviewer diaktifkan.'),
('REVIEW_REQUEST_ONECLICK_SUBSEQUENT', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\n<br />\nThis regards the manuscript &quot;{$submissionTitle},&quot; which is under consideration by {$contextName}.<br />\n<br />\nFollowing the review of the previous version of the manuscript, the authors have now submitted a revised version of their paper. We would appreciate it if you could help evaluate it.<br />\n<br />\nPlease log into the journal web site by {$responseDueDate} to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation.<br />\n<br />\nThe review itself is due {$reviewDueDate}.<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'This email from the Section Editor to a Reviewer requests that the reviewer accept or decline the task of reviewing a submission for a second or greater round of review. It provides information about the submission such as the title and abstract, a review due date, and how to access the submission itself. This message is used when the Standard Review Process is selected in Management > Settings > Workflow > Review, and one-click reviewer access is enabled.'),
('REVIEW_REQUEST_ONECLICK_SUBSEQUENT', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\n<br />\nEmail ini terkait naskah &quot;{$submissionTitle},&quot; yang sedang dalam pertimbangan di {$contextName}.<br />\n<br />\nSetelah review untuk versi naskah sebelumnya, penulis saat ini telah menyerahkan versi revisi naskahnya.  Kami berharap Anda dapat membantu mengevaluasinya.<br />\n<br />\nSilakan login ke website jurnal sebelum {$responseDueDate} untuk memberitahukan apakah Anda bersedia melakukan review atau tidak, juga untuk mengakses naskah dan menyerahkan review beserta rekomendasi Anda.<br />\n<br />\nTenggat review ini sendiri adalah {$reviewDueDate}.<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'Email ini dari Editor Bagian kepada Reviewer untuk meminta reviewer memberitahukan apakah menerima atau menolak permohonan review untuk ronde kedua atau berikutnya. Email ini memberikan informasi tentang naskah meliputi judul dan abstrak, tenggat review, dan cara mengakses naskah. Pesan ini digunakan saat Proses Review Standar dipilih di Manajemen > Pengaturan > Alur Kerja > Review, dan akses one-click reviewer diaktifkan.'),
('REVIEW_REQUEST_REMIND_AUTO', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\nJust a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have your response by {$responseDueDate}, and this email has been automatically generated and sent with the passing of that date.\n<br />\nI believe that you would serve as an excellent reviewer of the manuscript. The submission\'s abstract is inserted below, and I hope that you will consider undertaking this important task for us.<br />\n<br />\nPlease log into the journal web site to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation. The web site is {$contextUrl}<br />\n<br />\nThe review itself is due {$reviewDueDate}.<br />\n<br />\nIf you do not have your username and password for the journal\'s web site, you can use this link to reset your password (which will then be emailed to you along with your username). {$passwordResetUrl}<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'This email is automatically sent when a reviewer\'s confirmation due date elapses (see Review Options under Settings > Workflow > Review) and one-click reviewer access is disabled. Scheduled tasks must be enabled and configured (see the site configuration file).'),
('REVIEW_REQUEST_REMIND_AUTO', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\nSekedar mengingatkan tentang permohonan kami untuk melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Kami berharap telah menerima respon Anda pada {$responseDueDate}, dan email ini dikirim secara otomatis seiring terlewatinya tanggal tersebut.\n<br />\nKami memandang bahwa Anda merupakan reviewer yang tepat untuk naskah tersebut. Abstrak naskah tersebut disertakan di bawah ini, dan kami berharap Anda berkenan melakukannya.<br />\n<br />\nSilakan login ke website jurnal untuk memberitahukan apakah Anda bersedia melakukan review atau tidak, juga untuk mengakses naskah dan menyerahkan review beserta rekomendasi Anda. Website jurnal adalah {$contextUrl}<br />\n<br />\nTenggat review ini sendiri adalah {$reviewDueDate}.<br />\n<br />\nJika Anda tidak memiliki nama pengguna dan sandi untuk website jurnal ini, Anda dapat menggunakan tautan berikut untuk mereset sandi Anda (yang selanjutnya akan diemailkan kepada Anda bersama nama pengguna Anda). {$passwordResetUrl}<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'Email ini dikirim otomatis saat tenggat konfirmasi terlewati (lihat Opsi Review di menu Pengaturan > Alur Kerja > Review) dan akses one-click reviewer dinonaktifkan. Scheduled tasks must be enabled and configured (see the site configuration file).'),
('REVIEW_REQUEST_REMIND_AUTO_ONECLICK', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\nJust a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have your response by {$responseDueDate}, and this email has been automatically generated and sent with the passing of that date.\n<br />\nI believe that you would serve as an excellent reviewer of the manuscript. The submission\'s abstract is inserted below, and I hope that you will consider undertaking this important task for us.<br />\n<br />\nPlease log into the journal web site to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation.<br />\n<br />\nThe review itself is due {$reviewDueDate}.<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'This email is automatically sent when a reviewer\'s confirmation due date elapses (see Review Options under Settings > Workflow > Review) and one-click reviewer access is enabled. Scheduled tasks must be enabled and configured (see the site configuration file).'),
('REVIEW_REQUEST_REMIND_AUTO_ONECLICK', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\nSekedar mengingatkan tentang permohonan kami untuk melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Kami berharap telah menerima respon Anda pada {$responseDueDate}, dan email ini dikirim secara otomatis seiring terlewatinya tanggal tersebut.\n<br />\nKami memandang bahwa Anda merupakan reviewer yang tepat untuk naskah tersebut. Abstrak naskah tersebut disertakan di bawah ini, dan kami berharap Anda berkenan melakukannya.<br />\n<br />\nSilakan login ke website jurnal untuk memberitahukan apakah Anda bersedia melakukan review atau tidak, juga untuk mengakses naskah dan menyerahkan review beserta rekomendasi Anda. Website jurnal adalah {$contextUrl}<br />\n<br />\nTenggat review ini sendiri adalah {$reviewDueDate}.<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'Email ini dikirim otomatis saat tenggat konfirmasi reviewer terlewati (lihat Opsi Review di menu Pengaturan > Alur Kerja > Review) dan akses one-click reviewer diaktifkan. Tugas Terjadwal harus diaktifkan dan diatur (lihat file konfigurasi website).'),
('REVIEW_REQUEST_SUBSEQUENT', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\n<br />\nThis regards the manuscript &quot;{$submissionTitle},&quot; which is under consideration by {$contextName}.<br />\n<br />\nFollowing the review of the previous version of the manuscript, the authors have now submitted a revised version of their paper. We would appreciate it if you could help evaluate it.<br />\n<br />\nPlease log into the journal web site by {$responseDueDate} to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation. The web site is {$contextUrl}<br />\n<br />\nThe review itself is due {$reviewDueDate}.<br />\n<br />\nIf you do not have your username and password for the journal\'s web site, you can use this link to reset your password (which will then be emailed to you along with your username). {$passwordResetUrl}<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'This email from the Section Editor to a Reviewer requests that the reviewer accept or decline the task of reviewing a submission for a second or greater round of review. It provides information about the submission such as the title and abstract, a review due date, and how to access the submission itself. This message is used when the Standard Review Process is selected in Management > Settings > Workflow > Review. (Otherwise see REVIEW_REQUEST_ATTACHED_SUBSEQUENT.)'),
('REVIEW_REQUEST_SUBSEQUENT', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\n<br />\nEmail ini terkait naskah &quot;{$submissionTitle},&quot; yang sedang dalam pertimbangan di {$contextName}.<br />\n<br />\nSetelah review untuk versi naskah sebelumnya, penulis saat ini telah menyerahkan versi revisi naskahnya.  Kami berharap Anda dapat membantu mengevaluasinya.<br />\n<br />\nSilakan login ke website jurnal sebelum {$responseDueDate} untuk memberitahukan apakah Anda bersedia melakukan review atau tidak, juga untuk mengakses naskah dan menyerahkan review beserta rekomendasi Anda. Website jurnal adalah {$contextUrl}<br />\n<br />\nTenggat review ini sendiri adalah {$reviewDueDate}.<br />\n<br />\nJika Anda tidak memiliki nama pengguna dan sandi untuk website jurnal ini, Anda dapat menggunakan tautan berikut untuk mereset sandi Anda (yang selanjutnya akan diemailkan kepada Anda bersama nama pengguna Anda). {$passwordResetUrl}<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'Email ini dari Editor Bagian kepada Reviewer untuk meminta reviewer memberitahukan apakah menerima atau menolak permohonan review untuk ronde kedua atau berikutnya. Email ini memberikan informasi tentang naskah meliputi judul dan abstrak, tenggat review, dan cara mengakses naskah. Pesan ini digunakan saat Proses Review Standar dipilih di Manajemen > Pengaturan > Alur Kerja > Review. (Selain itu lihat REVIEW_REQUEST_ATTACHED_SUBSEQUENT.)'),
('REVISED_VERSION_NOTIFY', 'en_US', 'Revised Version Uploaded', 'Editors:<br />\n<br />\nA revised version of &quot;{$submissionTitle}&quot; has been uploaded by the author {$authorName}.<br />\n<br />\nSubmission URL: {$submissionUrl}<br />\n<br />\n{$editorialContactSignature}', 'This email is automatically sent to the assigned editor when author uploads a revised version of an article.'),
('REVISED_VERSION_NOTIFY', 'id_ID', 'Versi Revisi telah Diunggah', 'Editor:<br />\n<br />\nVersi revisi dari &quot;{$submissionTitle}&quot; telah diunggah oleh penulis {$authorName}.<br />\n<br />\nURL naskah: {$submissionUrl}<br />\n<br />\n{$editorialContactSignature}', 'Email ini dikirim secara otomatis ke editor yang ditugaskan saat penulis mengunggah naskah versi revisi.'),
('STATISTICS_REPORT_NOTIFICATION', 'en_US', 'Editorial activity for {$month}, {$year}', '\n{$name}, <br />\n<br />\nYour journal health report for {$month}, {$year} is now available. Your key stats for this month are below.<br />\n<ul>\n	<li>New submissions this month: {$newSubmissions}</li>\n	<li>Declined submissions this month: {$declinedSubmissions}</li>\n	<li>Accepted submissions this month: {$acceptedSubmissions}</li>\n	<li>Total submissions in the system: {$totalSubmissions}</li>\n</ul>\nLogin to the journal to view more detailed <a href=\"{$editorialStatsLink}\">editorial trends</a> and <a href=\"{$publicationStatsLink}\">published article stats</a>. A full copy of this month\'s editorial trends is attached.<br />\n<br />\nSincerely,<br />\n{$principalContactSignature}', 'This email is automatically sent monthly to editors and journal managers to provide them a system health overview.'),
('STATISTICS_REPORT_NOTIFICATION', 'id_ID', 'Kagiatan redaksi selama {$month}, {$year}', '\n{$name}, <br />\n<br />\nLaporan kondisi jurnal Anda untuk {$month}, {$year} sudah tersedia. Statistik utama Anda bulan ini tersaji sebagai berikut.<br />\n<ul>\n	<li>Naskah baru yang masuk bulan ini: {$newSubmissions}</li>\n	<li>Naskah ditolak bulan ini: {$declinedSubmissions}</li>\n	<li>Naskah yang diterima bulan ini: {$acceptedSubmissions}</li>\n	<li>Total naskah dalam sistem: {$totalSubmissions}</li>\n</ul>\nLogin untuk melihat lebih rinci <a href=\"{$editorialStatsLink}\">trend editorial</a> dan <a href=\"{$publicationStatsLink}\">statistik artikel yang dipublikasikan</a>. Salinan lengkap trend bulan ini terlampir.<br />\n<br />\nSalam Hormat,<br />\n{$principalContactSignature}', 'Email ini dikirim secara otomatis setiap bulan kepada editor dan manajer jurnal untuk menyediakan pratinjau kestabilan sistem.'),
('SUBMISSION_ACK', 'en_US', 'Submission Acknowledgement', '{$authorName}:<br />\n<br />\nThank you for submitting the manuscript, &quot;{$submissionTitle}&quot; to {$contextName}. With the online journal management system that we are using, you will be able to track its progress through the editorial process by logging in to the journal web site:<br />\n<br />\nSubmission URL: {$submissionUrl}<br />\nUsername: {$authorUsername}<br />\n<br />\nIf you have any questions, please contact me. Thank you for considering this journal as a venue for your work.<br />\n<br />\n{$editorialContactSignature}', 'This email, when enabled, is automatically sent to an author when he or she completes the process of submitting a manuscript to the journal. It provides information about tracking the submission through the process and thanks the author for the submission.'),
('SUBMISSION_ACK', 'id_ID', 'Ucapan Terimakasih atas Penyerahan Naskah', '{$authorName}:<br />\n<br />\nTerimakasih telah menyerahkan naskah, &quot;{$submissionTitle}&quot; ke {$contextName}. Dengan sistem manajemenn jurnal online yang kami gunakan, Anda dapat memantau kemajuan proses editorial naskah Anda melalui:<br />\n<br />\nURL Naskah: {$submissionUrl}<br />\nNama pengguna: {$authorUsername}<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami. Terimakasih telah mempercayakan publikasi karya Anda di jurnal kami.<br />\n<br />\n{$editorialContactSignature}', 'Email ini, jika diaktifkan, secara otomatis dikirimkan ke penulis saat menyelesaikan proses penyerahan naskah ke jurnal. Email ini berisi informasi tentang pemantauan kemajuan proses editorial dan ucaan terima kasih atas naskah yang dikirimkan.'),
('SUBMISSION_ACK_NOT_USER', 'en_US', 'Submission Acknowledgement', 'Hello,<br />\n<br />\n{$submitterName} has submitted the manuscript, &quot;{$submissionTitle}&quot; to {$contextName}. <br />\n<br />\nIf you have any questions, please contact me. Thank you for considering this journal as a venue for your work.<br />\n<br />\n{$editorialContactSignature}', 'This email, when enabled, is automatically sent to the other authors who are not users within OJS specified during the submission process.'),
('SUBMISSION_ACK_NOT_USER', 'id_ID', 'Pemberitahuan Penyerahan Naskah', 'Hello,<br />\n<br />\n{$submitterName} telah menyerahkan naskah, &quot;{$submissionTitle}&quot; ke {$contextName}. <br />\n<br />\nJika ada pertanyaan, silakan hubungi kami. Terimakasih telah mempercayakan publikasi karya Anda di jurnal kami.<br />\n<br />\n{$editorialContactSignature}', 'Email ini, jika diaktifkan, secara otomatis dikirimkan ke penulis saat menyelesaikan proses penyerahan naskah ke jurnal. Email ini memberikan informasi mengenai pemantauan kemajuan proses editorial dan mengucapkan terimakasih atas naskah yang dikirimkan.'),
('SUBSCRIPTION_AFTER_EXPIRY', 'en_US', 'Subscription Expired', '{$subscriberName}:<br />\n<br />\nYour {$contextName} subscription has expired.<br />\n<br />\n{$subscriptionType}<br />\nExpiry date: {$expiryDate}<br />\n<br />\nTo renew your subscription, please go to the journal website. You are able to log in to the system with your username, &quot;{$username}&quot;.<br />\n<br />\nIf you have any questions, please feel free to contact me.<br />\n<br />\n{$subscriptionContactSignature}', 'This email notifies a subscriber that their subscription has expired. It provides the journal\'s URL along with instructions for access.'),
('SUBSCRIPTION_AFTER_EXPIRY', 'id_ID', 'Langganan Berakhir', '{$subscriberName}:<br />\n<br />\nLangganan {$contextName} Anda telah berakhir.<br />\n<br />\n{$subscriptionType}<br />\nTanggal berakhir: {$expiryDate}<br />\n<br />\nUntuk memperbaharui langganan Anda, silakan kunjungi website jurnal.  Anda dapat login dengan menggunakan nama pengguna Anda, &quot;{$username}&quot;.<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami.<br />\n<br />\n{$subscriptionContactSignature}', 'Email ini memberitahu seorang pelanggan bahwa langganannya telah berakhir.  Email ini memberikan URL jurnal beserta petunjuk aksesnya.'),
('SUBSCRIPTION_AFTER_EXPIRY_LAST', 'en_US', 'Subscription Expired - Final Reminder', '{$subscriberName}:<br />\n<br />\nYour {$contextName} subscription has expired.<br />\nPlease note that this is the final reminder that will be emailed to you.<br />\n<br />\n{$subscriptionType}<br />\nExpiry date: {$expiryDate}<br />\n<br />\nTo renew your subscription, please go to the journal website. You are able to log in to the system with your username, &quot;{$username}&quot;.<br />\n<br />\nIf you have any questions, please feel free to contact me.<br />\n<br />\n{$subscriptionContactSignature}', 'This email notifies a subscriber that their subscription has expired. It provides the journal\'s URL along with instructions for access.'),
('SUBSCRIPTION_AFTER_EXPIRY_LAST', 'id_ID', 'Langganan Berakhir - Pengingat Terakhir', '{$subscriberName}:<br />\n<br />\nLangganan {$contextName} Anda telah berakhir.<br />\nIni adalah pengingat terakhir yang diemailkan kepada Anda.<br />\n<br />\n{$subscriptionType}<br />\nTanggal berakhir: {$expiryDate}<br />\n<br />\nUntuk memperbaharui langganan Anda, silakan kunjungi website jurnal.  Anda dapat login dengan menggunakan nama pengguna Anda, &quot;{$username}&quot;.<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami.<br />\n<br />\n{$subscriptionContactSignature}', 'Email ini memberitahu seorang pelanggan bahwa langganannya telah berakhir.  Email ini memberikan URL jurnal beserta petunjuk aksesnya.'),
('SUBSCRIPTION_BEFORE_EXPIRY', 'en_US', 'Notice of Subscription Expiry', '{$subscriberName}:<br />\n<br />\nYour {$contextName} subscription is about to expire.<br />\n<br />\n{$subscriptionType}<br />\nExpiry date: {$expiryDate}<br />\n<br />\nTo ensure the continuity of your access to this journal, please go to the journal website and renew your subscription. You are able to log in to the system with your username, &quot;{$username}&quot;.<br />\n<br />\nIf you have any questions, please feel free to contact me.<br />\n<br />\n{$subscriptionContactSignature}', 'This email notifies a subscriber that their subscription will soon expire. It provides the journal\'s URL along with instructions for access.'),
('SUBSCRIPTION_BEFORE_EXPIRY', 'id_ID', 'Pemberitahuan Tanggal Berakhir Langganan', '{$subscriberName}:<br />\n<br />\nLangganan {$contextName} Anda hampir berakhir.<br />\n<br />\n{$subscriptionType}<br />\nTanggal berakhir: {$expiryDate}<br />\n<br />\nUntuk terus memperoleh akses ke jurnal ini, silakan kunjungi website jurnal dan perbaharui langganan Anda. Anda dapat login menggunakan nama pengguna Anda, &quot;{$username}&quot;.<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami.<br />\n<br />\n{$subscriptionContactSignature}', 'Email ini memberitahu seorang pelanggan bahwa langganannya hampir berakhir.  Email ini memberikan URL jurnal beserta petunjuk aksesnya.'),
('SUBSCRIPTION_NOTIFY', 'en_US', 'Subscription Notification', '{$subscriberName}:<br />\n<br />\nYou have now been registered as a subscriber in our online journal management system for {$contextName}, with the following subscription:<br />\n<br />\n{$subscriptionType}<br />\n<br />\nTo access content that is available only to subscribers, simply log in to the system with your username, &quot;{$username}&quot;.<br />\n<br />\nOnce you have logged in to the system you can change your profile details and password at any point.<br />\n<br />\nPlease note that if you have an institutional subscription, there is no need for users at your institution to log in, since requests for subscription content will be automatically authenticated by the system.<br />\n<br />\nIf you have any questions, please feel free to contact me.<br />\n<br />\n{$subscriptionContactSignature}', 'This email notifies a registered reader that the Manager has created a subscription for them. It provides the journal\'s URL along with instructions for access.'),
('SUBSCRIPTION_NOTIFY', 'id_ID', 'Pemberitahuan Langganan', '{$subscriberName}:<br />\n<br />\nAnda sekarang telah terdaftar sebagai pelanggan di sistem manajemen jurnal online kami, {$contextName}, dengan jenis langganan:<br />\n<br />\n{$subscriptionType}<br />\n<br />\nUntuk mengakses konten yang hanya tersedia bagi pelanggan, silakan login ke sistem dengan menggunakan nama pengguna Anda, &quot;{$username}&quot;.<br />\n<br />\nSetelah login, Anda dapat mengubah detail profil Anda dan sandi Anda kapanpun Anda kehendaki.<br />\n<br />\nJika Anda memiliki langganan institusi, pengguna di institusi Anda tidak perlu login karena semua akses secara otomatis diotentikasi oleh sistem.<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami.<br />\n<br />\n{$subscriptionContactSignature}', 'Email ini memberitahu pembaca terdaftar bahwa Manajer telah membuat akses langganan untuk mereka.  Email ini memberikan URL jurnal beserta petunjuk aksesnya.'),
('SUBSCRIPTION_PURCHASE_INDL', 'en_US', 'Subscription Purchase: Individual', 'An individual subscription has been purchased online for {$contextName} with the following details.<br />\n<br />\nSubscription Type:<br />\n{$subscriptionType}<br />\n<br />\nUser:<br />\n{$userDetails}<br />\n<br />\nMembership Information (if provided):<br />\n{$membership}<br />\n<br />\nTo view or edit this subscription, please use the following URL.<br />\n<br />\nSubscription URL: {$subscriptionUrl}<br />\n', 'This email notifies the Subscription Manager that an individual subscription has been purchased online. It provides summary information about the subscription and a quick access link to the purchased subscription.'),
('SUBSCRIPTION_PURCHASE_INDL', 'id_ID', 'Pembelian Langganan: Individu', 'Sebuah langganan individu telah dibeli online untuk {$contextName} dengan rincian berikut.<br />\n<br />\nJenis Langganan:<br />\n{$subscriptionType}<br />\n<br />\nPengguna:<br />\n{$userDetails}<br />\n<br />\nInformasi Keanggotaan (jika ada):<br />\n{$membership}<br />\n<br />\nUntuk melihat atau mengubah langganan ini, silakan gunakan URL berikut ini.<br />\n<br />\nURL Langganan: {$subscriptionUrl}<br />\n', 'Email ini memberitahu Manajer Langganan bahwa sebuah langganan individu telah dibeli online.  Email ini memberikan ringkasan informasi tentang langganan tersebut beserta tautan untuk mengakses langganan yang dibeli.'),
('SUBSCRIPTION_PURCHASE_INSTL', 'en_US', 'Subscription Purchase: Institutional', 'An institutional subscription has been purchased online for {$contextName} with the following details. To activate this subscription, please use the provided Subscription URL and set the subscription status to \'Active\'.<br />\n<br />\nSubscription Type:<br />\n{$subscriptionType}<br />\n<br />\nInstitution:<br />\n{$institutionName}<br />\n{$institutionMailingAddress}<br />\n<br />\nDomain (if provided):<br />\n{$domain}<br />\n<br />\nIP Ranges (if provided):<br />\n{$ipRanges}<br />\n<br />\nContact Person:<br />\n{$userDetails}<br />\n<br />\nMembership Information (if provided):<br />\n{$membership}<br />\n<br />\nTo view or edit this subscription, please use the following URL.<br />\n<br />\nSubscription URL: {$subscriptionUrl}<br />\n', 'This email notifies the Subscription Manager that an institutional subscription has been purchased online. It provides summary information about the subscription and a quick access link to the purchased subscription.'),
('SUBSCRIPTION_PURCHASE_INSTL', 'id_ID', 'Pembelian Langganan: Institusi', 'Sebuah langganan institusi telah dibeli online untuk {$contextName} dengan rincian berikut. Untuk mengaktifkan langganan ini, silakan gunakan URL Langganan dan jadikan status langganan ke \'Aktif\'.<br />\n<br />\nJenis Langganan:<br />\n{$subscriptionType}<br />\n<br />\nInstitusi:<br />\n{$institutionName}<br />\n{$institutionMailingAddress}<br />\n<br />\nDomain (jika ada):<br />\n{$domain}<br />\n<br />\nIP Ranges (jika ada):<br />\n{$ipRanges}<br />\n<br />\nContact Person:<br />\n{$userDetails}<br />\n<br />\nInformasi Keanggotaan (jika ada):<br />\n{$membership}<br />\n<br />\nUntuk melihat atau mengubah langganan ini, silakan gunakan URL berikut ini.<br />\n<br />\nURL Langganan: {$subscriptionUrl}<br />\n', 'Email ini memberitahu Manajer Langganan bahwa sebuah langganan institusi telah dibeli online.  Email ini memberikan ringkasan informasi tentang langganan tersebut beserta tautan untuk mengakses langganan yang dibeli.'),
('SUBSCRIPTION_RENEW_INDL', 'en_US', 'Subscription Renewal: Individual', 'An individual subscription has been renewed online for {$contextName} with the following details.<br />\n<br />\nSubscription Type:<br />\n{$subscriptionType}<br />\n<br />\nUser:<br />\n{$userDetails}<br />\n<br />\nMembership Information (if provided):<br />\n{$membership}<br />\n<br />\nTo view or edit this subscription, please use the following URL.<br />\n<br />\nSubscription URL: {$subscriptionUrl}<br />\n', 'This email notifies the Subscription Manager that an individual subscription has been renewed online. It provides summary information about the subscription and a quick access link to the renewed subscription.'),
('SUBSCRIPTION_RENEW_INDL', 'id_ID', 'Pembaharuan Langganan: Individu', 'Sebuah langganan individu telah diperbaharui online untuk {$contextName} dengan rincian berikut.<br />\n<br />\nJenis Langganan:<br />\n{$subscriptionType}<br />\n<br />\nPengguna:<br />\n{$userDetails}<br />\n<br />\nInformasi Keanggotaan (jika ada):<br />\n{$membership}<br />\n<br />\nUntuk melihat atau mengubah langganan ini, silakan gunakan URL berikut ini.<br />\n<br />\nURL Langganan: {$subscriptionUrl}<br />\n', 'Email ini memberitahu Manajer Langganan bahwa sebuah langganan individu telah diperbaharui online.  Email ini memberikan ringkasan informasi tentang langganan tersebut beserta tautan untuk mengakses langganan yang diperbaharui.'),
('SUBSCRIPTION_RENEW_INSTL', 'en_US', 'Subscription Renewal: Institutional', 'An institutional subscription has been renewed online for {$contextName} with the following details.<br />\n<br />\nSubscription Type:<br />\n{$subscriptionType}<br />\n<br />\nInstitution:<br />\n{$institutionName}<br />\n{$institutionMailingAddress}<br />\n<br />\nDomain (if provided):<br />\n{$domain}<br />\n<br />\nIP Ranges (if provided):<br />\n{$ipRanges}<br />\n<br />\nContact Person:<br />\n{$userDetails}<br />\n<br />\nMembership Information (if provided):<br />\n{$membership}<br />\n<br />\nTo view or edit this subscription, please use the following URL.<br />\n<br />\nSubscription URL: {$subscriptionUrl}<br />\n', 'This email notifies the Subscription Manager that an institutional subscription has been renewed online. It provides summary information about the subscription and a quick access link to the renewed subscription.'),
('SUBSCRIPTION_RENEW_INSTL', 'id_ID', 'Pembaharuan Langganan: Institusi', 'Sebuah langganan institusi telah diperbaharui online untuk {$contextName} dengan rincian berikut.<br />\n<br />\nJenis Langganan:<br />\n{$subscriptionType}<br />\n<br />\nInstitusi:<br />\n{$institutionName}<br />\n{$institutionMailingAddress}<br />\n<br />\nDomain (jika ada):<br />\n{$domain}<br />\n<br />\nIP Ranges (jika ada):<br />\n{$ipRanges}<br />\n<br />\nContact Person:<br />\n{$userDetails}<br />\n<br />\nInformasi Keanggotaan (jika ada):<br />\n{$membership}<br />\n<br />\nUntuk melihat atau mengubah langganan ini, silakan gunakan URL berikut ini.<br />\n<br />\nURL Langganan: {$subscriptionUrl}<br />\n', 'Email ini memberitahu Manajer Langganan bahwa sebuah langganan institusi telah diperbaharui online.  Email ini memberikan ringkasan informasi tentang langganan tersebut beserta tautan untuk mengakses langganan yang diperbaharui.'),
('USER_REGISTER', 'en_US', 'Journal Registration', '{$userFullName}<br />\n<br />\nYou have now been registered as a user with {$contextName}. We have included your username and password in this email, which are needed for all work with this journal through its website. At any point, you can ask to be removed from the journal\'s list of users by contacting me.<br />\n<br />\nUsername: {$username}<br />\nPassword: {$password}<br />\n<br />\nThank you,<br />\n{$principalContactSignature}', 'This email is sent to a newly registered user to welcome them to the system and provide them with a record of their username and password.'),
('USER_REGISTER', 'id_ID', 'Registrasi Jurnal', '{$userFullName}<br />\n<br />\nAnda sekarang telah terdaftar sebagai pengguna di {$contextName}.  Kami sertakan nama pengguna dan sandi Anda di email ini, keduanya diperlukan untuk semua kegiatan melalui website jurnal ini. Anda dapat keluar dari daftar pengguna jurnal kapan saja dengan menghubungi kami.<br />\n<br />\nNama pengguna: {$username}<br />\nSandi: {$password}<br />\n<br />\nTerimakasih,<br />\n{$principalContactSignature}', 'Email ini dikirimkan ke pengguna baru untuk menyambut dan memberikan informasi nama pengguna dan sandi.'),
('USER_VALIDATE', 'en_US', 'Validate Your Account', '{$userFullName}<br />\n<br />\nYou have created an account with {$contextName}, but before you can start using it, you need to validate your email account. To do this, simply follow the link below:<br />\n<br />\n{$activateUrl}<br />\n<br />\nThank you,<br />\n{$principalContactSignature}', 'This email is sent to a newly registered user to validate their email account.'),
('USER_VALIDATE', 'id_ID', 'Validasi Akun Anda', '{$userFullName}<br />\n<br />\nAnda telah membuat akun di {$contextName}.  Sebelum dapat menggunakannya, Anda perlu melakukan validasi akun email. Untuk melakukannya, klik tautan berikut ini:<br />\n<br />\n{$activateUrl}<br />\n<br />\nTerimakasih,<br />\n{$principalContactSignature}', 'Email ini dikirim ke pengguna baru agar melakukan validasi email yang mereka gunakan untuk registrasi.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `email_templates_settings`
--

DROP TABLE IF EXISTS `email_templates_settings`;
CREATE TABLE IF NOT EXISTS `email_templates_settings` (
  `email_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  UNIQUE KEY `email_settings_pkey` (`email_id`,`locale`,`setting_name`),
  KEY `email_settings_email_id` (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `event_log`
--

DROP TABLE IF EXISTS `event_log`;
CREATE TABLE IF NOT EXISTS `event_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assoc_type` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `date_logged` datetime NOT NULL,
  `event_type` bigint(20) DEFAULT NULL,
  `message` text,
  `is_translated` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `event_log_assoc` (`assoc_type`,`assoc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `event_log_settings`
--

DROP TABLE IF EXISTS `event_log_settings`;
CREATE TABLE IF NOT EXISTS `event_log_settings` (
  `log_id` bigint(20) NOT NULL,
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `event_log_settings_pkey` (`log_id`,`setting_name`),
  KEY `event_log_settings_log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `filters`
--

DROP TABLE IF EXISTS `filters`;
CREATE TABLE IF NOT EXISTS `filters` (
  `filter_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `filter_group_id` bigint(20) NOT NULL DEFAULT '0',
  `context_id` bigint(20) NOT NULL DEFAULT '0',
  `display_name` varchar(255) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `is_template` tinyint(4) NOT NULL DEFAULT '0',
  `parent_filter_id` bigint(20) NOT NULL DEFAULT '0',
  `seq` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`filter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `filters`
--

INSERT INTO `filters` (`filter_id`, `filter_group_id`, `context_id`, `display_name`, `class_name`, `is_template`, `parent_filter_id`, `seq`) VALUES
(1, 1, 0, 'Extract metadata from a(n) Submission', 'plugins.metadata.dc11.filter.Dc11SchemaArticleAdapter', 0, 0, 0),
(2, 2, 0, 'MODS 3.4', 'lib.pkp.plugins.metadata.mods34.filter.Mods34DescriptionXmlFilter', 0, 0, 0),
(3, 3, 0, 'Extract metadata from a(n) Submission', 'plugins.metadata.mods34.filter.Mods34SchemaArticleAdapter', 0, 0, 0),
(4, 4, 0, 'Inject metadata into a(n) Submission', 'plugins.metadata.mods34.filter.Mods34SchemaArticleAdapter', 0, 0, 0),
(5, 5, 0, 'Crossref XML issue export', 'plugins.importexport.crossref.filter.IssueCrossrefXmlFilter', 0, 0, 0),
(6, 6, 0, 'Crossref XML issue export', 'plugins.importexport.crossref.filter.ArticleCrossrefXmlFilter', 0, 0, 0),
(7, 7, 0, 'DataCite XML export', 'plugins.importexport.datacite.filter.DataciteXmlFilter', 0, 0, 0),
(8, 8, 0, 'DataCite XML export', 'plugins.importexport.datacite.filter.DataciteXmlFilter', 0, 0, 0),
(9, 9, 0, 'DataCite XML export', 'plugins.importexport.datacite.filter.DataciteXmlFilter', 0, 0, 0),
(10, 10, 0, 'DOAJ XML export', 'plugins.importexport.doaj.filter.DOAJXmlFilter', 0, 0, 0),
(11, 11, 0, 'DOAJ JSON export', 'plugins.importexport.doaj.filter.DOAJJsonFilter', 0, 0, 0),
(12, 12, 0, 'mEDRA XML issue export', 'plugins.importexport.medra.filter.IssueMedraXmlFilter', 0, 0, 0),
(13, 13, 0, 'mEDRA XML article export', 'plugins.importexport.medra.filter.ArticleMedraXmlFilter', 0, 0, 0),
(14, 14, 0, 'mEDRA XML article export', 'plugins.importexport.medra.filter.GalleyMedraXmlFilter', 0, 0, 0),
(15, 15, 0, 'Native XML submission export', 'plugins.importexport.native.filter.ArticleNativeXmlFilter', 0, 0, 0),
(16, 16, 0, 'Native XML submission import', 'plugins.importexport.native.filter.NativeXmlArticleFilter', 0, 0, 0),
(17, 17, 0, 'Native XML issue export', 'plugins.importexport.native.filter.IssueNativeXmlFilter', 0, 0, 0),
(18, 18, 0, 'Native XML issue import', 'plugins.importexport.native.filter.NativeXmlIssueFilter', 0, 0, 0),
(19, 19, 0, 'Native XML issue galley export', 'plugins.importexport.native.filter.IssueGalleyNativeXmlFilter', 0, 0, 0),
(20, 20, 0, 'Native XML issue galley import', 'plugins.importexport.native.filter.NativeXmlIssueGalleyFilter', 0, 0, 0),
(21, 21, 0, 'Native XML author export', 'plugins.importexport.native.filter.AuthorNativeXmlFilter', 0, 0, 0),
(22, 22, 0, 'Native XML author import', 'plugins.importexport.native.filter.NativeXmlAuthorFilter', 0, 0, 0),
(23, 26, 0, 'Native XML submission file import', 'plugins.importexport.native.filter.NativeXmlArticleFileFilter', 0, 0, 0),
(24, 27, 0, 'Native XML submission file import', 'plugins.importexport.native.filter.NativeXmlArtworkFileFilter', 0, 0, 0),
(25, 28, 0, 'Native XML submission file import', 'plugins.importexport.native.filter.NativeXmlSupplementaryFileFilter', 0, 0, 0),
(26, 23, 0, 'Native XML submission file export', 'lib.pkp.plugins.importexport.native.filter.SubmissionFileNativeXmlFilter', 0, 0, 0),
(27, 24, 0, 'Native XML submission file export', 'plugins.importexport.native.filter.ArtworkFileNativeXmlFilter', 0, 0, 0),
(28, 25, 0, 'Native XML submission file export', 'plugins.importexport.native.filter.SupplementaryFileNativeXmlFilter', 0, 0, 0),
(29, 29, 0, 'Native XML representation export', 'plugins.importexport.native.filter.ArticleGalleyNativeXmlFilter', 0, 0, 0),
(30, 30, 0, 'Native XML representation import', 'plugins.importexport.native.filter.NativeXmlArticleGalleyFilter', 0, 0, 0),
(31, 31, 0, 'Native XML Publication export', 'plugins.importexport.native.filter.PublicationNativeXmlFilter', 0, 0, 0),
(32, 32, 0, 'Native XML publication import', 'plugins.importexport.native.filter.NativeXmlPublicationFilter', 0, 0, 0),
(33, 33, 0, 'ArticlePubMedXmlFilter', 'plugins.importexport.pubmed.filter.ArticlePubMedXmlFilter', 0, 0, 0),
(34, 34, 0, 'User XML user export', 'lib.pkp.plugins.importexport.users.filter.PKPUserUserXmlFilter', 0, 0, 0),
(35, 35, 0, 'User XML user import', 'lib.pkp.plugins.importexport.users.filter.UserXmlPKPUserFilter', 0, 0, 0),
(36, 36, 0, 'Native XML user group export', 'lib.pkp.plugins.importexport.users.filter.UserGroupNativeXmlFilter', 0, 0, 0),
(37, 37, 0, 'Native XML user group import', 'lib.pkp.plugins.importexport.users.filter.NativeXmlUserGroupFilter', 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `filter_groups`
--

DROP TABLE IF EXISTS `filter_groups`;
CREATE TABLE IF NOT EXISTS `filter_groups` (
  `filter_group_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `symbolic` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `input_type` varchar(255) DEFAULT NULL,
  `output_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`filter_group_id`),
  UNIQUE KEY `filter_groups_symbolic` (`symbolic`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `filter_groups`
--

INSERT INTO `filter_groups` (`filter_group_id`, `symbolic`, `display_name`, `description`, `input_type`, `output_type`) VALUES
(1, 'article=>dc11', 'plugins.metadata.dc11.articleAdapter.displayName', 'plugins.metadata.dc11.articleAdapter.description', 'class::classes.submission.Submission', 'metadata::plugins.metadata.dc11.schema.Dc11Schema(ARTICLE)'),
(2, 'mods34=>mods34-xml', 'plugins.metadata.mods34.mods34XmlOutput.displayName', 'plugins.metadata.mods34.mods34XmlOutput.description', 'metadata::plugins.metadata.mods34.schema.Mods34Schema(*)', 'xml::schema(lib/pkp/plugins/metadata/mods34/filter/mods34.xsd)'),
(3, 'article=>mods34', 'plugins.metadata.mods34.articleAdapter.displayName', 'plugins.metadata.mods34.articleAdapter.description', 'class::classes.submission.Submission', 'metadata::plugins.metadata.mods34.schema.Mods34Schema(ARTICLE)'),
(4, 'mods34=>article', 'plugins.metadata.mods34.articleAdapter.displayName', 'plugins.metadata.mods34.articleAdapter.description', 'metadata::plugins.metadata.mods34.schema.Mods34Schema(ARTICLE)', 'class::classes.submission.Submission'),
(5, 'issue=>crossref-xml', 'plugins.importexport.crossref.displayName', 'plugins.importexport.crossref.description', 'class::classes.issue.Issue[]', 'xml::schema(https://www.crossref.org/schemas/crossref4.3.6.xsd)'),
(6, 'article=>crossref-xml', 'plugins.importexport.crossref.displayName', 'plugins.importexport.crossref.description', 'class::classes.submission.Submission[]', 'xml::schema(https://www.crossref.org/schemas/crossref4.3.6.xsd)'),
(7, 'issue=>datacite-xml', 'plugins.importexport.datacite.displayName', 'plugins.importexport.datacite.description', 'class::classes.issue.Issue', 'xml::schema(http://schema.datacite.org/meta/kernel-4/metadata.xsd)'),
(8, 'article=>datacite-xml', 'plugins.importexport.datacite.displayName', 'plugins.importexport.datacite.description', 'class::classes.submission.Submission', 'xml::schema(http://schema.datacite.org/meta/kernel-4/metadata.xsd)'),
(9, 'galley=>datacite-xml', 'plugins.importexport.datacite.displayName', 'plugins.importexport.datacite.description', 'class::classes.article.ArticleGalley', 'xml::schema(http://schema.datacite.org/meta/kernel-4/metadata.xsd)'),
(10, 'article=>doaj-xml', 'plugins.importexport.doaj.displayName', 'plugins.importexport.doaj.description', 'class::classes.submission.Submission[]', 'xml::schema(plugins/importexport/doaj/doajArticles.xsd)'),
(11, 'article=>doaj-json', 'plugins.importexport.doaj.displayName', 'plugins.importexport.doaj.description', 'class::classes.submission.Submission', 'primitive::string'),
(12, 'issue=>medra-xml', 'plugins.importexport.medra.displayName', 'plugins.importexport.medra.description', 'class::classes.issue.Issue[]', 'xml::schema(http://www.medra.org/schema/onix/DOIMetadata/2.0/ONIX_DOIMetadata_2.0.xsd)'),
(13, 'article=>medra-xml', 'plugins.importexport.medra.displayName', 'plugins.importexport.medra.description', 'class::classes.submission.Submission[]', 'xml::schema(http://www.medra.org/schema/onix/DOIMetadata/2.0/ONIX_DOIMetadata_2.0.xsd)'),
(14, 'galley=>medra-xml', 'plugins.importexport.medra.displayName', 'plugins.importexport.medra.description', 'class::classes.article.ArticleGalley[]', 'xml::schema(http://www.medra.org/schema/onix/DOIMetadata/2.0/ONIX_DOIMetadata_2.0.xsd)'),
(15, 'article=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::classes.submission.Submission[]', 'xml::schema(plugins/importexport/native/native.xsd)'),
(16, 'native-xml=>article', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::classes.submission.Submission[]'),
(17, 'issue=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::classes.issue.Issue[]', 'xml::schema(plugins/importexport/native/native.xsd)'),
(18, 'native-xml=>issue', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::classes.issue.Issue[]'),
(19, 'issuegalley=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::classes.issue.IssueGalley[]', 'xml::schema(plugins/importexport/native/native.xsd)'),
(20, 'native-xml=>issuegalley', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::classes.issue.IssueGalley[]'),
(21, 'author=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::classes.article.Author[]', 'xml::schema(plugins/importexport/native/native.xsd)'),
(22, 'native-xml=>author', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::classes.article.Author[]'),
(23, 'SubmissionFile=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::lib.pkp.classes.submission.SubmissionFile', 'xml::schema(plugins/importexport/native/native.xsd)'),
(24, 'SubmissionArtworkFile=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::lib.pkp.classes.submission.SubmissionArtworkFile', 'xml::schema(plugins/importexport/native/native.xsd)'),
(25, 'SupplementaryFile=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::lib.pkp.classes.submission.SupplementaryFile', 'xml::schema(plugins/importexport/native/native.xsd)'),
(26, 'native-xml=>SubmissionFile', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::lib.pkp.classes.submission.SubmissionFile'),
(27, 'native-xml=>SubmissionArtworkFile', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::lib.pkp.classes.submission.SubmissionArtworkFile'),
(28, 'native-xml=>SupplementaryFile', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::lib.pkp.classes.submission.SupplementaryFile'),
(29, 'article-galley=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::classes.article.ArticleGalley', 'xml::schema(plugins/importexport/native/native.xsd)'),
(30, 'native-xml=>ArticleGalley', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::classes.article.ArticleGalley[]'),
(31, 'publication=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::classes.publication.Publication', 'xml::schema(plugins/importexport/native/native.xsd)'),
(32, 'native-xml=>Publication', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::classes.publication.Publication'),
(33, 'article=>pubmed-xml', 'plugins.importexport.pubmed.displayName', 'plugins.importexport.pubmed.description', 'class::classes.submission.Submission[]', 'xml::dtd'),
(34, 'user=>user-xml', 'plugins.importexport.users.displayName', 'plugins.importexport.users.description', 'class::lib.pkp.classes.user.User[]', 'xml::schema(lib/pkp/plugins/importexport/users/pkp-users.xsd)'),
(35, 'user-xml=>user', 'plugins.importexport.users.displayName', 'plugins.importexport.users.description', 'xml::schema(lib/pkp/plugins/importexport/users/pkp-users.xsd)', 'class::classes.users.User[]'),
(36, 'usergroup=>user-xml', 'plugins.importexport.users.displayName', 'plugins.importexport.users.description', 'class::lib.pkp.classes.security.UserGroup[]', 'xml::schema(lib/pkp/plugins/importexport/users/pkp-users.xsd)'),
(37, 'user-xml=>usergroup', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(lib/pkp/plugins/importexport/users/pkp-users.xsd)', 'class::lib.pkp.classes.security.UserGroup[]');

-- --------------------------------------------------------

--
-- Struktur dari tabel `filter_settings`
--

DROP TABLE IF EXISTS `filter_settings`;
CREATE TABLE IF NOT EXISTS `filter_settings` (
  `filter_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `filter_settings_pkey` (`filter_id`,`locale`,`setting_name`),
  KEY `filter_settings_id` (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `genres`
--

DROP TABLE IF EXISTS `genres`;
CREATE TABLE IF NOT EXISTS `genres` (
  `genre_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `context_id` bigint(20) NOT NULL,
  `seq` bigint(20) DEFAULT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `category` bigint(20) NOT NULL DEFAULT '1',
  `dependent` tinyint(4) NOT NULL DEFAULT '0',
  `supplementary` tinyint(4) DEFAULT '0',
  `entry_key` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `genres`
--

INSERT INTO `genres` (`genre_id`, `context_id`, `seq`, `enabled`, `category`, `dependent`, `supplementary`, `entry_key`) VALUES
(109, 1, 0, 1, 1, 0, 0, 'SUBMISSION'),
(110, 1, 1, 1, 3, 0, 1, 'RESEARCHINSTRUMENT'),
(111, 1, 2, 1, 3, 0, 1, 'RESEARCHMATERIALS'),
(112, 1, 3, 1, 3, 0, 1, 'RESEARCHRESULTS'),
(113, 1, 4, 1, 3, 0, 1, 'TRANSCRIPTS'),
(114, 1, 5, 1, 3, 0, 1, 'DATAANALYSIS'),
(115, 1, 6, 1, 3, 0, 1, 'DATASET'),
(116, 1, 7, 1, 3, 0, 1, 'SOURCETEXTS'),
(117, 1, 8, 1, 1, 1, 1, 'MULTIMEDIA'),
(118, 1, 9, 1, 2, 1, 0, 'IMAGE'),
(119, 1, 10, 1, 1, 1, 0, 'STYLE'),
(120, 1, 11, 1, 3, 0, 1, 'OTHER'),
(121, 2, 0, 1, 1, 0, 0, 'SUBMISSION'),
(122, 2, 1, 1, 3, 0, 1, 'RESEARCHINSTRUMENT'),
(123, 2, 2, 1, 3, 0, 1, 'RESEARCHMATERIALS'),
(124, 2, 3, 1, 3, 0, 1, 'RESEARCHRESULTS'),
(125, 2, 4, 1, 3, 0, 1, 'TRANSCRIPTS'),
(126, 2, 5, 1, 3, 0, 1, 'DATAANALYSIS'),
(127, 2, 6, 1, 3, 0, 1, 'DATASET'),
(128, 2, 7, 1, 3, 0, 1, 'SOURCETEXTS'),
(129, 2, 8, 1, 1, 1, 1, 'MULTIMEDIA'),
(130, 2, 9, 1, 2, 1, 0, 'IMAGE'),
(131, 2, 10, 1, 1, 1, 0, 'STYLE'),
(132, 2, 11, 1, 3, 0, 1, 'OTHER');

-- --------------------------------------------------------

--
-- Struktur dari tabel `genre_settings`
--

DROP TABLE IF EXISTS `genre_settings`;
CREATE TABLE IF NOT EXISTS `genre_settings` (
  `genre_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `genre_settings_pkey` (`genre_id`,`locale`,`setting_name`),
  KEY `genre_settings_genre_id` (`genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `genre_settings`
--

INSERT INTO `genre_settings` (`genre_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(109, 'en_US', 'name', 'Article Text', 'string'),
(109, 'id_ID', 'name', 'File Utama Naskah', 'string'),
(110, 'en_US', 'name', 'Research Instrument', 'string'),
(110, 'id_ID', 'name', 'Instrumen Penelitian', 'string'),
(111, 'en_US', 'name', 'Research Materials', 'string'),
(111, 'id_ID', 'name', 'Bahan Penelitian', 'string'),
(112, 'en_US', 'name', 'Research Results', 'string'),
(112, 'id_ID', 'name', 'Hasil Penelitian', 'string'),
(113, 'en_US', 'name', 'Transcripts', 'string'),
(113, 'id_ID', 'name', 'Transkrip', 'string'),
(114, 'en_US', 'name', 'Data Analysis', 'string'),
(114, 'id_ID', 'name', 'Analisis Data', 'string'),
(115, 'en_US', 'name', 'Data Set', 'string'),
(115, 'id_ID', 'name', 'Data Set', 'string'),
(116, 'en_US', 'name', 'Source Texts', 'string'),
(116, 'id_ID', 'name', 'Teks Sumber', 'string'),
(117, 'en_US', 'name', 'Multimedia', 'string'),
(117, 'id_ID', 'name', 'Multimedia', 'string'),
(118, 'en_US', 'name', 'Image', 'string'),
(118, 'id_ID', 'name', 'Gambar', 'string'),
(119, 'en_US', 'name', 'HTML Stylesheet', 'string'),
(119, 'id_ID', 'name', 'HTML StyleSheet', 'string'),
(120, 'en_US', 'name', 'Other', 'string'),
(120, 'id_ID', 'name', 'Lainnya', 'string'),
(121, 'en_US', 'name', 'Article Text', 'string'),
(121, 'id_ID', 'name', 'File Utama Naskah', 'string'),
(122, 'en_US', 'name', 'Research Instrument', 'string'),
(122, 'id_ID', 'name', 'Instrumen Penelitian', 'string'),
(123, 'en_US', 'name', 'Research Materials', 'string'),
(123, 'id_ID', 'name', 'Bahan Penelitian', 'string'),
(124, 'en_US', 'name', 'Research Results', 'string'),
(124, 'id_ID', 'name', 'Hasil Penelitian', 'string'),
(125, 'en_US', 'name', 'Transcripts', 'string'),
(125, 'id_ID', 'name', 'Transkrip', 'string'),
(126, 'en_US', 'name', 'Data Analysis', 'string'),
(126, 'id_ID', 'name', 'Analisis Data', 'string'),
(127, 'en_US', 'name', 'Data Set', 'string'),
(127, 'id_ID', 'name', 'Data Set', 'string'),
(128, 'en_US', 'name', 'Source Texts', 'string'),
(128, 'id_ID', 'name', 'Teks Sumber', 'string'),
(129, 'en_US', 'name', 'Multimedia', 'string'),
(129, 'id_ID', 'name', 'Multimedia', 'string'),
(130, 'en_US', 'name', 'Image', 'string'),
(130, 'id_ID', 'name', 'Gambar', 'string'),
(131, 'en_US', 'name', 'HTML Stylesheet', 'string'),
(131, 'id_ID', 'name', 'HTML StyleSheet', 'string'),
(132, 'en_US', 'name', 'Other', 'string'),
(132, 'id_ID', 'name', 'Lainnya', 'string');

-- --------------------------------------------------------

--
-- Struktur dari tabel `institutional_subscriptions`
--

DROP TABLE IF EXISTS `institutional_subscriptions`;
CREATE TABLE IF NOT EXISTS `institutional_subscriptions` (
  `institutional_subscription_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subscription_id` bigint(20) NOT NULL,
  `institution_name` varchar(255) NOT NULL,
  `mailing_address` varchar(255) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`institutional_subscription_id`),
  KEY `institutional_subscriptions_subscription_id` (`subscription_id`),
  KEY `institutional_subscriptions_domain` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `institutional_subscription_ip`
--

DROP TABLE IF EXISTS `institutional_subscription_ip`;
CREATE TABLE IF NOT EXISTS `institutional_subscription_ip` (
  `institutional_subscription_ip_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subscription_id` bigint(20) NOT NULL,
  `ip_string` varchar(40) NOT NULL,
  `ip_start` bigint(20) NOT NULL,
  `ip_end` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`institutional_subscription_ip_id`),
  KEY `institutional_subscription_ip_subscription_id` (`subscription_id`),
  KEY `institutional_subscription_ip_start` (`ip_start`),
  KEY `institutional_subscription_ip_end` (`ip_end`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `issues`
--

DROP TABLE IF EXISTS `issues`;
CREATE TABLE IF NOT EXISTS `issues` (
  `issue_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `journal_id` bigint(20) NOT NULL,
  `volume` smallint(6) DEFAULT NULL,
  `number` varchar(40) DEFAULT NULL,
  `year` smallint(6) DEFAULT NULL,
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `current` tinyint(4) NOT NULL DEFAULT '0',
  `date_published` datetime DEFAULT NULL,
  `date_notified` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `access_status` tinyint(4) NOT NULL DEFAULT '1',
  `open_access_date` datetime DEFAULT NULL,
  `show_volume` tinyint(4) NOT NULL DEFAULT '0',
  `show_number` tinyint(4) NOT NULL DEFAULT '0',
  `show_year` tinyint(4) NOT NULL DEFAULT '0',
  `show_title` tinyint(4) NOT NULL DEFAULT '0',
  `style_file_name` varchar(90) DEFAULT NULL,
  `original_style_file_name` varchar(255) DEFAULT NULL,
  `url_path` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`issue_id`),
  KEY `issues_journal_id` (`journal_id`),
  KEY `issues_url_path` (`url_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `issue_files`
--

DROP TABLE IF EXISTS `issue_files`;
CREATE TABLE IF NOT EXISTS `issue_files` (
  `file_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `issue_id` bigint(20) NOT NULL,
  `file_name` varchar(90) NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `file_size` bigint(20) NOT NULL,
  `content_type` bigint(20) NOT NULL,
  `original_file_name` varchar(127) DEFAULT NULL,
  `date_uploaded` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`file_id`),
  KEY `issue_files_issue_id` (`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `issue_galleys`
--

DROP TABLE IF EXISTS `issue_galleys`;
CREATE TABLE IF NOT EXISTS `issue_galleys` (
  `galley_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `locale` varchar(14) DEFAULT NULL,
  `issue_id` bigint(20) NOT NULL,
  `file_id` bigint(20) NOT NULL,
  `label` varchar(32) DEFAULT NULL,
  `seq` double NOT NULL DEFAULT '0',
  `url_path` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`galley_id`),
  KEY `issue_galleys_issue_id` (`issue_id`),
  KEY `issue_galleys_url_path` (`url_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `issue_galley_settings`
--

DROP TABLE IF EXISTS `issue_galley_settings`;
CREATE TABLE IF NOT EXISTS `issue_galley_settings` (
  `galley_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `issue_galley_settings_pkey` (`galley_id`,`locale`,`setting_name`),
  KEY `issue_galley_settings_galley_id` (`galley_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `issue_settings`
--

DROP TABLE IF EXISTS `issue_settings`;
CREATE TABLE IF NOT EXISTS `issue_settings` (
  `issue_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `issue_settings_pkey` (`issue_id`,`locale`,`setting_name`),
  KEY `issue_settings_issue_id` (`issue_id`),
  KEY `issue_settings_name_value` (`setting_name`(50),`setting_value`(150))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `item_views`
--

DROP TABLE IF EXISTS `item_views`;
CREATE TABLE IF NOT EXISTS `item_views` (
  `assoc_type` bigint(20) NOT NULL,
  `assoc_id` varchar(32) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `date_last_viewed` datetime DEFAULT NULL,
  UNIQUE KEY `item_views_pkey` (`assoc_type`,`assoc_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `journals`
--

DROP TABLE IF EXISTS `journals`;
CREATE TABLE IF NOT EXISTS `journals` (
  `journal_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `path` varchar(32) NOT NULL,
  `seq` double NOT NULL DEFAULT '0',
  `primary_locale` varchar(14) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`journal_id`),
  UNIQUE KEY `journals_path` (`path`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `journals`
--

INSERT INTO `journals` (`journal_id`, `path`, `seq`, `primary_locale`, `enabled`) VALUES
(1, 'ifi', 3, 'en_US', 1),
(2, 'imej', 1, 'en_US', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `journal_settings`
--

DROP TABLE IF EXISTS `journal_settings`;
CREATE TABLE IF NOT EXISTS `journal_settings` (
  `journal_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) DEFAULT NULL,
  UNIQUE KEY `journal_settings_pkey` (`journal_id`,`locale`,`setting_name`),
  KEY `journal_settings_journal_id` (`journal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `journal_settings`
--

INSERT INTO `journal_settings` (`journal_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, '', 'agencies', '0', NULL),
(1, '', 'citations', '0', NULL),
(1, '', 'contactEmail', 'darlin.aulia@indo-intellectual.id', NULL),
(1, '', 'contactName', 'Darlin Aulia S.E., M.S.Ak', NULL),
(1, '', 'contactPhone', '089682146379', NULL),
(1, '', 'copyrightYearBasis', 'issue', NULL),
(1, '', 'coverage', '0', NULL),
(1, '', 'defaultReviewMode', '2', NULL),
(1, '', 'disciplines', '0', NULL),
(1, '', 'emailSignature', '<br/>\n________________________________________________________________________<br/>\n<a href=\"http://localhost/ojs/index.php/ifi\">Indo-Fintech Intellectuals</a>', NULL),
(1, '', 'enableOai', '1', NULL),
(1, '', 'enablePublisherId', 'a:4:{i:0;s:11:\"publication\";i:1;s:6:\"galley\";i:2;s:5:\"issue\";i:3;s:11:\"issueGalley\";}', NULL),
(1, '', 'itemsPerPage', '25', NULL),
(1, '', 'keywords', 'request', NULL),
(1, '', 'languages', '0', NULL),
(1, '', 'mailingAddress', '.', NULL),
(1, '', 'membershipFee', '0', NULL),
(1, '', 'numPageLinks', '10', NULL),
(1, '', 'numWeeksPerResponse', '4', NULL),
(1, '', 'numWeeksPerReview', '4', NULL),
(1, '', 'publicationFee', '0', NULL),
(1, '', 'publisherInstitution', 'Lembaga Intelektual Muda (LIM) Maluku', NULL),
(1, '', 'publishingMode', '0', NULL),
(1, '', 'purchaseArticleFee', '0', NULL),
(1, '', 'rights', '0', NULL),
(1, '', 'sidebar', 'a:6:{i:0;s:4:\"view\";i:1;s:22:\"informationblockplugin\";i:2;s:7:\"Visitor\";i:3;s:6:\"INDEXS\";i:4;s:18:\"WebFeedBlockPlugin\";i:5;s:23:\"subscriptionblockplugin\";}', NULL),
(1, '', 'source', '0', NULL),
(1, '', 'subjects', '0', NULL),
(1, '', 'supportedFormLocales', 'a:1:{i:0;s:5:\"en_US\";}', NULL),
(1, '', 'supportedLocales', 'a:2:{i:0;s:5:\"id_ID\";i:1;s:5:\"en_US\";}', NULL),
(1, '', 'supportedSubmissionLocales', 'a:1:{i:0;s:5:\"en_US\";}', NULL),
(1, '', 'supportEmail', 'rizal_kamsurya@indo-intelectual.id', NULL),
(1, '', 'supportName', 'Rizal Kamsurya', NULL),
(1, '', 'supportPhone', '08524611246', NULL),
(1, '', 'themePluginPath', 'bootstrap3', NULL),
(1, '', 'type', '0', NULL),
(1, 'en_US', 'about', '<div>\n<div>|nl2br</div>\n</div>', NULL),
(1, 'en_US', 'acronym', 'IFI-JEB', NULL),
(1, 'en_US', 'additionalHomeContent', '<hr />\n<table width=\"100%\">\n<tbody>\n<tr>\n<td width=\"30%\">Jurnal Title</td>\n<td width=\"2%\">:</td>\n<td width=\"70%\"><strong>Indo-Fintech Intellectuals: Journal of Economics and Busines</strong></td>\n</tr>\n<tr>\n<td>Initials</td>\n<td>:</td>\n<td><strong>IFI-JEB</strong></td>\n</tr>\n<tr>\n<td>Frequency</td>\n<td>:</td>\n<td><strong>2 Issue per Year (March and September)</strong></td>\n</tr>\n<tr>\n<td>DOI</td>\n<td>:</td>\n<td>-</td>\n</tr>\n<tr>\n<td>Print ISSN</td>\n<td>:</td>\n<td>-</td>\n</tr>\n<tr>\n<td>Online ISSN</td>\n<td>:</td>\n<td>-</td>\n</tr>\n<tr>\n<td>Editor-in-chief</td>\n<td>:</td>\n<td><strong>Darlin Aulia, S.E., M.S.Ak</strong></td>\n</tr>\n<tr>\n<td>Publisher</td>\n<td>:</td>\n<td><strong>Lembaga Intelektual Muda (LIM) Maluku</strong></td>\n</tr>\n<tr>\n<td>Citation Analysis</td>\n<td>:</td>\n<td><strong>Google Scholar | Garuda | PKP Index</strong></td>\n</tr>\n</tbody>\n</table>\n<hr />', NULL),
(1, 'en_US', 'authorGuidelines', '<p>Author Guidelines</p>', NULL),
(1, 'en_US', 'authorInformation', 'Interested in submitting to this journal? We recommend that you review the <a href=\"http://localhost/ojs/index.phpifi/about\">About the Journal</a> page for the journal\'s section policies, as well as the <a href=\"http://localhost/ojs/index.phpifi/about/submissions#authorGuidelines\">Author Guidelines</a>. Authors need to <a href=\"http://localhost/ojs/index.phpifi/user/register\">register</a> with the journal prior to submitting or, if already registered, can simply <a href=\"http://localhost/ojs/index.php/index/login\">log in</a> and begin the five-step process.', NULL),
(1, 'en_US', 'clockssLicense', 'This journal utilizes the CLOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href=\"http://clockss.org/\">More...</a>', NULL),
(1, 'en_US', 'copyrightNotice', '<p>This work is licensed under a <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\">Creative Commons Attribution-ShareAlike 4.0 International (CC-BY-SA 4.0)</a> License.<br /><br />Authors who publish with this journal agree to the following terms:<br /><br />1) Authors retain copyright and grant the journal right of first publication with the work simultaneously licensed under a <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\">Creative Commons Attribution-ShareAlike (CC-BY-SA 4.0)</a> that allows others to share the work with an acknowledgement of the work\'s authorship and initial publication in this journal.<br /><br />2) Authors are able to enter into separate, additional contractual arrangements for the non-exclusive distribution of the journal\'s published version of the work (e.g., post it to an institutional repository or publish it in a book), with an acknowledgement of its initial publication in this journal.<br /><br />3) Authors are permitted and encouraged to post their work online (e.g., in institutional repositories or on their website) prior to and during the submission process, as it can lead to productive exchanges, as well as earlier and greater citation of published work.</p>', NULL),
(1, 'en_US', 'description', '<p>Indo-Fintech Intellectuals: Journal Economics and Business (IFI-JEB) is a fully refereed (double-blind peer review) and an open-access online journal for academics, researchers, graduate students, early-career researchers and students, published by Lembaga Intelektual Muda (LIM) Maluku. IFI-JEB is a periodical publication (two times a year, in March and September) with the primary objective to disseminate scientific articles in the fields of economics, business, and accounting. IFI-JEB is indexed by Directory of Open Access Journals, Indonesian Scientific Journal Database, Google Scholar, and several others including Crossref, Sinta, Road, OneSearch, Garuda, PKP Index and WorldCat.</p>', NULL),
(1, 'en_US', 'editorialTeam', '<p><strong>Editor in Chief </strong></p>\n<p>Darlin Aulia, SE., M.S.Ak</p>\n<p> </p>\n<p><strong>Editiorial Board</strong></p>\n<p>Ir. Budi Kramadibrata, M.BA</p>\n<p>Dr. Andi Heru Susanto. M.Si</p>\n<p>Rizal Kamsurya</p>\n<p>Oktaviani Ari W. SE., M.Sc (Univeritas Jember)</p>\n<p> </p>\n<p><strong>Revieweer</strong></p>\n<p>Andiyani Sukmasari, SE., MM</p>\n<p>Lu’lu’ul Jannah</p>\n<p>Kurnia Indah Sumunar</p>\n<p>Lutfi Alhazami</p>\n<p>Erni Kurniawati</p>\n<p>Zikri Kurniawan</p>', NULL),
(1, 'en_US', 'homepageImage', 'a:6:{s:4:\"name\";s:9:\"cover.jpg\";s:10:\"uploadName\";s:23:\"homepageImage_en_US.jpg\";s:5:\"width\";i:630;s:6:\"height\";i:891;s:12:\"dateUploaded\";s:19:\"2021-01-17 18:19:38\";s:7:\"altText\";s:0:\"\";}', NULL),
(1, 'en_US', 'librarianInformation', 'We encourage research librarians to list this journal among their library\'s electronic journal holdings. As well, it may be worth noting that this journal\'s open source publishing system is suitable for libraries to host for their faculty members to use with journals they are involved in editing (see <a href=\"http://pkp.sfu.ca/ojs\">Open Journal Systems</a>).', NULL),
(1, 'en_US', 'lockssLicense', 'This journal utilizes the LOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href=\"http://www.lockss.org/\">More...</a>', NULL),
(1, 'en_US', 'name', 'Indo-Fintech Intellectuals: Journal Economics and Business', NULL),
(1, 'en_US', 'openAccessPolicy', 'This journal provides immediate open access to its content on the principle that making research freely available to the public supports a greater global exchange of knowledge.', NULL),
(1, 'en_US', 'pageHeaderLogoImage', 'a:6:{s:4:\"name\";s:11:\"banner3.jpg\";s:10:\"uploadName\";s:29:\"pageHeaderLogoImage_en_US.jpg\";s:5:\"width\";i:1138;s:6:\"height\";i:164;s:12:\"dateUploaded\";s:19:\"2021-01-17 12:03:58\";s:7:\"altText\";s:0:\"\";}', NULL),
(1, 'en_US', 'privacyStatement', '<p>The names and email addresses entered in this journal site will be used exclusively for the stated purposes of this journal and will not be made available for any other purpose or to any other party.</p>', NULL),
(1, 'en_US', 'readerInformation', 'We encourage readers to sign up for the publishing notification service for this journal. Use the <a href=\"http://localhost/ojs/index.phpifi/user/register\">Register</a> link at the top of the home page for the journal. This registration will result in the reader receiving the Table of Contents by email for each new issue of the journal. This list also allows the journal to claim a certain level of support or readership. See the journal\'s <a href=\"http://localhost/ojs/index.phpifi/about/submissions#privacyStatement\">Privacy Statement</a>, which assures readers that their name and email address will not be used for other purposes.', NULL),
(1, 'en_US', 'submissionChecklist', 'a:5:{i:0;a:2:{s:5:\"order\";i:1;s:7:\"content\";s:165:\"The submission has not been previously published, nor is it before another journal for consideration (or an explanation has been provided in Comments to the Editor).\";}i:1;a:2:{s:5:\"order\";i:2;s:7:\"content\";s:82:\"The submission file is in OpenOffice, Microsoft Word, or RTF document file format.\";}i:2;a:2:{s:5:\"order\";i:3;s:7:\"content\";s:60:\"Where available, URLs for the references have been provided.\";}i:3;a:2:{s:5:\"order\";i:4;s:7:\"content\";s:239:\"The text is single-spaced; uses a 12-point font; employs italics, rather than underlining (except with URL addresses); and all illustrations, figures, and tables are placed within the text at the appropriate points, rather than at the end.\";}i:4;a:2:{s:5:\"order\";i:5;s:7:\"content\";s:99:\"The text adheres to the stylistic and bibliographic requirements outlined in the Author Guidelines.\";}}', NULL),
(1, 'id_ID', 'acronym', NULL, NULL),
(1, 'id_ID', 'authorInformation', 'Tertarik menerbitkan jurnal? Kami merekomendasikan Anda mereview halaman <a href=\"http://localhost/ojs/index.phpifi/about\">Tentang Kami </a>untuk kebijakan bagian jurnal serta <a href=\"http://localhost/ojs/index.phpifi/about/submissions#authorGuidelines\">Petunjuk Penulis </a>. Penulis perlu <a href=\"http://localhost/ojs/index.phpifi/user/register\">Mendaftar </a>dengan jurnal sebelum menyerahkan atau jika sudah terdaftar <a href=\"http://localhost/ojs/index.php/index/login\">login</a>dan mulai proses lima langkah.', NULL),
(1, 'id_ID', 'clockssLicense', 'Journal ini memakai sistem CLOCKSS untum membuat sistem pengarsipan terdistribusi di antara pustaka yang turut berpartisipasi dan mengizinkan pustaka tersebut membuat arsip jurnal secara permanen untuk tujuan pemeliharaan dan pemulihan. <a href=\"http://clockss.org/\">More...</a>', NULL),
(1, 'id_ID', 'librarianInformation', 'Kami mendorong pustakawan riset untuk mendaftar jurnal diantara pemegang jurnal eletronik perpustakaan. Begitu juga, ini mungkin berharga bahwa sistem penerbitan sumber terbuka jurnal cocok untuk perpustakaan untuk menjadi tuan rumah untuk anggota fakultas untuk menggunakan jurnal saat mereka terlibat dalam proses editing. (Lihat <a href=\"http://pkp.sfu.ca/ojs\">Open Journal Systems</a>).', NULL),
(1, 'id_ID', 'lockssLicense', 'OJS sistem LOCKSS berfungsi sebagai sistem pengarsipan terdistribusi antar-perpustakaan yang menggunakan sistem ini dengan tujuan membuat arsip permanen (untuk preservasi dan restorasi). <a href=\"http://www.lockss.org/\">Lanjut...</a>', NULL),
(1, 'id_ID', 'name', NULL, NULL),
(1, 'id_ID', 'openAccessPolicy', 'Jurnal ini menyediakan akses terbuka yang pada prinsipnya membuat riset tersedia secara gratis untuk publik dan akan mensupport pertukaran pengetahuan global terbesar.', NULL),
(1, 'id_ID', 'privacyStatement', '<p>Nama dan alamat email yang dimasukkan di website ini hanya akan digunakan untuk tujuan yang sudah disebutkan, tidak akan disalahgunakan untuk tujuan lain atau untuk disebarluaskan ke pihak lain.</p>', NULL),
(1, 'id_ID', 'readerInformation', 'Kami mendorong pembaca untuk mendaftarkan diri di layanan notifikasi penerbitan untuk jurnal ini. Gunakan tautan <a href=\"http://localhost/ojs/index.phpifi/user/register\">Daftar</a>di bagian atas beranda jurnal. Dengan mendaftar, pembaca akan memperoleh email berisi Daftar Isi tiap ada terbitan jurnal baru. Daftar ini juga membuat jurnal dapat mengetahui tingkat dukungan atau jumlah pembaca. Lihat jurnal <a href=\"http://localhost/ojs/index.phpifi/about/submissions#privacyStatement\">Pernyataan Privasi</a>, yang meyakinkan pembaca bahwa nama dan alamat email yang didaftarkan tidak akan digunakan untuk tujuan lain.', NULL),
(1, 'id_ID', 'submissionChecklist', 'a:5:{i:0;a:2:{s:5:\"order\";i:1;s:7:\"content\";s:166:\"Naskah belum pernah diterbitkan sebelumnya, dan tidak sedang dalam pertimbangan untuk diterbitkan di jurnal lain (atau sudah dijelaskan dalam Komentar kepada Editor).\";}i:1;a:2:{s:5:\"order\";i:2;s:7:\"content\";s:70:\"File naskah dalam format dokumen OpenOffice, Microsoft Word, atau RTF.\";}i:2;a:2:{s:5:\"order\";i:3;s:7:\"content\";s:61:\"Referensi yang dapat diakses online telah dituliskan URL-nya.\";}i:3;a:2:{s:5:\"order\";i:4;s:7:\"content\";s:257:\"Naskah diketik dengan teks 1 spasi; font 12; menggunakan huruf miring, bukan huruf bergaris bawah (kecuali alamat URL); dan semua ilustrasi, gambar, dan tabel diletakkan dalam teks pada tempat yang diharapkan, bukan dikelompokkan tersendiri di akhir naskah.\";}i:4;a:2:{s:5:\"order\";i:5;s:7:\"content\";s:95:\"Naskah mengikuti aturan gaya selingkung dan bibliografi yang disyaratkan dalam Panduan Penulis.\";}}', NULL),
(2, '', 'copyrightYearBasis', 'issue', NULL),
(2, '', 'defaultReviewMode', '2', NULL),
(2, '', 'emailSignature', '<br/>\n________________________________________________________________________<br/>\n<a href=\"http://localhost/ojs/index.php/1\">1</a>', NULL),
(2, '', 'enableOai', '1', NULL),
(2, '', 'itemsPerPage', '25', NULL),
(2, '', 'keywords', 'request', NULL),
(2, '', 'membershipFee', '0', NULL),
(2, '', 'numPageLinks', '10', NULL),
(2, '', 'numWeeksPerResponse', '4', NULL),
(2, '', 'numWeeksPerReview', '4', NULL),
(2, '', 'publicationFee', '0', NULL),
(2, '', 'purchaseArticleFee', '0', NULL),
(2, '', 'supportedFormLocales', 'a:1:{i:0;s:5:\"en_US\";}', NULL),
(2, '', 'supportedLocales', 'a:2:{i:0;s:5:\"id_ID\";i:1;s:5:\"en_US\";}', NULL),
(2, '', 'supportedSubmissionLocales', 'a:1:{i:0;s:5:\"en_US\";}', NULL),
(2, '', 'themePluginPath', 'bootstrap3', NULL),
(2, 'en_US', 'acronym', 'IMEJ', NULL),
(2, 'en_US', 'authorInformation', 'Interested in submitting to this journal? We recommend that you review the <a href=\"http://localhost/ojs/index.php1/about\">About the Journal</a> page for the journal\'s section policies, as well as the <a href=\"http://localhost/ojs/index.php1/about/submissions#authorGuidelines\">Author Guidelines</a>. Authors need to <a href=\"http://localhost/ojs/index.php1/user/register\">register</a> with the journal prior to submitting or, if already registered, can simply <a href=\"http://localhost/ojs/index.php/index/login\">log in</a> and begin the five-step process.', NULL),
(2, 'en_US', 'clockssLicense', 'This journal utilizes the CLOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href=\"http://clockss.org/\">More...</a>', NULL),
(2, 'en_US', 'librarianInformation', 'We encourage research librarians to list this journal among their library\'s electronic journal holdings. As well, it may be worth noting that this journal\'s open source publishing system is suitable for libraries to host for their faculty members to use with journals they are involved in editing (see <a href=\"http://pkp.sfu.ca/ojs\">Open Journal Systems</a>).', NULL),
(2, 'en_US', 'lockssLicense', 'This journal utilizes the LOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href=\"http://www.lockss.org/\">More...</a>', NULL),
(2, 'en_US', 'name', 'Indo-MathEdu Intellectuals Journal', NULL),
(2, 'en_US', 'openAccessPolicy', 'This journal provides immediate open access to its content on the principle that making research freely available to the public supports a greater global exchange of knowledge.', NULL),
(2, 'en_US', 'privacyStatement', '<p>The names and email addresses entered in this journal site will be used exclusively for the stated purposes of this journal and will not be made available for any other purpose or to any other party.</p>', NULL),
(2, 'en_US', 'readerInformation', 'We encourage readers to sign up for the publishing notification service for this journal. Use the <a href=\"http://localhost/ojs/index.php1/user/register\">Register</a> link at the top of the home page for the journal. This registration will result in the reader receiving the Table of Contents by email for each new issue of the journal. This list also allows the journal to claim a certain level of support or readership. See the journal\'s <a href=\"http://localhost/ojs/index.php1/about/submissions#privacyStatement\">Privacy Statement</a>, which assures readers that their name and email address will not be used for other purposes.', NULL),
(2, 'en_US', 'submissionChecklist', 'a:5:{i:0;a:2:{s:5:\"order\";i:1;s:7:\"content\";s:165:\"The submission has not been previously published, nor is it before another journal for consideration (or an explanation has been provided in Comments to the Editor).\";}i:1;a:2:{s:5:\"order\";i:2;s:7:\"content\";s:82:\"The submission file is in OpenOffice, Microsoft Word, or RTF document file format.\";}i:2;a:2:{s:5:\"order\";i:3;s:7:\"content\";s:60:\"Where available, URLs for the references have been provided.\";}i:3;a:2:{s:5:\"order\";i:4;s:7:\"content\";s:239:\"The text is single-spaced; uses a 12-point font; employs italics, rather than underlining (except with URL addresses); and all illustrations, figures, and tables are placed within the text at the appropriate points, rather than at the end.\";}i:4;a:2:{s:5:\"order\";i:5;s:7:\"content\";s:99:\"The text adheres to the stylistic and bibliographic requirements outlined in the Author Guidelines.\";}}', NULL),
(2, 'id_ID', 'acronym', NULL, NULL),
(2, 'id_ID', 'authorInformation', 'Tertarik menerbitkan jurnal? Kami merekomendasikan Anda mereview halaman <a href=\"http://localhost/ojs/index.php1/about\">Tentang Kami </a>untuk kebijakan bagian jurnal serta <a href=\"http://localhost/ojs/index.php1/about/submissions#authorGuidelines\">Petunjuk Penulis </a>. Penulis perlu <a href=\"http://localhost/ojs/index.php1/user/register\">Mendaftar </a>dengan jurnal sebelum menyerahkan atau jika sudah terdaftar <a href=\"http://localhost/ojs/index.php/index/login\">login</a>dan mulai proses lima langkah.', NULL),
(2, 'id_ID', 'clockssLicense', 'Journal ini memakai sistem CLOCKSS untum membuat sistem pengarsipan terdistribusi di antara pustaka yang turut berpartisipasi dan mengizinkan pustaka tersebut membuat arsip jurnal secara permanen untuk tujuan pemeliharaan dan pemulihan. <a href=\"http://clockss.org/\">More...</a>', NULL),
(2, 'id_ID', 'librarianInformation', 'Kami mendorong pustakawan riset untuk mendaftar jurnal diantara pemegang jurnal eletronik perpustakaan. Begitu juga, ini mungkin berharga bahwa sistem penerbitan sumber terbuka jurnal cocok untuk perpustakaan untuk menjadi tuan rumah untuk anggota fakultas untuk menggunakan jurnal saat mereka terlibat dalam proses editing. (Lihat <a href=\"http://pkp.sfu.ca/ojs\">Open Journal Systems</a>).', NULL),
(2, 'id_ID', 'lockssLicense', 'OJS sistem LOCKSS berfungsi sebagai sistem pengarsipan terdistribusi antar-perpustakaan yang menggunakan sistem ini dengan tujuan membuat arsip permanen (untuk preservasi dan restorasi). <a href=\"http://www.lockss.org/\">Lanjut...</a>', NULL),
(2, 'id_ID', 'name', NULL, NULL),
(2, 'id_ID', 'openAccessPolicy', 'Jurnal ini menyediakan akses terbuka yang pada prinsipnya membuat riset tersedia secara gratis untuk publik dan akan mensupport pertukaran pengetahuan global terbesar.', NULL),
(2, 'id_ID', 'privacyStatement', '<p>Nama dan alamat email yang dimasukkan di website ini hanya akan digunakan untuk tujuan yang sudah disebutkan, tidak akan disalahgunakan untuk tujuan lain atau untuk disebarluaskan ke pihak lain.</p>', NULL),
(2, 'id_ID', 'readerInformation', 'Kami mendorong pembaca untuk mendaftarkan diri di layanan notifikasi penerbitan untuk jurnal ini. Gunakan tautan <a href=\"http://localhost/ojs/index.php1/user/register\">Daftar</a>di bagian atas beranda jurnal. Dengan mendaftar, pembaca akan memperoleh email berisi Daftar Isi tiap ada terbitan jurnal baru. Daftar ini juga membuat jurnal dapat mengetahui tingkat dukungan atau jumlah pembaca. Lihat jurnal <a href=\"http://localhost/ojs/index.php1/about/submissions#privacyStatement\">Pernyataan Privasi</a>, yang meyakinkan pembaca bahwa nama dan alamat email yang didaftarkan tidak akan digunakan untuk tujuan lain.', NULL),
(2, 'id_ID', 'submissionChecklist', 'a:5:{i:0;a:2:{s:5:\"order\";i:1;s:7:\"content\";s:166:\"Naskah belum pernah diterbitkan sebelumnya, dan tidak sedang dalam pertimbangan untuk diterbitkan di jurnal lain (atau sudah dijelaskan dalam Komentar kepada Editor).\";}i:1;a:2:{s:5:\"order\";i:2;s:7:\"content\";s:70:\"File naskah dalam format dokumen OpenOffice, Microsoft Word, atau RTF.\";}i:2;a:2:{s:5:\"order\";i:3;s:7:\"content\";s:61:\"Referensi yang dapat diakses online telah dituliskan URL-nya.\";}i:3;a:2:{s:5:\"order\";i:4;s:7:\"content\";s:257:\"Naskah diketik dengan teks 1 spasi; font 12; menggunakan huruf miring, bukan huruf bergaris bawah (kecuali alamat URL); dan semua ilustrasi, gambar, dan tabel diletakkan dalam teks pada tempat yang diharapkan, bukan dikelompokkan tersendiri di akhir naskah.\";}i:4;a:2:{s:5:\"order\";i:5;s:7:\"content\";s:95:\"Naskah mengikuti aturan gaya selingkung dan bibliografi yang disyaratkan dalam Panduan Penulis.\";}}', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `library_files`
--

DROP TABLE IF EXISTS `library_files`;
CREATE TABLE IF NOT EXISTS `library_files` (
  `file_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `context_id` bigint(20) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `original_file_name` varchar(255) NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `file_size` bigint(20) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `date_uploaded` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `submission_id` bigint(20) NOT NULL,
  `public_access` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`file_id`),
  KEY `library_files_context_id` (`context_id`),
  KEY `library_files_submission_id` (`submission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `library_file_settings`
--

DROP TABLE IF EXISTS `library_file_settings`;
CREATE TABLE IF NOT EXISTS `library_file_settings` (
  `file_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `library_file_settings_pkey` (`file_id`,`locale`,`setting_name`),
  KEY `library_file_settings_id` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `metadata_descriptions`
--

DROP TABLE IF EXISTS `metadata_descriptions`;
CREATE TABLE IF NOT EXISTS `metadata_descriptions` (
  `metadata_description_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assoc_type` bigint(20) NOT NULL DEFAULT '0',
  `assoc_id` bigint(20) NOT NULL DEFAULT '0',
  `schema_namespace` varchar(255) NOT NULL,
  `schema_name` varchar(255) NOT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `seq` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`metadata_description_id`),
  KEY `metadata_descriptions_assoc` (`assoc_type`,`assoc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `metadata_description_settings`
--

DROP TABLE IF EXISTS `metadata_description_settings`;
CREATE TABLE IF NOT EXISTS `metadata_description_settings` (
  `metadata_description_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `metadata_descripton_settings_pkey` (`metadata_description_id`,`locale`,`setting_name`),
  KEY `metadata_description_settings_id` (`metadata_description_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `metrics`
--

DROP TABLE IF EXISTS `metrics`;
CREATE TABLE IF NOT EXISTS `metrics` (
  `load_id` varchar(255) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  `pkp_section_id` bigint(20) DEFAULT NULL,
  `assoc_object_type` bigint(20) DEFAULT NULL,
  `assoc_object_id` bigint(20) DEFAULT NULL,
  `submission_id` bigint(20) DEFAULT NULL,
  `representation_id` bigint(20) DEFAULT NULL,
  `assoc_type` bigint(20) NOT NULL,
  `assoc_id` bigint(20) NOT NULL,
  `day` varchar(8) DEFAULT NULL,
  `month` varchar(6) DEFAULT NULL,
  `file_type` tinyint(4) DEFAULT NULL,
  `country_id` varchar(2) DEFAULT NULL,
  `region` varchar(2) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `metric_type` varchar(255) NOT NULL,
  `metric` int(11) NOT NULL,
  KEY `metrics_load_id` (`load_id`),
  KEY `metrics_metric_type_context_id` (`metric_type`,`context_id`),
  KEY `metrics_metric_type_submission_id_assoc_type` (`metric_type`,`submission_id`,`assoc_type`),
  KEY `metrics_metric_type_submission_id_assoc` (`metric_type`,`context_id`,`assoc_type`,`assoc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `metrics`
--

INSERT INTO `metrics` (`load_id`, `context_id`, `pkp_section_id`, `assoc_object_type`, `assoc_object_id`, `submission_id`, `representation_id`, `assoc_type`, `assoc_id`, `day`, `month`, `file_type`, `country_id`, `region`, `city`, `metric_type`, `metric`) VALUES
('usage_events_20210117.log', 1, NULL, NULL, NULL, NULL, NULL, 256, 1, '20210117', '202101', NULL, NULL, NULL, NULL, 'ojs::counter', 191),
('usage_events_20210117.log', 2, NULL, NULL, NULL, NULL, NULL, 256, 2, '20210117', '202101', NULL, NULL, NULL, NULL, 'ojs::counter', 1),
('usage_events_20210118.log', 1, NULL, NULL, NULL, NULL, NULL, 256, 1, '20210118', '202101', NULL, NULL, NULL, NULL, 'ojs::counter', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `navigation_menus`
--

DROP TABLE IF EXISTS `navigation_menus`;
CREATE TABLE IF NOT EXISTS `navigation_menus` (
  `navigation_menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `context_id` bigint(20) NOT NULL,
  `area_name` varchar(255) DEFAULT '',
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`navigation_menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `navigation_menus`
--

INSERT INTO `navigation_menus` (`navigation_menu_id`, `context_id`, `area_name`, `title`) VALUES
(1, 0, 'user', 'User Navigation Menu'),
(11, 0, 'primary', 'Menu Utama'),
(23, 1, 'user', 'User Navigation Menu'),
(24, 1, 'primary', 'Primary Navigation Menu'),
(25, 2, 'user', 'User Navigation Menu'),
(26, 2, 'primary', 'Primary Navigation Menu');

-- --------------------------------------------------------

--
-- Struktur dari tabel `navigation_menu_items`
--

DROP TABLE IF EXISTS `navigation_menu_items`;
CREATE TABLE IF NOT EXISTS `navigation_menu_items` (
  `navigation_menu_item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `context_id` bigint(20) NOT NULL,
  `url` varchar(255) DEFAULT '',
  `path` varchar(255) DEFAULT '',
  `type` varchar(255) DEFAULT '',
  PRIMARY KEY (`navigation_menu_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `navigation_menu_items`
--

INSERT INTO `navigation_menu_items` (`navigation_menu_item_id`, `context_id`, `url`, `path`, `type`) VALUES
(1, 0, NULL, NULL, 'NMI_TYPE_USER_REGISTER'),
(2, 0, NULL, NULL, 'NMI_TYPE_USER_LOGIN'),
(3, 0, NULL, NULL, 'NMI_TYPE_USER_DASHBOARD'),
(4, 0, NULL, NULL, 'NMI_TYPE_USER_DASHBOARD'),
(5, 0, NULL, NULL, 'NMI_TYPE_USER_PROFILE'),
(6, 0, NULL, NULL, 'NMI_TYPE_ADMINISTRATION'),
(7, 0, NULL, NULL, 'NMI_TYPE_USER_LOGOUT'),
(76, 0, '', '', 'NMI_TYPE_ABOUT'),
(77, 0, '', '', 'NMI_TYPE_EDITORIAL_TEAM'),
(78, 0, '', '', 'NMI_TYPE_ABOUT'),
(79, 0, '', '', 'NMI_TYPE_SUBMISSIONS'),
(80, 0, '', '', 'NMI_TYPE_CURRENT'),
(81, 0, '', '', 'NMI_TYPE_ARCHIVES'),
(83, 0, '', '', 'NMI_TYPE_ANNOUNCEMENTS'),
(84, 0, '', '', 'NMI_TYPE_CONTACT'),
(85, 0, '', '', 'NMI_TYPE_EDITORIAL_TEAM'),
(134, 0, '', 'index/index', 'NMI_TYPE_CUSTOM'),
(188, 1, NULL, NULL, 'NMI_TYPE_USER_REGISTER'),
(189, 1, NULL, NULL, 'NMI_TYPE_USER_LOGIN'),
(190, 1, NULL, NULL, 'NMI_TYPE_USER_DASHBOARD'),
(191, 1, NULL, NULL, 'NMI_TYPE_USER_DASHBOARD'),
(192, 1, NULL, NULL, 'NMI_TYPE_USER_PROFILE'),
(193, 1, NULL, NULL, 'NMI_TYPE_ADMINISTRATION'),
(194, 1, NULL, NULL, 'NMI_TYPE_USER_LOGOUT'),
(195, 1, NULL, NULL, 'NMI_TYPE_CURRENT'),
(196, 1, NULL, NULL, 'NMI_TYPE_ARCHIVES'),
(197, 1, NULL, NULL, 'NMI_TYPE_ANNOUNCEMENTS'),
(198, 1, NULL, NULL, 'NMI_TYPE_ABOUT'),
(199, 1, NULL, NULL, 'NMI_TYPE_ABOUT'),
(200, 1, NULL, NULL, 'NMI_TYPE_SUBMISSIONS'),
(201, 1, NULL, NULL, 'NMI_TYPE_EDITORIAL_TEAM'),
(202, 1, NULL, NULL, 'NMI_TYPE_PRIVACY'),
(204, 1, NULL, NULL, 'NMI_TYPE_SEARCH'),
(205, 2, NULL, NULL, 'NMI_TYPE_USER_REGISTER'),
(206, 2, NULL, NULL, 'NMI_TYPE_USER_LOGIN'),
(207, 2, NULL, NULL, 'NMI_TYPE_USER_DASHBOARD'),
(208, 2, NULL, NULL, 'NMI_TYPE_USER_DASHBOARD'),
(209, 2, NULL, NULL, 'NMI_TYPE_USER_PROFILE'),
(210, 2, NULL, NULL, 'NMI_TYPE_ADMINISTRATION'),
(211, 2, NULL, NULL, 'NMI_TYPE_USER_LOGOUT'),
(212, 2, NULL, NULL, 'NMI_TYPE_CURRENT'),
(213, 2, NULL, NULL, 'NMI_TYPE_ARCHIVES'),
(214, 2, NULL, NULL, 'NMI_TYPE_ANNOUNCEMENTS'),
(215, 2, NULL, NULL, 'NMI_TYPE_ABOUT'),
(216, 2, NULL, NULL, 'NMI_TYPE_ABOUT'),
(217, 2, NULL, NULL, 'NMI_TYPE_SUBMISSIONS'),
(218, 2, NULL, NULL, 'NMI_TYPE_EDITORIAL_TEAM'),
(219, 2, NULL, NULL, 'NMI_TYPE_PRIVACY'),
(220, 2, NULL, NULL, 'NMI_TYPE_CONTACT'),
(221, 2, NULL, NULL, 'NMI_TYPE_SEARCH'),
(222, 1, '', 'index/index', 'NMI_TYPE_CUSTOM'),
(223, 1, '', '', 'NMI_TYPE_SUBMISSIONS'),
(224, 1, '', '', 'NMI_TYPE_CONTACT');

-- --------------------------------------------------------

--
-- Struktur dari tabel `navigation_menu_item_assignments`
--

DROP TABLE IF EXISTS `navigation_menu_item_assignments`;
CREATE TABLE IF NOT EXISTS `navigation_menu_item_assignments` (
  `navigation_menu_item_assignment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `navigation_menu_id` bigint(20) NOT NULL,
  `navigation_menu_item_id` bigint(20) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `seq` bigint(20) DEFAULT '0',
  PRIMARY KEY (`navigation_menu_item_assignment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=620 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `navigation_menu_item_assignments`
--

INSERT INTO `navigation_menu_item_assignments` (`navigation_menu_item_assignment_id`, `navigation_menu_id`, `navigation_menu_item_id`, `parent_id`, `seq`) VALUES
(413, 25, 205, 0, 0),
(414, 25, 206, 0, 1),
(415, 25, 207, 0, 2),
(416, 25, 208, 207, 0),
(417, 25, 209, 207, 1),
(418, 25, 210, 207, 2),
(419, 25, 211, 207, 3),
(420, 26, 212, 0, 0),
(421, 26, 213, 0, 1),
(422, 26, 214, 0, 2),
(423, 26, 215, 0, 3),
(424, 26, 216, 215, 0),
(425, 26, 217, 215, 1),
(426, 26, 218, 215, 2),
(427, 26, 219, 215, 3),
(428, 26, 220, 215, 4),
(515, 11, 4, 0, 0),
(516, 11, 134, 0, 1),
(517, 11, 76, 0, 2),
(518, 11, 1, 0, 3),
(519, 11, 2, 0, 4),
(520, 11, 7, 0, 5),
(608, 24, 222, 0, 0),
(609, 24, 198, 0, 1),
(610, 24, 189, 0, 2),
(611, 24, 204, 0, 3),
(612, 24, 195, 0, 4),
(613, 24, 196, 0, 5),
(614, 24, 197, 0, 6),
(615, 24, 201, 0, 7),
(616, 24, 223, 0, 8),
(617, 24, 191, 0, 9),
(618, 24, 188, 0, 10),
(619, 24, 194, 0, 11);

-- --------------------------------------------------------

--
-- Struktur dari tabel `navigation_menu_item_assignment_settings`
--

DROP TABLE IF EXISTS `navigation_menu_item_assignment_settings`;
CREATE TABLE IF NOT EXISTS `navigation_menu_item_assignment_settings` (
  `navigation_menu_item_assignment_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `navigation_menu_item_assignment_settings_pkey` (`navigation_menu_item_assignment_id`,`locale`,`setting_name`),
  KEY `assignment_settings_navigation_menu_item_assignment_id` (`navigation_menu_item_assignment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `navigation_menu_item_settings`
--

DROP TABLE IF EXISTS `navigation_menu_item_settings`;
CREATE TABLE IF NOT EXISTS `navigation_menu_item_settings` (
  `navigation_menu_item_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` longtext,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `navigation_menu_item_settings_pkey` (`navigation_menu_item_id`,`locale`,`setting_name`),
  KEY `navigation_menu_item_settings_navigation_menu_id` (`navigation_menu_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `navigation_menu_item_settings`
--

INSERT INTO `navigation_menu_item_settings` (`navigation_menu_item_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, '', 'titleLocaleKey', 'navigation.register', 'string'),
(2, '', 'titleLocaleKey', 'navigation.login', 'string'),
(3, '', 'titleLocaleKey', '{$loggedInUsername}', 'string'),
(4, '', 'titleLocaleKey', 'navigation.dashboard', 'string'),
(5, '', 'titleLocaleKey', 'common.viewProfile', 'string'),
(6, '', 'titleLocaleKey', 'navigation.admin', 'string'),
(7, '', 'titleLocaleKey', 'user.logOut', 'string'),
(76, 'en_US', 'content', '', 'string'),
(76, 'en_US', 'title', 'About Us', 'string'),
(76, 'id_ID', 'content', '', 'string'),
(76, 'id_ID', 'title', 'Tentang Kami', 'string'),
(77, 'en_US', 'content', '', 'string'),
(77, 'en_US', 'title', 'Editorial Team', 'string'),
(77, 'id_ID', 'content', '', 'string'),
(77, 'id_ID', 'title', 'Tim Editor', 'string'),
(78, 'en_US', 'content', '', 'string'),
(78, 'en_US', 'title', 'About The Journal', 'string'),
(78, 'id_ID', 'content', '', 'string'),
(78, 'id_ID', 'title', 'Tentang Jurnal', 'string'),
(79, 'en_US', 'content', '', 'string'),
(79, 'en_US', 'title', 'Submissions', 'string'),
(79, 'id_ID', 'content', '', 'string'),
(79, 'id_ID', 'title', 'Pendapat', 'string'),
(80, 'en_US', 'content', '', 'string'),
(80, 'en_US', 'title', 'Current', 'string'),
(80, 'id_ID', 'content', '', 'string'),
(80, 'id_ID', 'title', 'Sedang Beredar', 'string'),
(81, 'en_US', 'content', '', 'string'),
(81, 'en_US', 'title', 'Archives', 'string'),
(81, 'id_ID', 'content', '', 'string'),
(81, 'id_ID', 'title', 'Arsip', 'string'),
(83, 'en_US', 'content', '', 'string'),
(83, 'en_US', 'title', 'Announcements', 'string'),
(83, 'id_ID', 'content', '', 'string'),
(83, 'id_ID', 'title', '', 'string'),
(84, 'en_US', 'content', '', 'string'),
(84, 'en_US', 'title', 'Contact', 'string'),
(84, 'id_ID', 'content', '', 'string'),
(84, 'id_ID', 'title', '', 'string'),
(85, 'en_US', 'content', '', 'string'),
(85, 'en_US', 'title', 'Editorial Team', 'string'),
(85, 'id_ID', 'content', '', 'string'),
(85, 'id_ID', 'title', '', 'string'),
(134, 'en_US', 'content', '', 'string'),
(134, 'en_US', 'title', 'Home', 'string'),
(134, 'id_ID', 'content', '', 'string'),
(134, 'id_ID', 'title', '', 'string'),
(188, '', 'titleLocaleKey', 'navigation.register', 'string'),
(189, '', 'titleLocaleKey', 'navigation.login', 'string'),
(190, '', 'titleLocaleKey', '{$loggedInUsername}', 'string'),
(191, '', 'titleLocaleKey', 'navigation.dashboard', 'string'),
(192, '', 'titleLocaleKey', 'common.viewProfile', 'string'),
(193, '', 'titleLocaleKey', 'navigation.admin', 'string'),
(194, '', 'titleLocaleKey', 'user.logOut', 'string'),
(195, '', 'titleLocaleKey', 'navigation.current', 'string'),
(196, '', 'titleLocaleKey', 'navigation.archives', 'string'),
(197, '', 'titleLocaleKey', 'manager.announcements', 'string'),
(198, '', 'titleLocaleKey', 'navigation.about', 'string'),
(199, '', 'titleLocaleKey', 'about.aboutContext', 'string'),
(200, '', 'titleLocaleKey', 'about.submissions', 'string'),
(201, '', 'titleLocaleKey', 'about.editorialTeam', 'string'),
(202, '', 'titleLocaleKey', 'manager.setup.privacyStatement', 'string'),
(204, '', 'titleLocaleKey', 'common.search', 'string'),
(205, '', 'titleLocaleKey', 'navigation.register', 'string'),
(206, '', 'titleLocaleKey', 'navigation.login', 'string'),
(207, '', 'titleLocaleKey', '{$loggedInUsername}', 'string'),
(208, '', 'titleLocaleKey', 'navigation.dashboard', 'string'),
(209, '', 'titleLocaleKey', 'common.viewProfile', 'string'),
(210, '', 'titleLocaleKey', 'navigation.admin', 'string'),
(211, '', 'titleLocaleKey', 'user.logOut', 'string'),
(212, '', 'titleLocaleKey', 'navigation.current', 'string'),
(213, '', 'titleLocaleKey', 'navigation.archives', 'string'),
(214, '', 'titleLocaleKey', 'manager.announcements', 'string'),
(215, '', 'titleLocaleKey', 'navigation.about', 'string'),
(216, '', 'titleLocaleKey', 'about.aboutContext', 'string'),
(217, '', 'titleLocaleKey', 'about.submissions', 'string'),
(218, '', 'titleLocaleKey', 'about.editorialTeam', 'string'),
(219, '', 'titleLocaleKey', 'manager.setup.privacyStatement', 'string'),
(220, '', 'titleLocaleKey', 'about.contact', 'string'),
(221, '', 'titleLocaleKey', 'common.search', 'string'),
(222, 'en_US', 'content', '', 'string'),
(222, 'en_US', 'title', 'Home', 'string'),
(223, 'en_US', 'content', '', 'string'),
(223, 'en_US', 'title', 'Submissions', 'string'),
(224, 'en_US', 'content', '&lt;br data-mce-bogus=\"1\"&gt;', 'string'),
(224, 'en_US', 'title', 'Contact', 'string');

-- --------------------------------------------------------

--
-- Struktur dari tabel `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE IF NOT EXISTS `notes` (
  `note_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assoc_type` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `contents` text,
  PRIMARY KEY (`note_id`),
  KEY `notes_assoc` (`assoc_type`,`assoc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `notification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `context_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `level` bigint(20) NOT NULL,
  `type` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_read` datetime DEFAULT NULL,
  `assoc_type` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`notification_id`),
  KEY `notifications_context_id_user_id` (`context_id`,`user_id`,`level`),
  KEY `notifications_context_id` (`context_id`,`level`),
  KEY `notifications_assoc` (`assoc_type`,`assoc_id`),
  KEY `notifications_user_id_level` (`user_id`,`level`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `notifications`
--

INSERT INTO `notifications` (`notification_id`, `context_id`, `user_id`, `level`, `type`, `date_created`, `date_read`, `assoc_type`, `assoc_id`) VALUES
(2, 1, 8, 3, 16777258, '2021-01-18 15:33:17', NULL, 0, 0),
(4, 1, 7, 3, 16777258, '2021-01-18 15:33:22', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `notification_mail_list`
--

DROP TABLE IF EXISTS `notification_mail_list`;
CREATE TABLE IF NOT EXISTS `notification_mail_list` (
  `notification_mail_list_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(90) NOT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `token` varchar(40) NOT NULL,
  `context` bigint(20) NOT NULL,
  PRIMARY KEY (`notification_mail_list_id`),
  UNIQUE KEY `notification_mail_list_email_context` (`email`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `notification_settings`
--

DROP TABLE IF EXISTS `notification_settings`;
CREATE TABLE IF NOT EXISTS `notification_settings` (
  `notification_id` bigint(20) NOT NULL,
  `locale` varchar(14) DEFAULT NULL,
  `setting_name` varchar(64) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `notification_settings_pkey` (`notification_id`,`locale`,`setting_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `notification_settings`
--

INSERT INTO `notification_settings` (`notification_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(2, '', 'contents', 'This is a kind reminder for you to check your journal\'s health through the editorial report.', 'string'),
(4, '', 'contents', 'This is a kind reminder for you to check your journal\'s health through the editorial report.', 'string');

-- --------------------------------------------------------

--
-- Struktur dari tabel `notification_subscription_settings`
--

DROP TABLE IF EXISTS `notification_subscription_settings`;
CREATE TABLE IF NOT EXISTS `notification_subscription_settings` (
  `setting_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(64) NOT NULL,
  `setting_value` text,
  `user_id` bigint(20) NOT NULL,
  `context` bigint(20) NOT NULL,
  `setting_type` varchar(6) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oai_resumption_tokens`
--

DROP TABLE IF EXISTS `oai_resumption_tokens`;
CREATE TABLE IF NOT EXISTS `oai_resumption_tokens` (
  `token` varchar(32) NOT NULL,
  `expire` bigint(20) NOT NULL,
  `record_offset` int(11) NOT NULL,
  `params` text,
  UNIQUE KEY `oai_resumption_tokens_pkey` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `plugin_settings`
--

DROP TABLE IF EXISTS `plugin_settings`;
CREATE TABLE IF NOT EXISTS `plugin_settings` (
  `plugin_name` varchar(80) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  `setting_name` varchar(80) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `plugin_settings_pkey` (`plugin_name`,`context_id`,`setting_name`),
  KEY `plugin_settings_plugin_name` (`plugin_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `plugin_settings`
--

INSERT INTO `plugin_settings` (`plugin_name`, `context_id`, `setting_name`, `setting_value`, `setting_type`) VALUES
('acronplugin', 0, 'crontab', 'a:29:{i:0;a:3:{s:9:\"className\";s:43:\"plugins.generic.usageStats.UsageStatsLoader\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:1:{i:0;s:9:\"autoStage\";}}i:1;a:3:{s:9:\"className\";s:43:\"plugins.generic.usageStats.UsageStatsLoader\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:1:{i:0;s:9:\"autoStage\";}}i:2;a:3:{s:9:\"className\";s:48:\"plugins.importexport.crossref.CrossrefInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:3;a:3:{s:9:\"className\";s:48:\"plugins.importexport.datacite.DataciteInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:4;a:3:{s:9:\"className\";s:40:\"plugins.importexport.doaj.DOAJInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:5;a:3:{s:9:\"className\";s:42:\"plugins.importexport.medra.MedraInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:6;a:3:{s:9:\"className\";s:43:\"plugins.generic.usageStats.UsageStatsLoader\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:1:{i:0;s:9:\"autoStage\";}}i:7;a:3:{s:9:\"className\";s:48:\"plugins.importexport.crossref.CrossrefInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:8;a:3:{s:9:\"className\";s:48:\"plugins.importexport.datacite.DataciteInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:9;a:3:{s:9:\"className\";s:40:\"plugins.importexport.doaj.DOAJInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:10;a:3:{s:9:\"className\";s:42:\"plugins.importexport.medra.MedraInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:11;a:3:{s:9:\"className\";s:43:\"plugins.generic.usageStats.UsageStatsLoader\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:1:{i:0;s:9:\"autoStage\";}}i:12;a:3:{s:9:\"className\";s:48:\"plugins.importexport.crossref.CrossrefInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:13;a:3:{s:9:\"className\";s:48:\"plugins.importexport.datacite.DataciteInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:14;a:3:{s:9:\"className\";s:40:\"plugins.importexport.doaj.DOAJInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:15;a:3:{s:9:\"className\";s:42:\"plugins.importexport.medra.MedraInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:16;a:3:{s:9:\"className\";s:43:\"plugins.generic.usageStats.UsageStatsLoader\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:1:{i:0;s:9:\"autoStage\";}}i:17;a:3:{s:9:\"className\";s:48:\"plugins.importexport.crossref.CrossrefInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:18;a:3:{s:9:\"className\";s:48:\"plugins.importexport.datacite.DataciteInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:19;a:3:{s:9:\"className\";s:40:\"plugins.importexport.doaj.DOAJInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:20;a:3:{s:9:\"className\";s:42:\"plugins.importexport.medra.MedraInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:21;a:3:{s:9:\"className\";s:43:\"plugins.generic.usageStats.UsageStatsLoader\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:1:{i:0;s:9:\"autoStage\";}}i:22;a:3:{s:9:\"className\";s:48:\"plugins.importexport.crossref.CrossrefInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:23;a:3:{s:9:\"className\";s:48:\"plugins.importexport.datacite.DataciteInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:24;a:3:{s:9:\"className\";s:40:\"plugins.importexport.doaj.DOAJInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:25;a:3:{s:9:\"className\";s:42:\"plugins.importexport.medra.MedraInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:26;a:3:{s:9:\"className\";s:35:\"lib.pkp.classes.task.ReviewReminder\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:27;a:3:{s:9:\"className\";s:37:\"lib.pkp.classes.task.StatisticsReport\";s:9:\"frequency\";a:1:{s:3:\"day\";s:1:\"1\";}s:4:\"args\";a:0:{}}i:28;a:3:{s:9:\"className\";s:40:\"classes.tasks.SubscriptionExpiryReminder\";s:9:\"frequency\";a:1:{s:3:\"day\";s:1:\"1\";}s:4:\"args\";a:0:{}}}', 'object'),
('acronplugin', 0, 'enabled', '1', 'bool'),
('bootstrapthreethemeplugin', 0, 'bootstrapTheme', 'cosmo', 'string'),
('bootstrapthreethemeplugin', 0, 'enabled', '1', 'bool'),
('bootstrapthreethemeplugin', 1, 'bootstrapTheme', 'yeti', 'string'),
('bootstrapthreethemeplugin', 1, 'enabled', '1', 'bool'),
('bootstrapthreethemeplugin', 2, 'bootstrapTheme', 'journal', 'string'),
('browseblockplugin', 0, 'enabled', '0', 'bool'),
('browseblockplugin', 1, 'enabled', '1', 'bool'),
('citationstylelanguageplugin', 1, 'enabled', '1', 'bool'),
('customblockmanagerplugin', 0, 'blocks', 'a:1:{i:0;s:8:\"Visitors\";}', 'object'),
('customblockmanagerplugin', 0, 'enabled', '1', 'bool'),
('customblockmanagerplugin', 1, 'blocks', 'a:3:{i:0;s:7:\"Visitor\";i:1;s:6:\"INDEXS\";i:2;s:4:\"view\";}', 'object'),
('customblockmanagerplugin', 1, 'enabled', '1', 'bool'),
('defaultthemeplugin', 0, 'baseColour', '#1E6292', 'string'),
('defaultthemeplugin', 0, 'enabled', '1', 'bool'),
('defaultthemeplugin', 0, 'showDescriptionInJournalIndex', 'false', 'string'),
('defaultthemeplugin', 0, 'typography', 'notoSans', 'string'),
('defaultthemeplugin', 0, 'useHomepageImageAsHeader', 'false', 'string'),
('defaultthemeplugin', 1, 'baseColour', '#AEC2D0', 'string'),
('defaultthemeplugin', 1, 'enabled', '1', 'bool'),
('defaultthemeplugin', 1, 'showDescriptionInJournalIndex', 'false', 'string'),
('defaultthemeplugin', 1, 'typography', 'notoSerif_notoSans', 'string'),
('defaultthemeplugin', 1, 'useHomepageImageAsHeader', 'false', 'string'),
('defaultthemeplugin', 2, 'enabled', '1', 'bool'),
('developedbyblockplugin', 0, 'enabled', '0', 'bool'),
('developedbyblockplugin', 0, 'seq', '0', 'int'),
('developedbyblockplugin', 1, 'enabled', '0', 'bool'),
('developedbyblockplugin', 1, 'seq', '0', 'int'),
('developedbyblockplugin', 2, 'enabled', '0', 'bool'),
('developedbyblockplugin', 2, 'seq', '0', 'int'),
('dublincoremetaplugin', 1, 'enabled', '1', 'bool'),
('dublincoremetaplugin', 2, 'enabled', '1', 'bool'),
('googleanalyticsplugin', 1, 'enabled', '1', 'bool'),
('googlescholarplugin', 0, 'enabled', '1', 'bool'),
('googlescholarplugin', 1, 'enabled', '1', 'bool'),
('googlescholarplugin', 2, 'enabled', '1', 'bool'),
('htmlarticlegalleyplugin', 1, 'enabled', '1', 'bool'),
('htmlarticlegalleyplugin', 2, 'enabled', '1', 'bool'),
('indexs', 1, 'blockContent', 'a:1:{s:5:\"en_US\";s:633:\"<p><strong>INDEXS</strong></p>\r\n<hr>\r\n<p><img src=\"http://localhost/ojs/public/site/images/meitro_hartanto/google-scholar-logo-2015-ok.png\" alt=\"\" width=\"146\" height=\"56\"><img src=\"http://localhost/ojs/public/site/images/meitro_hartanto/garuda1.png\" alt=\"\" width=\"154\" height=\"48\"></p>\r\n<p><img src=\"http://localhost/ojs/public/site/images/meitro_hartanto/crossref.png\" alt=\"\" width=\"144\" height=\"45\"><img src=\"http://localhost/ojs/public/site/images/meitro_hartanto/pkp-index.png\" alt=\"\" width=\"144\" height=\"45\"></p>\r\n<p><img src=\"http://localhost/ojs/public/site/images/meitro_hartanto/road.png\" alt=\"\" width=\"144\" height=\"45\"></p>\";}', 'object'),
('indexs', 1, 'enabled', '1', 'bool'),
('informationblockplugin', 0, 'enabled', '1', 'bool'),
('informationblockplugin', 1, 'enabled', '1', 'bool'),
('informationblockplugin', 1, 'seq', '7', 'int'),
('informationblockplugin', 2, 'enabled', '1', 'bool'),
('informationblockplugin', 2, 'seq', '7', 'int'),
('languagetoggleblockplugin', 0, 'enabled', '1', 'bool'),
('languagetoggleblockplugin', 0, 'seq', '4', 'int'),
('languagetoggleblockplugin', 1, 'enabled', '1', 'bool'),
('languagetoggleblockplugin', 1, 'seq', '4', 'int'),
('languagetoggleblockplugin', 2, 'enabled', '1', 'bool'),
('languagetoggleblockplugin', 2, 'seq', '4', 'int'),
('lensgalleyplugin', 1, 'enabled', '1', 'bool'),
('lensgalleyplugin', 2, 'enabled', '1', 'bool'),
('makesubmissionblockplugin', 0, 'enabled', '1', 'bool'),
('makesubmissionblockplugin', 1, 'enabled', '1', 'bool'),
('pdfjsviewerplugin', 0, 'enabled', '1', 'bool'),
('pdfjsviewerplugin', 1, 'enabled', '1', 'bool'),
('pdfjsviewerplugin', 2, 'enabled', '1', 'bool'),
('recommendbyauthorplugin', 1, 'enabled', '1', 'bool'),
('resolverplugin', 1, 'enabled', '1', 'bool'),
('resolverplugin', 2, 'enabled', '1', 'bool'),
('staticpagesplugin', 0, 'enabled', '1', 'bool'),
('staticpagesplugin', 1, 'enabled', '1', 'bool'),
('subscriptionblockplugin', 0, 'enabled', '1', 'bool'),
('subscriptionblockplugin', 1, 'enabled', '1', 'bool'),
('subscriptionblockplugin', 1, 'seq', '2', 'int'),
('subscriptionblockplugin', 2, 'enabled', '1', 'bool'),
('subscriptionblockplugin', 2, 'seq', '2', 'int'),
('tinymceplugin', 0, 'enabled', '1', 'bool'),
('tinymceplugin', 1, 'enabled', '1', 'bool'),
('tinymceplugin', 2, 'enabled', '1', 'bool'),
('usageeventplugin', 0, 'enabled', '1', 'bool'),
('usageeventplugin', 0, 'uniqueSiteId', '5ed0919fdbec6', 'string'),
('usagestatsplugin', 0, 'accessLogFileParseRegex', '/^(?P<ip>\\S+) \\S+ \\S+ \\[(?P<date>.*?)\\] \"\\S+ (?P<url>\\S+).*?\" (?P<returnCode>\\S+) \\S+ \".*?\" \"(?P<userAgent>.*?)\"/', 'string'),
('usagestatsplugin', 0, 'chartType', 'bar', 'string'),
('usagestatsplugin', 0, 'createLogFiles', '1', 'bool'),
('usagestatsplugin', 0, 'datasetMaxCount', '4', 'string'),
('usagestatsplugin', 0, 'enabled', '1', 'bool'),
('usagestatsplugin', 0, 'optionalColumns', 'a:2:{i:0;s:4:\"city\";i:1;s:6:\"region\";}', 'object'),
('view', 1, 'blockContent', 'a:1:{s:5:\"en_US\";s:1032:\"<ul class=\"sidemenu\">\r\n<li><strong><a href=\"/ojs/index.php/ifi/about/submissions#authorGuidelines\">Author Guidelines</a></strong></li>\r\n<li><strong><a href=\"/ojs/index.php/ifi/about/submissions#authorGuidelines\">Editorial Team</a></strong></li>\r\n<li><strong><a href=\"/ojs/index.php/ifi/about/submissions#authorGuidelines\">Focus and Scope</a></strong></li>\r\n<li><strong><a href=\"/ojs/index.php/ifi/about/submissions#authorGuidelines\">Publication Ethics</a></strong></li>\r\n<li><strong><a href=\"/ojs/index.php/ifi/about/submissions#authorGuidelines\">Open Access Policy</a></strong></li>\r\n<li><strong><a href=\"/ojs/index.php/ifi/about/submissions#authorGuidelines\">Peer Review Process</a></strong></li>\r\n<li><strong><a href=\"/ojs/index.php/ifi/about/submissions#authorGuidelines\">Online Submissions</a></strong></li>\r\n<li><strong><a href=\"/ojs/index.php/ifi/about/submissions#authorGuidelines\">Author Fees</a></strong></li>\r\n<li><strong><a href=\"/ojs/index.php/ifi/about/submissions#authorGuidelines\">Contact Us</a></strong></li>\r\n</ul>\";}', 'object'),
('view', 1, 'enabled', '1', 'bool'),
('visitor', 1, 'blockContent', 'a:1:{s:5:\"en_US\";s:283:\"<p><strong>VISITORS</strong></p>\r\n<hr>\r\n<p><a href=\"https://info.flagcounter.com/bNw4\"><img src=\"https://s01.flagcounter.com/count2/bNw4/bg_FFFFFF/txt_6C5AE6/border_CCCCCC/columns_2/maxflags_50/viewers_0/labels_0/pageviews_0/flags_0/percent_0/\" alt=\"Flag Counter\" border=\"0\"></a></p>\";}', 'object'),
('visitor', 1, 'enabled', '1', 'bool'),
('visitors', 0, 'blockContent', 'a:2:{s:5:\"en_US\";s:243:\"<p><a href=\"https://info.flagcounter.com/bNw4\"><img src=\"https://s01.flagcounter.com/count2/bNw4/bg_FFFFFF/txt_6C5AE6/border_CCCCCC/columns_2/maxflags_50/viewers_0/labels_0/pageviews_0/flags_0/percent_0/\" alt=\"Flag Counter\" border=\"0\"></a></p>\";s:5:\"id_ID\";s:0:\"\";}', 'object'),
('visitors', 0, 'enabled', '1', 'bool'),
('webfeedplugin', 1, 'displayItems', '1', 'bool'),
('webfeedplugin', 1, 'displayPage', 'homepage', 'string'),
('webfeedplugin', 1, 'enabled', '1', 'bool'),
('webfeedplugin', 2, 'displayItems', '1', 'bool'),
('webfeedplugin', 2, 'displayPage', 'homepage', 'string'),
('webfeedplugin', 2, 'enabled', '1', 'bool');

-- --------------------------------------------------------

--
-- Struktur dari tabel `publications`
--

DROP TABLE IF EXISTS `publications`;
CREATE TABLE IF NOT EXISTS `publications` (
  `publication_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `access_status` bigint(20) DEFAULT '0',
  `date_published` date DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `locale` varchar(14) DEFAULT NULL,
  `primary_contact_id` bigint(20) DEFAULT NULL,
  `section_id` bigint(20) DEFAULT NULL,
  `seq` double NOT NULL DEFAULT '0',
  `submission_id` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `url_path` varchar(64) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`publication_id`),
  KEY `publications_submission_id` (`submission_id`),
  KEY `publications_section_id` (`section_id`),
  KEY `publications_url_path` (`url_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `publication_categories`
--

DROP TABLE IF EXISTS `publication_categories`;
CREATE TABLE IF NOT EXISTS `publication_categories` (
  `publication_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  UNIQUE KEY `publication_categories_id` (`publication_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `publication_galleys`
--

DROP TABLE IF EXISTS `publication_galleys`;
CREATE TABLE IF NOT EXISTS `publication_galleys` (
  `galley_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `locale` varchar(14) DEFAULT NULL,
  `publication_id` bigint(20) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `file_id` bigint(20) DEFAULT NULL,
  `seq` double NOT NULL DEFAULT '0',
  `remote_url` varchar(2047) DEFAULT NULL,
  `is_approved` tinyint(4) NOT NULL DEFAULT '0',
  `url_path` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`galley_id`),
  KEY `publication_galleys_publication_id` (`publication_id`),
  KEY `publication_galleys_url_path` (`url_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `publication_galley_settings`
--

DROP TABLE IF EXISTS `publication_galley_settings`;
CREATE TABLE IF NOT EXISTS `publication_galley_settings` (
  `galley_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  UNIQUE KEY `publication_galley_settings_pkey` (`galley_id`,`locale`,`setting_name`),
  KEY `publication_galley_settings_galley_id` (`galley_id`),
  KEY `publication_galley_settings_name_value` (`setting_name`(50),`setting_value`(150))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `publication_settings`
--

DROP TABLE IF EXISTS `publication_settings`;
CREATE TABLE IF NOT EXISTS `publication_settings` (
  `publication_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  UNIQUE KEY `publication_settings_pkey` (`publication_id`,`locale`,`setting_name`),
  KEY `publication_settings_publication_id` (`publication_id`),
  KEY `publication_settings_name_value` (`setting_name`(50),`setting_value`(150))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `queries`
--

DROP TABLE IF EXISTS `queries`;
CREATE TABLE IF NOT EXISTS `queries` (
  `query_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assoc_type` bigint(20) NOT NULL,
  `assoc_id` bigint(20) NOT NULL,
  `stage_id` tinyint(4) NOT NULL DEFAULT '1',
  `seq` double NOT NULL DEFAULT '0',
  `date_posted` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `closed` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`query_id`),
  KEY `queries_assoc_id` (`assoc_type`,`assoc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `query_participants`
--

DROP TABLE IF EXISTS `query_participants`;
CREATE TABLE IF NOT EXISTS `query_participants` (
  `query_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  UNIQUE KEY `query_participants_pkey` (`query_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `queued_payments`
--

DROP TABLE IF EXISTS `queued_payments`;
CREATE TABLE IF NOT EXISTS `queued_payments` (
  `queued_payment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `expiry_date` date DEFAULT NULL,
  `payment_data` text,
  PRIMARY KEY (`queued_payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_assignments`
--

DROP TABLE IF EXISTS `review_assignments`;
CREATE TABLE IF NOT EXISTS `review_assignments` (
  `review_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `submission_id` bigint(20) NOT NULL,
  `reviewer_id` bigint(20) NOT NULL,
  `competing_interests` text,
  `recommendation` tinyint(4) DEFAULT NULL,
  `date_assigned` datetime DEFAULT NULL,
  `date_notified` datetime DEFAULT NULL,
  `date_confirmed` datetime DEFAULT NULL,
  `date_completed` datetime DEFAULT NULL,
  `date_acknowledged` datetime DEFAULT NULL,
  `date_due` datetime DEFAULT NULL,
  `date_response_due` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `reminder_was_automatic` tinyint(4) NOT NULL DEFAULT '0',
  `declined` tinyint(4) NOT NULL DEFAULT '0',
  `cancelled` tinyint(4) NOT NULL DEFAULT '0',
  `reviewer_file_id` bigint(20) DEFAULT NULL,
  `date_rated` datetime DEFAULT NULL,
  `date_reminded` datetime DEFAULT NULL,
  `quality` tinyint(4) DEFAULT NULL,
  `review_round_id` bigint(20) DEFAULT NULL,
  `stage_id` tinyint(4) NOT NULL DEFAULT '1',
  `review_method` tinyint(4) NOT NULL DEFAULT '1',
  `round` tinyint(4) NOT NULL DEFAULT '1',
  `step` tinyint(4) NOT NULL DEFAULT '1',
  `review_form_id` bigint(20) DEFAULT NULL,
  `unconsidered` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`review_id`),
  KEY `review_assignments_submission_id` (`submission_id`),
  KEY `review_assignments_reviewer_id` (`reviewer_id`),
  KEY `review_assignments_form_id` (`review_form_id`),
  KEY `review_assignments_reviewer_review` (`reviewer_id`,`review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_files`
--

DROP TABLE IF EXISTS `review_files`;
CREATE TABLE IF NOT EXISTS `review_files` (
  `review_id` bigint(20) NOT NULL,
  `file_id` bigint(20) NOT NULL,
  UNIQUE KEY `review_files_pkey` (`review_id`,`file_id`),
  KEY `review_files_review_id` (`review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_forms`
--

DROP TABLE IF EXISTS `review_forms`;
CREATE TABLE IF NOT EXISTS `review_forms` (
  `review_form_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assoc_type` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  `seq` double DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`review_form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_form_elements`
--

DROP TABLE IF EXISTS `review_form_elements`;
CREATE TABLE IF NOT EXISTS `review_form_elements` (
  `review_form_element_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `review_form_id` bigint(20) NOT NULL,
  `seq` double DEFAULT NULL,
  `element_type` bigint(20) DEFAULT NULL,
  `required` tinyint(4) DEFAULT NULL,
  `included` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`review_form_element_id`),
  KEY `review_form_elements_review_form_id` (`review_form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_form_element_settings`
--

DROP TABLE IF EXISTS `review_form_element_settings`;
CREATE TABLE IF NOT EXISTS `review_form_element_settings` (
  `review_form_element_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `review_form_element_settings_pkey` (`review_form_element_id`,`locale`,`setting_name`),
  KEY `review_form_element_settings_review_form_element_id` (`review_form_element_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_form_responses`
--

DROP TABLE IF EXISTS `review_form_responses`;
CREATE TABLE IF NOT EXISTS `review_form_responses` (
  `review_form_element_id` bigint(20) NOT NULL,
  `review_id` bigint(20) NOT NULL,
  `response_type` varchar(6) DEFAULT NULL,
  `response_value` text,
  KEY `review_form_responses_pkey` (`review_form_element_id`,`review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_form_settings`
--

DROP TABLE IF EXISTS `review_form_settings`;
CREATE TABLE IF NOT EXISTS `review_form_settings` (
  `review_form_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `review_form_settings_pkey` (`review_form_id`,`locale`,`setting_name`),
  KEY `review_form_settings_review_form_id` (`review_form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_rounds`
--

DROP TABLE IF EXISTS `review_rounds`;
CREATE TABLE IF NOT EXISTS `review_rounds` (
  `review_round_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `submission_id` bigint(20) NOT NULL,
  `stage_id` bigint(20) DEFAULT NULL,
  `round` tinyint(4) NOT NULL,
  `review_revision` bigint(20) DEFAULT NULL,
  `status` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`review_round_id`),
  UNIQUE KEY `review_rounds_submission_id_stage_id_round_pkey` (`submission_id`,`stage_id`,`round`),
  KEY `review_rounds_submission_id` (`submission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_round_files`
--

DROP TABLE IF EXISTS `review_round_files`;
CREATE TABLE IF NOT EXISTS `review_round_files` (
  `submission_id` bigint(20) NOT NULL,
  `review_round_id` bigint(20) NOT NULL,
  `stage_id` tinyint(4) NOT NULL,
  `file_id` bigint(20) NOT NULL,
  `revision` bigint(20) NOT NULL DEFAULT '1',
  UNIQUE KEY `review_round_files_pkey` (`submission_id`,`review_round_id`,`file_id`,`revision`),
  KEY `review_round_files_submission_id` (`submission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `scheduled_tasks`
--

DROP TABLE IF EXISTS `scheduled_tasks`;
CREATE TABLE IF NOT EXISTS `scheduled_tasks` (
  `class_name` varchar(255) NOT NULL,
  `last_run` datetime DEFAULT NULL,
  UNIQUE KEY `scheduled_tasks_pkey` (`class_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `scheduled_tasks`
--

INSERT INTO `scheduled_tasks` (`class_name`, `last_run`) VALUES
('classes.tasks.SubscriptionExpiryReminder', '2021-01-18 15:33:24'),
('lib.pkp.classes.task.ReviewReminder', '2021-01-19 20:49:44'),
('lib.pkp.classes.task.StatisticsReport', '2021-01-18 15:33:13'),
('plugins.generic.usageStats.UsageStatsLoader', '2021-01-19 20:49:37'),
('plugins.importexport.crossref.CrossrefInfoSender', '2021-01-19 20:49:40'),
('plugins.importexport.datacite.DataciteInfoSender', '2021-01-19 20:49:41'),
('plugins.importexport.doaj.DOAJInfoSender', '2021-01-19 20:49:42'),
('plugins.importexport.medra.MedraInfoSender', '2021-01-19 20:49:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sections`
--

DROP TABLE IF EXISTS `sections`;
CREATE TABLE IF NOT EXISTS `sections` (
  `section_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `journal_id` bigint(20) NOT NULL,
  `review_form_id` bigint(20) DEFAULT NULL,
  `seq` double NOT NULL DEFAULT '0',
  `editor_restricted` tinyint(4) NOT NULL DEFAULT '0',
  `meta_indexed` tinyint(4) NOT NULL DEFAULT '0',
  `meta_reviewed` tinyint(4) NOT NULL DEFAULT '1',
  `abstracts_not_required` tinyint(4) NOT NULL DEFAULT '0',
  `hide_title` tinyint(4) NOT NULL DEFAULT '0',
  `hide_author` tinyint(4) NOT NULL DEFAULT '0',
  `abstract_word_count` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`section_id`),
  KEY `sections_journal_id` (`journal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `section_settings`
--

DROP TABLE IF EXISTS `section_settings`;
CREATE TABLE IF NOT EXISTS `section_settings` (
  `section_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `section_settings_pkey` (`section_id`,`locale`,`setting_name`),
  KEY `section_settings_section_id` (`section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `section_settings`
--

INSERT INTO `section_settings` (`section_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, 'en_US', 'abbrev', 'ART', 'string'),
(1, 'en_US', 'identifyType', '', 'string'),
(1, 'en_US', 'policy', '', 'string'),
(1, 'en_US', 'title', 'Archive', 'string'),
(10, 'en_US', 'abbrev', 'ART', 'string'),
(10, 'en_US', 'policy', 'Section default policy', 'string'),
(10, 'en_US', 'title', 'Articles', 'string'),
(11, 'en_US', 'abbrev', 'ART', 'string'),
(11, 'en_US', 'policy', 'Section default policy', 'string'),
(11, 'en_US', 'title', 'Articles', 'string');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(128) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `ip_address` varchar(39) NOT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `created` bigint(20) NOT NULL DEFAULT '0',
  `last_used` bigint(20) NOT NULL DEFAULT '0',
  `remember` tinyint(4) NOT NULL DEFAULT '0',
  `data` text,
  `domain` varchar(255) DEFAULT NULL,
  UNIQUE KEY `sessions_pkey` (`session_id`),
  KEY `sessions_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sessions`
--

INSERT INTO `sessions` (`session_id`, `user_id`, `ip_address`, `user_agent`, `created`, `last_used`, `remember`, `data`, `domain`) VALUES
('0gtsp4tmldek3rinm2lm6kpvhm', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 1611064168, 1611064178, 0, '', 'localhost'),
('g8n7frab2oms3q7l2pp01tfi9h', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 1610990796, 1610990799, 0, '', 'localhost'),
('hptkv54cj51u3cqbu2hg5u5vfl', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 1610983943, 1610995106, 0, '', 'localhost'),
('jvd6ep1mqptk2lloim60ft2tla', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 1611022445, 1611022502, 0, '', 'localhost'),
('sss86ah3k60tj4to8s87ed4unj', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0', 1610984037, 1611078836, 0, 'csrf|a:2:{s:9:\"timestamp\";i:1611075758;s:5:\"token\";s:32:\"1f29e454793c565b93ff9a5247c0479a\";}username|s:15:\"meitro_hartanto\";currentLocale|s:5:\"en_US\";', 'localhost'),
('u4tudphbnad6kr7cfkep3rlvtc', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 1611064178, 1611078949, 1, 'csrf|a:2:{s:9:\"timestamp\";i:1611078954;s:5:\"token\";s:32:\"34a93b5d02c9428dd1a40a7f175a65cf\";}userId|s:1:\"1\";username|s:15:\"meitro_hartanto\";', 'localhost');

-- --------------------------------------------------------

--
-- Struktur dari tabel `site`
--

DROP TABLE IF EXISTS `site`;
CREATE TABLE IF NOT EXISTS `site` (
  `redirect` bigint(20) NOT NULL DEFAULT '0',
  `primary_locale` varchar(14) NOT NULL,
  `min_password_length` tinyint(4) NOT NULL DEFAULT '6',
  `installed_locales` varchar(1024) NOT NULL DEFAULT 'en_US',
  `supported_locales` varchar(1024) DEFAULT NULL,
  `original_style_file_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `site`
--

INSERT INTO `site` (`redirect`, `primary_locale`, `min_password_length`, `installed_locales`, `supported_locales`, `original_style_file_name`) VALUES
(0, 'en_US', 6, 'a:2:{i:0;s:5:\"id_ID\";i:1;s:5:\"en_US\";}', 'a:2:{i:0;s:5:\"id_ID\";i:1;s:5:\"en_US\";}', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `site_settings`
--

DROP TABLE IF EXISTS `site_settings`;
CREATE TABLE IF NOT EXISTS `site_settings` (
  `setting_name` varchar(255) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_value` text,
  UNIQUE KEY `site_settings_pkey` (`setting_name`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `site_settings`
--

INSERT INTO `site_settings` (`setting_name`, `locale`, `setting_value`) VALUES
('contactEmail', 'en_US', 'meyhantra@gmail.com'),
('contactName', 'en_US', 'Meitro Hartanto'),
('contactName', 'id_ID', 'Open Journal Systems'),
('pageHeaderTitleImage', 'en_US', 'a:5:{s:10:\"uploadName\";s:30:\"pageHeaderTitleImage_en_US.jpg\";s:5:\"width\";i:1138;s:6:\"height\";i:164;s:12:\"dateUploaded\";s:19:\"2021-01-16 14:42:03\";s:7:\"altText\";s:0:\"\";}'),
('sidebar', '', 'a:5:{i:0;s:22:\"informationblockplugin\";i:1;s:25:\"languagetoggleblockplugin\";i:2;s:25:\"makesubmissionblockplugin\";i:3;s:23:\"subscriptionblockplugin\";i:4;s:8:\"Visitors\";}'),
('themePluginPath', '', 'bootstrap3'),
('title', 'en_US', 'Lembaga Intelektual Muda (LIM) Maluku');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stage_assignments`
--

DROP TABLE IF EXISTS `stage_assignments`;
CREATE TABLE IF NOT EXISTS `stage_assignments` (
  `stage_assignment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `submission_id` bigint(20) NOT NULL,
  `user_group_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `date_assigned` datetime NOT NULL,
  `recommend_only` tinyint(4) NOT NULL DEFAULT '0',
  `can_change_metadata` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`stage_assignment_id`),
  UNIQUE KEY `stage_assignment` (`submission_id`,`user_group_id`,`user_id`),
  KEY `stage_assignments_submission_id` (`submission_id`),
  KEY `stage_assignments_user_group_id` (`user_group_id`),
  KEY `stage_assignments_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `stage_assignments`
--

INSERT INTO `stage_assignments` (`stage_assignment_id`, `submission_id`, `user_group_id`, `user_id`, `date_assigned`, `recommend_only`, `can_change_metadata`) VALUES
(1, 1, 167, 6, '2021-01-17 14:54:24', 0, 0),
(2, 1, 158, 7, '2021-01-17 15:18:56', 1, 1),
(3, 1, 156, 8, '2021-01-17 15:19:46', 1, 1),
(4, 1, 164, 9, '2021-01-17 15:52:35', 0, 1),
(5, 1, 157, 8, '2021-01-17 17:14:00', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `static_pages`
--

DROP TABLE IF EXISTS `static_pages`;
CREATE TABLE IF NOT EXISTS `static_pages` (
  `static_page_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  PRIMARY KEY (`static_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `static_pages`
--

INSERT INTO `static_pages` (`static_page_id`, `path`, `context_id`) VALUES
(2, 'home', 5),
(4, 'about', 1),
(5, 'contact', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `static_page_settings`
--

DROP TABLE IF EXISTS `static_page_settings`;
CREATE TABLE IF NOT EXISTS `static_page_settings` (
  `static_page_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` longtext,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `static_page_settings_pkey` (`static_page_id`,`locale`,`setting_name`),
  KEY `static_page_settings_static_page_id` (`static_page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `static_page_settings`
--

INSERT INTO `static_page_settings` (`static_page_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(2, 'en_US', 'content', '<p>Selamat Datang</p>', 'string'),
(2, 'en_US', 'title', 'Selamat Datang', 'string'),
(4, 'en_US', 'content', '<div id=\"aboutPeople\">\r\n<h3>People</h3>\r\n<ul>\r\n<li><a href=\"/ojs/index.php/ifi/about/contact\">Contact</a></li>\r\n<li id=\"editorialTeamLink\"><a href=\"/ojs/index.php/ifi/about/editorialTeam\">Editorial Team</a></li>\r\n</ul>\r\n</div>\r\n<div id=\"aboutPolicies\">\r\n<h3>Policies</h3>\r\n<ul>\r\n<li><a href=\"localhost/index.php/ifi/about/editorialPolicies#focusAndScope\">Focus and Scope</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/editorialPolicies#sectionPolicies\">Section Policies</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/editorialPolicies#peerReviewProcess\">Peer Review Process</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/editorialPolicies#publicationFrequency\">Publication Frequency</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/editorialPolicies#openAccessPolicy\">Open Access Policy</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/editorialPolicies#archiving\">Archiving</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/editorialPolicies#custom-0\">Publication Ethics and Publication Malpractice Statement</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/editorialPolicies#custom-1\">Abstracting and Indexing</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/editorialPolicies#custom-2\">Retraction</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/editorialPolicies#custom-3\">Withdrawal of Manuscripts</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/editorialPolicies#custom-4\">Sources of Support</a></li>\r\n</ul>\r\n</div>\r\n<div id=\"aboutSubmissions\">\r\n<h3>Submissions</h3>\r\n<ul>\r\n<li><a href=\"localhost/index.php/ifi/about/submissions#onlineSubmissions\">Online Submissions</a></li>\r\n<li><a href=\"localhost/index.php/ifit/submissions#authorGuidelines\">Author Guidelines</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/submissions#copyrightNotice\">Copyright Notice</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/submissions#privacyStatement\">Privacy Statement</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/submissions#authorFees\">Author Fees</a></li>\r\n</ul>\r\n</div>\r\n<div id=\"aboutOther\">\r\n<h3>Other</h3>\r\n<ul>\r\n<li><a href=\"localhost/index.php/ifi/about/journalSponsorship\">Journal Sponsorship</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/siteMap\">Site Map</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/aboutThisPublishingSystem\">About this Publishing System</a></li>\r\n<li><a href=\"localhost/index.php/ifi/about/statistics\">Statistics</a></li>\r\n</ul>\r\n</div>', 'string'),
(4, 'en_US', 'title', 'About', 'string'),
(5, 'en_US', 'content', '<p>Contact</p>', 'string'),
(5, 'en_US', 'title', 'Contact', 'string');

-- --------------------------------------------------------

--
-- Struktur dari tabel `subeditor_submission_group`
--

DROP TABLE IF EXISTS `subeditor_submission_group`;
CREATE TABLE IF NOT EXISTS `subeditor_submission_group` (
  `context_id` bigint(20) NOT NULL,
  `assoc_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `assoc_type` bigint(20) NOT NULL,
  UNIQUE KEY `section_editors_pkey` (`context_id`,`assoc_id`,`user_id`,`assoc_type`),
  KEY `subeditor_submission_group_context_id` (`context_id`),
  KEY `subeditor_submission_group_assoc_id` (`assoc_type`,`assoc_id`),
  KEY `subeditor_submission_group_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `submissions`
--

DROP TABLE IF EXISTS `submissions`;
CREATE TABLE IF NOT EXISTS `submissions` (
  `submission_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `locale` varchar(14) DEFAULT NULL,
  `context_id` bigint(20) NOT NULL,
  `section_id` bigint(20) DEFAULT NULL,
  `current_publication_id` bigint(20) DEFAULT NULL,
  `date_last_activity` datetime DEFAULT NULL,
  `date_submitted` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `stage_id` bigint(20) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `submission_progress` tinyint(4) NOT NULL DEFAULT '1',
  `work_type` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`submission_id`),
  KEY `submissions_context_id` (`context_id`),
  KEY `submissions_publication_id` (`current_publication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `submission_artwork_files`
--

DROP TABLE IF EXISTS `submission_artwork_files`;
CREATE TABLE IF NOT EXISTS `submission_artwork_files` (
  `file_id` bigint(20) NOT NULL,
  `revision` bigint(20) NOT NULL,
  `caption` text,
  `credit` varchar(255) DEFAULT NULL,
  `copyright_owner` varchar(255) DEFAULT NULL,
  `copyright_owner_contact` text,
  `permission_terms` text,
  `permission_file_id` bigint(20) DEFAULT NULL,
  `chapter_id` bigint(20) DEFAULT NULL,
  `contact_author` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`file_id`,`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `submission_comments`
--

DROP TABLE IF EXISTS `submission_comments`;
CREATE TABLE IF NOT EXISTS `submission_comments` (
  `comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment_type` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `submission_id` bigint(20) NOT NULL,
  `assoc_id` bigint(20) NOT NULL,
  `author_id` bigint(20) NOT NULL,
  `comment_title` text,
  `comments` text,
  `date_posted` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `viewable` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `submission_comments_submission_id` (`submission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `submission_files`
--

DROP TABLE IF EXISTS `submission_files`;
CREATE TABLE IF NOT EXISTS `submission_files` (
  `file_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `revision` bigint(20) NOT NULL,
  `source_file_id` bigint(20) DEFAULT NULL,
  `source_revision` bigint(20) DEFAULT NULL,
  `submission_id` bigint(20) NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `genre_id` bigint(20) DEFAULT NULL,
  `file_size` bigint(20) NOT NULL,
  `original_file_name` varchar(127) DEFAULT NULL,
  `file_stage` bigint(20) NOT NULL,
  `direct_sales_price` varchar(255) DEFAULT NULL,
  `sales_type` varchar(255) DEFAULT NULL,
  `viewable` tinyint(4) DEFAULT NULL,
  `date_uploaded` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `uploader_user_id` bigint(20) DEFAULT NULL,
  `assoc_type` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`file_id`,`revision`),
  KEY `submission_files_submission_id` (`submission_id`),
  KEY `submission_files_stage_assoc` (`file_stage`,`assoc_type`,`assoc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `submission_file_settings`
--

DROP TABLE IF EXISTS `submission_file_settings`;
CREATE TABLE IF NOT EXISTS `submission_file_settings` (
  `file_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `submission_file_settings_pkey` (`file_id`,`locale`,`setting_name`),
  KEY `submission_file_settings_id` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `submission_search_keyword_list`
--

DROP TABLE IF EXISTS `submission_search_keyword_list`;
CREATE TABLE IF NOT EXISTS `submission_search_keyword_list` (
  `keyword_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `keyword_text` varchar(60) NOT NULL,
  PRIMARY KEY (`keyword_id`),
  UNIQUE KEY `submission_search_keyword_text` (`keyword_text`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `submission_search_objects`
--

DROP TABLE IF EXISTS `submission_search_objects`;
CREATE TABLE IF NOT EXISTS `submission_search_objects` (
  `object_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `submission_id` bigint(20) NOT NULL,
  `type` int(11) NOT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`object_id`),
  KEY `submission_search_object_submission` (`submission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `submission_search_object_keywords`
--

DROP TABLE IF EXISTS `submission_search_object_keywords`;
CREATE TABLE IF NOT EXISTS `submission_search_object_keywords` (
  `object_id` bigint(20) NOT NULL,
  `keyword_id` bigint(20) NOT NULL,
  `pos` int(11) NOT NULL,
  UNIQUE KEY `submission_search_object_keywords_pkey` (`object_id`,`pos`),
  KEY `submission_search_object_keywords_keyword_id` (`keyword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `submission_settings`
--

DROP TABLE IF EXISTS `submission_settings`;
CREATE TABLE IF NOT EXISTS `submission_settings` (
  `submission_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  UNIQUE KEY `submission_settings_pkey` (`submission_id`,`locale`,`setting_name`),
  KEY `submission_settings_submission_id` (`submission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `submission_supplementary_files`
--

DROP TABLE IF EXISTS `submission_supplementary_files`;
CREATE TABLE IF NOT EXISTS `submission_supplementary_files` (
  `file_id` bigint(20) NOT NULL,
  `revision` bigint(20) NOT NULL,
  PRIMARY KEY (`file_id`,`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `submission_tombstones`
--

DROP TABLE IF EXISTS `submission_tombstones`;
CREATE TABLE IF NOT EXISTS `submission_tombstones` (
  `tombstone_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `submission_id` bigint(20) NOT NULL,
  `date_deleted` datetime NOT NULL,
  `journal_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `set_spec` varchar(255) NOT NULL,
  `set_name` varchar(255) NOT NULL,
  `oai_identifier` varchar(255) NOT NULL,
  PRIMARY KEY (`tombstone_id`),
  KEY `submission_tombstones_journal_id` (`journal_id`),
  KEY `submission_tombstones_submission_id` (`submission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `subscription_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `journal_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `type_id` bigint(20) NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `membership` varchar(40) DEFAULT NULL,
  `reference_number` varchar(40) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `subscription_types`
--

DROP TABLE IF EXISTS `subscription_types`;
CREATE TABLE IF NOT EXISTS `subscription_types` (
  `type_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `journal_id` bigint(20) NOT NULL,
  `cost` double NOT NULL,
  `currency_code_alpha` varchar(3) NOT NULL,
  `non_expiring` tinyint(4) NOT NULL DEFAULT '0',
  `duration` smallint(6) DEFAULT NULL,
  `format` smallint(6) NOT NULL,
  `institutional` tinyint(4) NOT NULL DEFAULT '0',
  `membership` tinyint(4) NOT NULL DEFAULT '0',
  `disable_public_display` tinyint(4) NOT NULL,
  `seq` double NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `subscription_type_settings`
--

DROP TABLE IF EXISTS `subscription_type_settings`;
CREATE TABLE IF NOT EXISTS `subscription_type_settings` (
  `type_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `subscription_type_settings_pkey` (`type_id`,`locale`,`setting_name`),
  KEY `subscription_type_settings_type_id` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `temporary_files`
--

DROP TABLE IF EXISTS `temporary_files`;
CREATE TABLE IF NOT EXISTS `temporary_files` (
  `file_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `file_name` varchar(90) NOT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  `file_size` bigint(20) NOT NULL,
  `original_file_name` varchar(127) DEFAULT NULL,
  `date_uploaded` datetime NOT NULL,
  PRIMARY KEY (`file_id`),
  KEY `temporary_files_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `usage_stats_temporary_records`
--

DROP TABLE IF EXISTS `usage_stats_temporary_records`;
CREATE TABLE IF NOT EXISTS `usage_stats_temporary_records` (
  `assoc_id` bigint(20) NOT NULL,
  `assoc_type` bigint(20) NOT NULL,
  `day` bigint(20) NOT NULL,
  `entry_time` bigint(20) NOT NULL,
  `metric` bigint(20) NOT NULL DEFAULT '1',
  `country_id` varchar(2) DEFAULT NULL,
  `region` varchar(2) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `load_id` varchar(255) NOT NULL,
  `file_type` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `url` varchar(2047) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `mailing_address` varchar(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `country` varchar(90) DEFAULT NULL,
  `locales` varchar(255) DEFAULT NULL,
  `gossip` text,
  `date_last_email` datetime DEFAULT NULL,
  `date_registered` datetime NOT NULL,
  `date_validated` datetime DEFAULT NULL,
  `date_last_login` datetime NOT NULL,
  `must_change_password` tinyint(4) DEFAULT NULL,
  `auth_id` bigint(20) DEFAULT NULL,
  `auth_str` varchar(255) DEFAULT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `disabled_reason` text,
  `inline_help` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_username` (`username`),
  UNIQUE KEY `users_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `email`, `url`, `phone`, `mailing_address`, `billing_address`, `country`, `locales`, `gossip`, `date_last_email`, `date_registered`, `date_validated`, `date_last_login`, `must_change_password`, `auth_id`, `auth_str`, `disabled`, `disabled_reason`, `inline_help`) VALUES
(1, 'meitro_hartanto', '$2y$10$IfiZeY4r3P8OnWab1qiwg.QONWbnpuACGA1GASL0CFEvzcosJs6Fq', 'meyhantra@gmail.com', '', '', '', NULL, 'ID', 'en_US', NULL, NULL, '2020-05-29 08:22:42', NULL, '2021-01-19 20:50:13', 0, NULL, NULL, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE IF NOT EXISTS `user_groups` (
  `user_group_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `context_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `is_default` tinyint(4) NOT NULL DEFAULT '0',
  `show_title` tinyint(4) NOT NULL DEFAULT '1',
  `permit_self_registration` tinyint(4) NOT NULL DEFAULT '0',
  `permit_metadata_edit` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_group_id`),
  KEY `user_groups_user_group_id` (`user_group_id`),
  KEY `user_groups_context_id` (`context_id`),
  KEY `user_groups_role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_groups`
--

INSERT INTO `user_groups` (`user_group_id`, `context_id`, `role_id`, `is_default`, `show_title`, `permit_self_registration`, `permit_metadata_edit`) VALUES
(1, 0, 1, 1, 0, 0, 0),
(2, 1, 16, 1, 0, 0, 1),
(3, 1, 16, 1, 0, 0, 1),
(4, 1, 16, 1, 0, 0, 1),
(5, 1, 17, 1, 0, 0, 1),
(6, 1, 17, 1, 0, 0, 0),
(7, 1, 4097, 1, 0, 0, 0),
(8, 1, 4097, 1, 0, 0, 0),
(9, 1, 4097, 1, 0, 0, 0),
(10, 1, 4097, 1, 0, 0, 0),
(11, 1, 4097, 1, 0, 0, 0),
(12, 1, 4097, 1, 0, 0, 0),
(13, 1, 4097, 1, 0, 0, 0),
(14, 1, 65536, 1, 0, 1, 0),
(15, 1, 65536, 1, 0, 0, 0),
(16, 1, 4096, 1, 0, 1, 0),
(17, 1, 1048576, 1, 0, 1, 0),
(18, 1, 2097152, 1, 0, 0, 0),
(19, 2, 16, 1, 0, 0, 1),
(20, 2, 16, 1, 0, 0, 1),
(21, 2, 16, 1, 0, 0, 1),
(22, 2, 17, 1, 0, 0, 1),
(23, 2, 17, 1, 0, 0, 0),
(24, 2, 4097, 1, 0, 0, 0),
(25, 2, 4097, 1, 0, 0, 0),
(26, 2, 4097, 1, 0, 0, 0),
(27, 2, 4097, 1, 0, 0, 0),
(28, 2, 4097, 1, 0, 0, 0),
(29, 2, 4097, 1, 0, 0, 0),
(30, 2, 4097, 1, 0, 0, 0),
(31, 2, 65536, 1, 0, 1, 0),
(32, 2, 65536, 1, 0, 0, 0),
(33, 2, 4096, 1, 0, 1, 0),
(34, 2, 1048576, 1, 0, 1, 0),
(35, 2, 2097152, 1, 0, 0, 0),
(36, 3, 16, 1, 0, 0, 1),
(37, 3, 16, 1, 0, 0, 1),
(38, 3, 16, 1, 0, 0, 1),
(39, 3, 17, 1, 0, 0, 1),
(40, 3, 17, 1, 0, 0, 0),
(41, 3, 4097, 1, 0, 0, 0),
(42, 3, 4097, 1, 0, 0, 0),
(43, 3, 4097, 1, 0, 0, 0),
(44, 3, 4097, 1, 0, 0, 0),
(45, 3, 4097, 1, 0, 0, 0),
(46, 3, 4097, 1, 0, 0, 0),
(47, 3, 4097, 1, 0, 0, 0),
(48, 3, 65536, 1, 0, 1, 0),
(49, 3, 65536, 1, 0, 0, 0),
(50, 3, 4096, 1, 0, 1, 0),
(51, 3, 1048576, 1, 0, 1, 0),
(52, 3, 2097152, 1, 0, 0, 0),
(53, 4, 16, 1, 0, 0, 1),
(54, 4, 16, 1, 0, 0, 1),
(55, 4, 16, 1, 0, 0, 1),
(56, 4, 17, 1, 0, 0, 1),
(57, 4, 17, 1, 0, 0, 0),
(58, 4, 4097, 1, 0, 0, 0),
(59, 4, 4097, 1, 0, 0, 0),
(60, 4, 4097, 1, 0, 0, 0),
(61, 4, 4097, 1, 0, 0, 0),
(62, 4, 4097, 1, 0, 0, 0),
(63, 4, 4097, 1, 0, 0, 0),
(64, 4, 4097, 1, 0, 0, 0),
(65, 4, 65536, 1, 0, 1, 0),
(66, 4, 65536, 1, 0, 0, 0),
(67, 4, 4096, 1, 0, 1, 0),
(68, 4, 1048576, 1, 0, 1, 0),
(69, 4, 2097152, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_group_settings`
--

DROP TABLE IF EXISTS `user_group_settings`;
CREATE TABLE IF NOT EXISTS `user_group_settings` (
  `user_group_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `user_group_settings_pkey` (`user_group_id`,`locale`,`setting_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_group_settings`
--

INSERT INTO `user_group_settings` (`user_group_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, 'en_US', 'name', 'Site Admin', 'string'),
(1, 'id_ID', 'name', '##default.groups.name.siteAdmin##', 'string'),
(155, '', 'abbrevLocaleKey', 'default.groups.abbrev.manager', 'string'),
(155, '', 'nameLocaleKey', 'default.groups.name.manager', 'string'),
(155, 'en_US', 'abbrev', 'JM', 'string'),
(155, 'en_US', 'name', 'Journal manager', 'string'),
(155, 'id_ID', 'abbrev', 'JM', 'string'),
(155, 'id_ID', 'name', 'Manajer Jurnal', 'string'),
(156, '', 'abbrevLocaleKey', 'default.groups.abbrev.editor', 'string'),
(156, '', 'nameLocaleKey', 'default.groups.name.editor', 'string'),
(156, '', 'recommendOnly', '0', 'bool'),
(156, 'en_US', 'abbrev', 'JE', 'string'),
(156, 'en_US', 'name', 'Journal editor', 'string'),
(156, 'id_ID', 'abbrev', 'JE', 'string'),
(156, 'id_ID', 'name', 'Editor Jurnal', 'string'),
(157, '', 'abbrevLocaleKey', 'default.groups.abbrev.productionEditor', 'string'),
(157, '', 'nameLocaleKey', 'default.groups.name.productionEditor', 'string'),
(157, 'en_US', 'abbrev', 'ProdE', 'string'),
(157, 'en_US', 'name', 'Production editor', 'string'),
(157, 'id_ID', 'abbrev', 'ProdE', 'string'),
(157, 'id_ID', 'name', 'Editor produksi', 'string'),
(158, '', 'abbrevLocaleKey', 'default.groups.abbrev.sectionEditor', 'string'),
(158, '', 'nameLocaleKey', 'default.groups.name.sectionEditor', 'string'),
(158, 'en_US', 'abbrev', 'SecE', 'string'),
(158, 'en_US', 'name', 'Section editor', 'string'),
(158, 'id_ID', 'abbrev', 'EdBag', 'string'),
(158, 'id_ID', 'name', 'Editor Bagian', 'string'),
(159, '', 'abbrevLocaleKey', 'default.groups.abbrev.guestEditor', 'string'),
(159, '', 'nameLocaleKey', 'default.groups.name.guestEditor', 'string'),
(159, 'en_US', 'abbrev', 'GE', 'string'),
(159, 'en_US', 'name', 'Guest editor', 'string'),
(159, 'id_ID', 'abbrev', 'GE', 'string'),
(159, 'id_ID', 'name', 'Editor Tamu', 'string'),
(160, '', 'abbrevLocaleKey', 'default.groups.abbrev.copyeditor', 'string'),
(160, '', 'nameLocaleKey', 'default.groups.name.copyeditor', 'string'),
(160, 'en_US', 'abbrev', 'CE', 'string'),
(160, 'en_US', 'name', 'Copyeditor', 'string'),
(160, 'id_ID', 'abbrev', 'CE', 'string'),
(160, 'id_ID', 'name', 'Copyeditor', 'string'),
(161, '', 'abbrevLocaleKey', 'default.groups.abbrev.designer', 'string'),
(161, '', 'nameLocaleKey', 'default.groups.name.designer', 'string'),
(161, 'en_US', 'abbrev', 'Design', 'string'),
(161, 'en_US', 'name', 'Designer', 'string'),
(161, 'id_ID', 'abbrev', 'Desain', 'string'),
(161, 'id_ID', 'name', 'Desainer', 'string'),
(162, '', 'abbrevLocaleKey', 'default.groups.abbrev.funding', 'string'),
(162, '', 'nameLocaleKey', 'default.groups.name.funding', 'string'),
(162, 'en_US', 'abbrev', 'FC', 'string'),
(162, 'en_US', 'name', 'Funding coordinator', 'string'),
(162, 'id_ID', 'abbrev', 'FC', 'string'),
(162, 'id_ID', 'name', 'Koordinator pendanaan', 'string'),
(163, '', 'abbrevLocaleKey', 'default.groups.abbrev.indexer', 'string'),
(163, '', 'nameLocaleKey', 'default.groups.name.indexer', 'string'),
(163, 'en_US', 'abbrev', 'IND', 'string'),
(163, 'en_US', 'name', 'Indexer', 'string'),
(163, 'id_ID', 'abbrev', 'IND', 'string'),
(163, 'id_ID', 'name', 'Pengindeks', 'string'),
(164, '', 'abbrevLocaleKey', 'default.groups.abbrev.layoutEditor', 'string'),
(164, '', 'nameLocaleKey', 'default.groups.name.layoutEditor', 'string'),
(164, 'en_US', 'abbrev', 'LE', 'string'),
(164, 'en_US', 'name', 'Layout Editor', 'string'),
(164, 'id_ID', 'abbrev', 'LE', 'string'),
(164, 'id_ID', 'name', 'Editor Tata Letak', 'string'),
(165, '', 'abbrevLocaleKey', 'default.groups.abbrev.marketing', 'string'),
(165, '', 'nameLocaleKey', 'default.groups.name.marketing', 'string'),
(165, 'en_US', 'abbrev', 'MS', 'string'),
(165, 'en_US', 'name', 'Marketing and sales coordinator', 'string'),
(165, 'id_ID', 'abbrev', 'MS', 'string'),
(165, 'id_ID', 'name', 'Koordinator penjualan dan pemasaran', 'string'),
(166, '', 'abbrevLocaleKey', 'default.groups.abbrev.proofreader', 'string'),
(166, '', 'nameLocaleKey', 'default.groups.name.proofreader', 'string'),
(166, 'en_US', 'abbrev', 'PR', 'string'),
(166, 'en_US', 'name', 'Proofreader', 'string'),
(166, 'id_ID', 'abbrev', 'PR', 'string'),
(166, 'id_ID', 'name', 'Proofreader', 'string'),
(167, '', 'abbrevLocaleKey', 'default.groups.abbrev.author', 'string'),
(167, '', 'nameLocaleKey', 'default.groups.name.author', 'string'),
(167, 'en_US', 'abbrev', 'AU', 'string'),
(167, 'en_US', 'name', 'Author', 'string'),
(167, 'id_ID', 'abbrev', 'AU', 'string'),
(167, 'id_ID', 'name', 'Penulis', 'string'),
(168, '', 'abbrevLocaleKey', 'default.groups.abbrev.translator', 'string'),
(168, '', 'nameLocaleKey', 'default.groups.name.translator', 'string'),
(168, 'en_US', 'abbrev', 'Trans', 'string'),
(168, 'en_US', 'name', 'Translator', 'string'),
(168, 'id_ID', 'abbrev', 'Trans', 'string'),
(168, 'id_ID', 'name', 'Penerjemah', 'string'),
(169, '', 'abbrevLocaleKey', 'default.groups.abbrev.externalReviewer', 'string'),
(169, '', 'nameLocaleKey', 'default.groups.name.externalReviewer', 'string'),
(169, 'en_US', 'abbrev', 'R', 'string'),
(169, 'en_US', 'name', 'Reviewer', 'string'),
(169, 'id_ID', 'abbrev', 'R', 'string'),
(169, 'id_ID', 'name', 'Mitra Bestari', 'string'),
(170, '', 'abbrevLocaleKey', 'default.groups.abbrev.reader', 'string'),
(170, '', 'nameLocaleKey', 'default.groups.name.reader', 'string'),
(170, 'en_US', 'abbrev', 'Read', 'string'),
(170, 'en_US', 'name', 'Reader', 'string'),
(170, 'id_ID', 'abbrev', 'Baca', 'string'),
(170, 'id_ID', 'name', 'Pembaca', 'string'),
(171, '', 'abbrevLocaleKey', 'default.groups.abbrev.subscriptionManager', 'string'),
(171, '', 'nameLocaleKey', 'default.groups.name.subscriptionManager', 'string'),
(171, 'en_US', 'abbrev', 'SubM', 'string'),
(171, 'en_US', 'name', 'Subscription Manager', 'string'),
(171, 'id_ID', 'abbrev', 'MReg', 'string'),
(171, 'id_ID', 'name', 'Manajer Langganan', 'string'),
(172, '', 'abbrevLocaleKey', 'default.groups.abbrev.manager', 'string'),
(172, '', 'nameLocaleKey', 'default.groups.name.manager', 'string'),
(172, 'en_US', 'abbrev', 'JM', 'string'),
(172, 'en_US', 'name', 'Journal manager', 'string'),
(172, 'id_ID', 'abbrev', 'JM', 'string'),
(172, 'id_ID', 'name', 'Manajer Jurnal', 'string'),
(173, '', 'abbrevLocaleKey', 'default.groups.abbrev.editor', 'string'),
(173, '', 'nameLocaleKey', 'default.groups.name.editor', 'string'),
(173, 'en_US', 'abbrev', 'JE', 'string'),
(173, 'en_US', 'name', 'Journal editor', 'string'),
(173, 'id_ID', 'abbrev', 'JE', 'string'),
(173, 'id_ID', 'name', 'Editor Jurnal', 'string'),
(174, '', 'abbrevLocaleKey', 'default.groups.abbrev.productionEditor', 'string'),
(174, '', 'nameLocaleKey', 'default.groups.name.productionEditor', 'string'),
(174, 'en_US', 'abbrev', 'ProdE', 'string'),
(174, 'en_US', 'name', 'Production editor', 'string'),
(174, 'id_ID', 'abbrev', 'ProdE', 'string'),
(174, 'id_ID', 'name', 'Editor produksi', 'string'),
(175, '', 'abbrevLocaleKey', 'default.groups.abbrev.sectionEditor', 'string'),
(175, '', 'nameLocaleKey', 'default.groups.name.sectionEditor', 'string'),
(175, 'en_US', 'abbrev', 'SecE', 'string'),
(175, 'en_US', 'name', 'Section editor', 'string'),
(175, 'id_ID', 'abbrev', 'EdBag', 'string'),
(175, 'id_ID', 'name', 'Editor Bagian', 'string'),
(176, '', 'abbrevLocaleKey', 'default.groups.abbrev.guestEditor', 'string'),
(176, '', 'nameLocaleKey', 'default.groups.name.guestEditor', 'string'),
(176, 'en_US', 'abbrev', 'GE', 'string'),
(176, 'en_US', 'name', 'Guest editor', 'string'),
(176, 'id_ID', 'abbrev', 'GE', 'string'),
(176, 'id_ID', 'name', 'Editor Tamu', 'string'),
(177, '', 'abbrevLocaleKey', 'default.groups.abbrev.copyeditor', 'string'),
(177, '', 'nameLocaleKey', 'default.groups.name.copyeditor', 'string'),
(177, 'en_US', 'abbrev', 'CE', 'string'),
(177, 'en_US', 'name', 'Copyeditor', 'string'),
(177, 'id_ID', 'abbrev', 'CE', 'string'),
(177, 'id_ID', 'name', 'Copyeditor', 'string'),
(178, '', 'abbrevLocaleKey', 'default.groups.abbrev.designer', 'string'),
(178, '', 'nameLocaleKey', 'default.groups.name.designer', 'string'),
(178, 'en_US', 'abbrev', 'Design', 'string'),
(178, 'en_US', 'name', 'Designer', 'string'),
(178, 'id_ID', 'abbrev', 'Desain', 'string'),
(178, 'id_ID', 'name', 'Desainer', 'string'),
(179, '', 'abbrevLocaleKey', 'default.groups.abbrev.funding', 'string'),
(179, '', 'nameLocaleKey', 'default.groups.name.funding', 'string'),
(179, 'en_US', 'abbrev', 'FC', 'string'),
(179, 'en_US', 'name', 'Funding coordinator', 'string'),
(179, 'id_ID', 'abbrev', 'FC', 'string'),
(179, 'id_ID', 'name', 'Koordinator pendanaan', 'string'),
(180, '', 'abbrevLocaleKey', 'default.groups.abbrev.indexer', 'string'),
(180, '', 'nameLocaleKey', 'default.groups.name.indexer', 'string'),
(180, 'en_US', 'abbrev', 'IND', 'string'),
(180, 'en_US', 'name', 'Indexer', 'string'),
(180, 'id_ID', 'abbrev', 'IND', 'string'),
(180, 'id_ID', 'name', 'Pengindeks', 'string'),
(181, '', 'abbrevLocaleKey', 'default.groups.abbrev.layoutEditor', 'string'),
(181, '', 'nameLocaleKey', 'default.groups.name.layoutEditor', 'string'),
(181, 'en_US', 'abbrev', 'LE', 'string'),
(181, 'en_US', 'name', 'Layout Editor', 'string'),
(181, 'id_ID', 'abbrev', 'LE', 'string'),
(181, 'id_ID', 'name', 'Editor Tata Letak', 'string'),
(182, '', 'abbrevLocaleKey', 'default.groups.abbrev.marketing', 'string'),
(182, '', 'nameLocaleKey', 'default.groups.name.marketing', 'string'),
(182, 'en_US', 'abbrev', 'MS', 'string'),
(182, 'en_US', 'name', 'Marketing and sales coordinator', 'string'),
(182, 'id_ID', 'abbrev', 'MS', 'string'),
(182, 'id_ID', 'name', 'Koordinator penjualan dan pemasaran', 'string'),
(183, '', 'abbrevLocaleKey', 'default.groups.abbrev.proofreader', 'string'),
(183, '', 'nameLocaleKey', 'default.groups.name.proofreader', 'string'),
(183, 'en_US', 'abbrev', 'PR', 'string'),
(183, 'en_US', 'name', 'Proofreader', 'string'),
(183, 'id_ID', 'abbrev', 'PR', 'string'),
(183, 'id_ID', 'name', 'Proofreader', 'string'),
(184, '', 'abbrevLocaleKey', 'default.groups.abbrev.author', 'string'),
(184, '', 'nameLocaleKey', 'default.groups.name.author', 'string'),
(184, 'en_US', 'abbrev', 'AU', 'string'),
(184, 'en_US', 'name', 'Author', 'string'),
(184, 'id_ID', 'abbrev', 'AU', 'string'),
(184, 'id_ID', 'name', 'Penulis', 'string'),
(185, '', 'abbrevLocaleKey', 'default.groups.abbrev.translator', 'string'),
(185, '', 'nameLocaleKey', 'default.groups.name.translator', 'string'),
(185, 'en_US', 'abbrev', 'Trans', 'string'),
(185, 'en_US', 'name', 'Translator', 'string'),
(185, 'id_ID', 'abbrev', 'Trans', 'string'),
(185, 'id_ID', 'name', 'Penerjemah', 'string'),
(186, '', 'abbrevLocaleKey', 'default.groups.abbrev.externalReviewer', 'string'),
(186, '', 'nameLocaleKey', 'default.groups.name.externalReviewer', 'string'),
(186, 'en_US', 'abbrev', 'R', 'string'),
(186, 'en_US', 'name', 'Reviewer', 'string'),
(186, 'id_ID', 'abbrev', 'R', 'string'),
(186, 'id_ID', 'name', 'Mitra Bestari', 'string'),
(187, '', 'abbrevLocaleKey', 'default.groups.abbrev.reader', 'string'),
(187, '', 'nameLocaleKey', 'default.groups.name.reader', 'string'),
(187, 'en_US', 'abbrev', 'Read', 'string'),
(187, 'en_US', 'name', 'Reader', 'string'),
(187, 'id_ID', 'abbrev', 'Baca', 'string'),
(187, 'id_ID', 'name', 'Pembaca', 'string'),
(188, '', 'abbrevLocaleKey', 'default.groups.abbrev.subscriptionManager', 'string'),
(188, '', 'nameLocaleKey', 'default.groups.name.subscriptionManager', 'string'),
(188, 'en_US', 'abbrev', 'SubM', 'string'),
(188, 'en_US', 'name', 'Subscription Manager', 'string'),
(188, 'id_ID', 'abbrev', 'MReg', 'string'),
(188, 'id_ID', 'name', 'Manajer Langganan', 'string');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_group_stage`
--

DROP TABLE IF EXISTS `user_group_stage`;
CREATE TABLE IF NOT EXISTS `user_group_stage` (
  `context_id` bigint(20) NOT NULL,
  `user_group_id` bigint(20) NOT NULL,
  `stage_id` bigint(20) NOT NULL,
  UNIQUE KEY `user_group_stage_pkey` (`context_id`,`user_group_id`,`stage_id`),
  KEY `user_group_stage_context_id` (`context_id`),
  KEY `user_group_stage_user_group_id` (`user_group_id`),
  KEY `user_group_stage_stage_id` (`stage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_group_stage`
--

INSERT INTO `user_group_stage` (`context_id`, `user_group_id`, `stage_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_interests`
--

DROP TABLE IF EXISTS `user_interests`;
CREATE TABLE IF NOT EXISTS `user_interests` (
  `user_id` bigint(20) NOT NULL,
  `controlled_vocab_entry_id` bigint(20) NOT NULL,
  UNIQUE KEY `u_e_pkey` (`user_id`,`controlled_vocab_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_settings`
--

DROP TABLE IF EXISTS `user_settings`;
CREATE TABLE IF NOT EXISTS `user_settings` (
  `user_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `assoc_type` bigint(20) DEFAULT '0',
  `assoc_id` bigint(20) DEFAULT '0',
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL,
  UNIQUE KEY `user_settings_pkey` (`user_id`,`locale`,`setting_name`,`assoc_type`,`assoc_id`),
  KEY `user_settings_user_id` (`user_id`),
  KEY `user_settings_locale_setting_name_index` (`setting_name`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_settings`
--

INSERT INTO `user_settings` (`user_id`, `locale`, `setting_name`, `assoc_type`, `assoc_id`, `setting_value`, `setting_type`) VALUES
(1, 'en_US', 'givenName', 0, 0, 'Meitro', 'string'),
(1, 'id_ID', 'givenName', 0, 0, '', 'string'),
(1, 'en_US', 'familyName', 0, 0, 'Hartanto', 'string'),
(1, 'id_ID', 'familyName', 0, 0, '', 'string'),
(1, 'en_US', 'preferredPublicName', 0, 0, '', 'string'),
(1, 'id_ID', 'preferredPublicName', 0, 0, '', 'string');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_user_groups`
--

DROP TABLE IF EXISTS `user_user_groups`;
CREATE TABLE IF NOT EXISTS `user_user_groups` (
  `user_group_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  UNIQUE KEY `user_user_groups_pkey` (`user_group_id`,`user_id`),
  KEY `user_user_groups_user_group_id` (`user_group_id`),
  KEY `user_user_groups_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_user_groups`
--

INSERT INTO `user_user_groups` (`user_group_id`, `user_id`) VALUES
(1, 1),
(2, 1),
(19, 1),
(36, 1),
(53, 1),
(67, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `versions`
--

DROP TABLE IF EXISTS `versions`;
CREATE TABLE IF NOT EXISTS `versions` (
  `major` int(11) NOT NULL DEFAULT '0',
  `minor` int(11) NOT NULL DEFAULT '0',
  `revision` int(11) NOT NULL DEFAULT '0',
  `build` int(11) NOT NULL DEFAULT '0',
  `date_installed` datetime NOT NULL,
  `current` tinyint(4) NOT NULL DEFAULT '0',
  `product_type` varchar(30) DEFAULT NULL,
  `product` varchar(30) DEFAULT NULL,
  `product_class_name` varchar(80) DEFAULT NULL,
  `lazy_load` tinyint(4) NOT NULL DEFAULT '0',
  `sitewide` tinyint(4) NOT NULL DEFAULT '0',
  UNIQUE KEY `versions_pkey` (`product_type`,`product`,`major`,`minor`,`revision`,`build`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `versions`
--

INSERT INTO `versions` (`major`, `minor`, `revision`, `build`, `date_installed`, `current`, `product_type`, `product`, `product_class_name`, `lazy_load`, `sitewide`) VALUES
(1, 0, 0, 0, '2020-05-29 08:22:57', 1, 'plugins.metadata', 'dc11', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:22:58', 1, 'plugins.metadata', 'mods34', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:22:58', 1, 'plugins.metadata', 'openurl10', '', 0, 0),
(1, 0, 1, 0, '2020-05-29 08:22:58', 1, 'plugins.blocks', 'browse', 'BrowseBlockPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:22:58', 1, 'plugins.blocks', 'developedBy', 'DevelopedByBlockPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:22:58', 1, 'plugins.blocks', 'information', 'InformationBlockPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:22:58', 1, 'plugins.blocks', 'languageToggle', 'LanguageToggleBlockPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:22:58', 1, 'plugins.blocks', 'makeSubmission', 'MakeSubmissionBlockPlugin', 1, 0),
(1, 1, 0, 0, '2020-05-29 08:22:58', 1, 'plugins.blocks', 'subscription', 'SubscriptionBlockPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:22:58', 1, 'plugins.gateways', 'resolver', '', 0, 0),
(1, 2, 0, 0, '2020-05-29 08:22:58', 1, 'plugins.generic', 'acron', 'AcronPlugin', 1, 1),
(1, 1, 2, 1, '2020-05-29 08:22:58', 0, 'plugins.generic', 'orcidProfile', 'OrcidProfilePlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'usageStats', 'UsageStatsPlugin', 0, 1),
(1, 0, 0, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'usageEvent', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'tinymce', 'TinyMCEPlugin', 1, 0),
(1, 2, 0, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'staticPages', 'StaticPagesPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'recommendBySimilarity', 'RecommendBySimilarityPlugin', 1, 1),
(1, 0, 0, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'recommendByAuthor', 'RecommendByAuthorPlugin', 1, 1),
(1, 0, 1, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'pdfJsViewer', 'PdfJsViewerPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'lensGalley', 'LensGalleyPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'announcementFeed', 'AnnouncementFeedPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'htmlArticleGalley', 'HtmlArticleGalleyPlugin', 1, 0),
(1, 1, 0, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'googleScholar', 'GoogleScholarPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'googleAnalytics', 'GoogleAnalyticsPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'dublinCoreMeta', 'DublinCoreMetaPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:22:59', 1, 'plugins.generic', 'driver', 'DRIVERPlugin', 1, 0),
(1, 2, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.generic', 'customBlockManager', 'CustomBlockManagerPlugin', 1, 0),
(0, 1, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.generic', 'citationStyleLanguage', 'CitationStyleLanguagePlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.generic', 'webFeed', 'WebFeedPlugin', 1, 0),
(2, 1, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.importexport', 'crossref', '', 0, 0),
(2, 0, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.importexport', 'datacite', '', 0, 0),
(1, 1, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.importexport', 'doaj', '', 0, 0),
(2, 0, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.importexport', 'medra', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.importexport', 'native', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.importexport', 'pubmed', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.importexport', 'users', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.oaiMetadataFormats', 'dc', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.oaiMetadataFormats', 'marc', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.oaiMetadataFormats', 'marcxml', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.oaiMetadataFormats', 'rfc1807', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.paymethod', 'manual', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.paymethod', 'paypal', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:23:00', 1, 'plugins.pubIds', 'doi', 'DOIPubIdPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:23:01', 1, 'plugins.pubIds', 'urn', 'URNPubIdPlugin', 1, 0),
(1, 0, 0, 0, '2020-05-29 08:23:01', 1, 'plugins.reports', 'articles', '', 0, 0),
(1, 1, 0, 0, '2020-05-29 08:23:01', 1, 'plugins.reports', 'counterReport', '', 0, 0),
(2, 0, 0, 0, '2020-05-29 08:23:01', 1, 'plugins.reports', 'reviewReport', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:23:01', 1, 'plugins.reports', 'subscriptions', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:23:01', 1, 'plugins.reports', 'views', '', 0, 0),
(1, 0, 0, 0, '2020-05-29 08:23:01', 1, 'plugins.themes', 'default', 'DefaultThemePlugin', 1, 0),
(3, 2, 0, 3, '2021-01-15 08:20:15', 0, 'core', 'ojs2', '', 0, 1),
(3, 2, 0, 0, '2020-05-29 10:02:52', 1, 'plugins.themes', 'bootstrap3', 'BootstrapThreeThemePlugin', 0, 0),
(1, 0, 5, 2, '2020-05-29 10:38:31', 1, 'plugins.importexport', 'quickSubmit', '', 0, 0),
(2, 1, 0, 0, '2021-01-11 17:44:00', 1, 'plugins.themes', 'oldGregg', 'OldGreggThemePlugin', 1, 0),
(1, 1, 2, 5, '2021-01-16 16:38:05', 1, 'plugins.generic', 'orcidProfile', 'OrcidProfilePlugin', 1, 0),
(3, 2, 1, 2, '2021-01-16 16:35:11', 1, 'core', 'ojs2', '', 0, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
